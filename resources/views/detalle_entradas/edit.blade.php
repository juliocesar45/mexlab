@extends('layouts.app', ['activePage' => 'detalle_entrada', 'menuParent' => 'procesos', 'titlePage' => __('Detalle Entrada')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('detalle_entrada.update', $detalleEntrada->id) }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('put')
                        <div class="card ">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-info-circle fa-2x"></i>
                                </div>
                                <h4 class="card-title">Editar Detalle Entrada</h4>
                            </div>
                            @include('detalle_entradas.layout.form',['show' => false,'edit' => true, 'create' => false])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
