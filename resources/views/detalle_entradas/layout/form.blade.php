<div class="card-body ">
    <div class="row">
        <div class="col-md-12 text-right">
            @if ($create)
                <a href="{{ route('entrada.show', $entrada->id) }}" class="btn btn-sm btn-success">{{ __('Regresar a la consulta de la entrada') }}</a>
            @else
                <a href="{{ route('entrada.show', $detalleEntrada->entrada_id) }}" class="btn btn-sm btn-success">{{ __('Regresar a la consulta de la entrada') }}</a>
            @endif
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Entrada</label>
        <div class="col-sm-7">
            <div class="form-group">
                <select class="select2" aria-label="Entrada" disabled>
                    @if ($create)
                        <option value="{{ $entrada->id }}">{{ $entrada->folio_compra }}</option>
                    @else
                        <option value="{{ $detalleEntrada->id }}">{{ $detalleEntrada->entrada->folio_compra }}</option>
                    @endif
                </select>
            </div>
        </div>
        <input type="hidden" name="entrada_id" value="{{$entrada->id}}">
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Cantidad</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('cantidad') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('cantidad') ? ' is-invalid' : '' }}" name="cantidad"
                    id="cantidad" type="text" number="true" placeholder="Cantidad" @if ($create) value="{{ old('cantidad') }}" @else value="{{ $detalleEntrada->cantidad, old('cantidad') }}" @endif required="true" number="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'cantidad'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Cantidad En Mal Estado</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('cantidad_mal_estado') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('cantidad_mal_estado') ? ' is-invalid' : '' }}" name="cantidad_mal_estado"
                    id="cantidad_mal_estado" type="text" number="true" placeholder="Cantidad En Mal Estado" @if ($create) value="{{ old('cantidad_mal_estado') }}" @else value="{{ $detalleEntrada->cantidad_mal_estado, old('cantidad_mal_estado') }}" @endif required="true" number="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'cantidad_mal_estado'])
            </div>
        </div>
    </div>
</div>
<div class="card-footer ml-auto mr-auto">
    @if ($create)
        <button type="submit" class="btn btn-success">Crear</button>
    @endif
    @if ($edit)
        <button type="submit" class="btn btn-success">Editar</button>
    @endif
    @if ($show)
        <form action="{{ route('detalle_entrada.destroy', $detalleEntrada->id) }}" method="post">
            @csrf
            @method('delete')
            <a rel="tooltip" class="btn btn-success" href="{{ route('detalle_entrada.edit', $detalleEntrada->id) }}"
                data-original-title="" title=""> Editar
            </a>

            <input type="submit" class="btn btn-danger" value="Eliminar">
        </form>
    @endif
</div>
