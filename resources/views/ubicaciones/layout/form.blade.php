<div class="card-body ">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="{{ route('ubicacion.index') }}" class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Pasillo</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('rack_id') ? ' has-danger' : '' }}">
                <select class="select2" name="rack_id" aria-label="Rack" @if ($show) disabled @endif>
                    @foreach (App\Rack::all(['id', 'pasillo']) as $rack)
                        @if ($create)
                            <option value="{{ $rack->id }}">{{ $rack->pasillo }}</option>
                        @else
                            @if ($rack->id == $ubicacion->rack_id)
                                <option value="{{ $rack->id }}" selected>{{ $rack->pasillo }}</option>
                            @else
                                <option value="{{ $rack->id }}">{{ $rack->pasillo }}</option>
                            @endif
                        @endif

                    @endforeach
                </select>
            </div>
        </div>
    </div>

    {{-- <div class="row">
        <label class="col-sm-2 col-form-label">Lote</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('lote_producto_id') ? ' has-danger' : '' }}">
                <select class="select2" name="lote_producto_id" aria-label="LoteProducto" @if ($show) disabled @endif>
                    @foreach (App\LoteProducto::all(['id', 'lote']) as $loteProducto)
                        @if ($create)
                            <option value="{{ $loteProducto->id }}">{{ $loteProducto->lote }}</option>
                        @else
                            @if ($loteProducto->id == $ubicacion->lote_producto_id)
                                <option value="{{ $loteProducto->id }}" selected>{{ $loteProducto->lote }}</option>
                            @else
                                <option value="{{ $loteProducto->id }}">{{ $loteProducto->lote }}</option>
                            @endif
                        @endif

                    @endforeach
                </select>
            </div>
        </div>
    </div> --}}

    {{-- <div class="row">
        <label class="col-sm-2 col-form-label"></label>
        <div class="col-sm-7">
            <div class="form-check">
                <label class="form-check-label">


                    <input class="form-check-input" name="seccion" type="checkbox" 
                        @if ($create) 
                            @if(old('seccion'))
                                checked
                            @endif
                        @else 
                            @if($ubicacion->seccion)
                                checked
                            @endif
                        @endif 

                        @if ($show)
                            @if($ubicacion->seccion)
                                checked
                            @endif
                            disabled 
                        @endif
                    >
                    Seccionado ?
                    <span class="form-check-sign">
                        <span class="check"></span>
                    </span>
                </label>
            </div>
        </div>
    </div> --}}


    <div class="row">
        <label class="col-sm-2 col-form-label">Fila</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('identificador') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('identificador') ? ' is-invalid' : '' }}" name="identificador"
                    id="identificador" type="text" placeholder="Fila" @if ($create) value="{{ old('identificador') }}" @else value="{{ $ubicacion->identificador, old('identificador') }}" @endif required="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'identificador'])
            </div>
        </div>
    </div>

    <div class="row">
        <label class="col-sm-2 col-form-label">Anaquel</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('x') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('x') ? ' is-invalid' : '' }}" name="x"
                    id="x" type="text" number="true" placeholder="Anaquel" @if ($create) value="{{ old('x') }}" @else value="{{ $ubicacion->x, old('x') }}" @endif required="true" number="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'x'])
            </div>
        </div>
    </div>


    <div class="row">
        <label class="col-sm-2 col-form-label">Estante</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('y') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('y') ? ' is-invalid' : '' }}" name="y"
                    id="y" type="text" number="true" placeholder="Estante" @if ($create) value="{{ old('y') }}" @else value="{{ $ubicacion->y, old('y') }}" @endif required="true" number="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'y'])
            </div>
        </div>
    </div>

    {{-- <div class="row">
        <label class="col-sm-2 col-form-label">Cantidad</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('cantidad') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('cantidad') ? ' is-invalid' : '' }}" name="cantidad"
                    id="cantidad" tcantidadpe="text" placeholder="Cantidad" @if ($create) value="{{ old('cantidad') }}" @else value="{{ $ubicacion->cantidad, old('cantidad') }}" @endif number="true" aria-required="true" disabled/>
                @include('alerts.feedback', ['field' => 'cantidad'])
            </div>
        </div>
    </div> --}}
 
</div>
<div class="card-footer ml-auto mr-auto">
    @if ($create)
        <button type="submit" class="btn btn-success">Crear</button>
    @endif
    @if ($edit)
        <button type="submit" class="btn btn-success">Editar</button>
    @endif
    @if ($show)
        <form action="{{ route('ubicacion.destroy', $ubicacion->id) }}" method="post">
            @csrf
            @method('delete')
            <a rel="tooltip" class="btn btn-success" href="{{ route('ubicacion.edit', $ubicacion->id) }}"
                data-original-title="" title=""> Editar
            </a>

            <input type="submit" class="btn btn-danger" value="Eliminar">
        </form>
    @endif
</div>
