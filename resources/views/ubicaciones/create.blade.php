@extends('layouts.app', ['activePage' => 'ubicacion', 'menuParent' => 'catalogos', 'titlePage' => __('Ubicación')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('ubicacion.store') }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        <div class="card">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-map-marked-alt fa-2x"></i>
                                </div>
                                <h4 class="card-title">Agregar Ubicación</h4>
                            </div>
                            @include('ubicaciones.layout.form',['show' => false,'edit' => false, 'create' => true])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
