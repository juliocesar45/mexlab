@extends('layouts.app', ['activePage' => 'ruta embarque', 'menuParent' => 'consultas', 'titlePage' => __('Rutas de embarques')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                  <i class="fas fa-route fa-2x"></i>
                </div>
                <h4 class="card-title">Rutas de embarques</h4>
              </div>
              <div class="card-body">
                @can('create', App\User::class)
                  <div class="row">
                    <div class="col-12 text-right">
                      <a href="{{ route('ruta_embarque.create') }}" class="btn btn-sm btn-success">Agregar Ruta de Embarques</a>
                    </div>
                  </div>
                @endcan
                <div class="table-responsive">
                  <table id="datatables" class="table table-striped table-no-bordered table-hover" style="display:none;width:100%">
                    <thead class="text-primary">
                      <th class="desktop">
                      Paquetería
                      </th>
                      <th class="desktop">
                      Guía
                      </th>
                      <th class="desktop">
                      Entregar a
                      </th>
                      <th class="desktop">
                      Dirección Factura
                      </th>
                      {{-- <th class="desktop">
                      Dirección
                      </th>  --}}
                      <th class="desktop">
                      Conductor
                      </th>
                      <th class="desktop">
                      Mensajero
                      </th>
                      <th class="desktop">
                      Tipo de Envio
                      </th>
                      {{-- <th class="desktop">
                      Ruta
                      </th> --}}
                      {{-- <th class="desktop">
                      Tiempo Estimado
                      </th> --}}
                      {{-- <th class="desktop">
                      Fecha de Entrega Estimada
                      </th> --}}
                      {{-- <th class="desktop">
                      Fecha de Entrega
                      </th> --}}
                      <th class="desktop">
                      Estatus
                      </th>
                      @can('manage-users', App\User::class)
                        <th class="text-right desktop">
                          {{ __('Acciones') }}
                        </th>
                      @endcan
                    </thead>
                    <tbody>
                      @foreach($rutaEmbarques as $rutaEmbarque)
                        <tr>
                          <td>{{$rutaEmbarque->paqueteria->nombre ?? ''}}</td>
                          <td>{{$rutaEmbarque->guia}}</td>
                          <td>{{$rutaEmbarque->entregar_a}}</td>
                          <td>{{$rutaEmbarque->direccion_factura}}</td>
                          {{-- <td>{{$rutaEmbarque->direccion}}</td>   --}}
                          <td>{{$rutaEmbarque->conductor->nombre ?? ''}}</td>
                          <td>{{$rutaEmbarque->mensajero->nombre ?? ''}}</td> 
                          <td>{{$rutaEmbarque->tipo_envio->nombre ?? ''}}</td>
                          {{-- <td>{{$rutaEmbarque->ruta}}</td> 
                          <td>{{$rutaEmbarque->tiempo_estimado}}</td> 
                          <td>{{\Carbon\Carbon::parse($rutaEmbarque->fecha_entrega_estimada)->format('d/m/Y H:i:s')}}</td>  
                          <td>{{\Carbon\Carbon::parse($rutaEmbarque->fecha_entrega)->format('d/m/Y H:i:s')}}</td>   --}}
                          <td>{{$rutaEmbarque->estatus}}</td>
                          <td class="td-actions text-right">
                              <form action="{{ route('ruta_embarque.destroy', $rutaEmbarque->id) }}" method="post">
                                  @csrf
                                  @method('delete')
                                  <a rel="tooltip" class="btn btn-warning btn-link" href="{{ route('ruta_embarque.show', $rutaEmbarque->id) }}" data-original-title="" title="">
                                    <i class="material-icons">visibility</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('ruta_embarque.edit', $rutaEmbarque->id) }}" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="Swal.fire({
                                    title: 'Estás seguro?',
                                    text: 'Se borrará el registro!',
                                    type: 'warning',
                                    showCancelButton: true,
                                    buttonsStyling: false, 
                                    confirmButtonClass: 'btn btn-danger',
                                    cancelButtonClass: 'btn btn-success',
                                    confirmButtonText: 'Si, borralo!',
                                    cancelButtonText: 'No, quiero conservarlo!'
                                }).then((result) => {
                                    if (result.value) {
                                        this.parentElement.submit()
                                    }
                                })">
                                      <i class="material-icons">close</i>
                                      <div class="ripple-container"></div>
                                  </button>
                              </form>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables').fadeIn(1100);
      $('#datatables').DataTable({
        "order": [[ 0, "desc" ]],
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "Todos"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Buscar Ruta de Embarque",
        },
        "columnDefs": [
          { "orderable": false, "targets": 5 },
        ],
      });
    });
  </script>
@endpush