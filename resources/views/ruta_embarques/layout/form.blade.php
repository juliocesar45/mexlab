<div class="card-body ">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="{{ route('ruta_embarque.index') }}" class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
        </div>
    </div>    
     <div class="row">
        <label class="col-sm-2 col-form-label">Paquetería</label>
        <div class="col-sm-4">
            <div class="form-group{{ $errors->has('paqueteria_id') ? ' has-danger' : '' }}">
                @if ($show) 
                {!! Form::select('paqueteria_id', App\ListHelper::getPaqueteria(), $rutaEmbarque->paqueteria_id ?? old('paqueteria_id') , ['class' => 'select2', 'id' => 'paqueteria_id', 'placeholder' => 'Seleccione la paquetería...', 'required' => 'true']) !!}
                @elseif ($edit)
                    {!! Form::select('paqueteria_id', App\ListHelper::getPaqueteria(), $rutaEmbarque->paqueteria_id ?? old('paqueteria_id') , ['class' => 'select2', 'id' => 'paqueteria_id', 'placeholder' => 'Seleccione la paquetería...', 'required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'paqueteria_id'])
                @else
                    {!! Form::select('paqueteria_id', App\ListHelper::getPaqueteria(), old('paqueteria_id') , ['class' => 'select2', 'id' => 'paqueteria_id', 'placeholder' => 'Seleccione la paquetería...', 'required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'paqueteria_id'])
                @endif
            </div>
        </div>
     </div>
        <div class="row">
         <label class="col-sm-2 col-form-label">Guía</label>
            <div class="col-sm-7">
                <div class="form-group{{ $errors->has('guia') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('guia') ? ' is-invalid' : '' }}" name="guia"
                        id="guia" type="text" placeholder="guia" @if ($create) value="{{ old('guia') }}" @else value="{{ $rutaEmbarque->guia, old('guia') }}" @endif @if ($show)
                    disabled @endif/>
                    @include('alerts.feedback', ['field' => 'guia'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Entregar a</label>
            <div class="col-sm-7">
                <div class="form-group{{ $errors->has('entregar_a') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('entregar_a') ? ' is-invalid' : '' }}" name="entregar_a"
                        id="entregar_a" type="text" placeholder="entregar a" @if ($create) value="{{ old('entregar_a') }}" @else value="{{ $rutaEmbarque->entregar_a, old('entregar_a') }}" @endif @if ($show)
                    disabled @endif/>
                    @include('alerts.feedback', ['field' => 'entregar_a'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Dirección Factura</label>
            <div class="col-sm-7">
                <div class="form-group{{ $errors->has('direccion_factura') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('direccion_factura') ? ' is-invalid' : '' }}" name="direccion_factura"
                        id="direccion_factura" type="text" placeholder="direccion factura" @if ($create) value="{{ old('direccion_factura') }}" @else value="{{ $rutaEmbarque->direccion_factura, old('direccion_factura') }}" @endif @if ($show)
                    disabled @endif/>
                    @include('alerts.feedback', ['field' => 'direccion_factura'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Dirección</label>
            <div class="col-sm-7">
                <div class="form-group{{ $errors->has('direccion') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion"
                        id="direccion" type="text" placeholder="direccion" @if ($create) value="{{ old('direccion') }}" @else value="{{ $rutaEmbarque->direccion, old('direccion') }}" @endif @if ($show)
                    disabled @endif/>
                    @include('alerts.feedback', ['field' => 'direccion'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Conductor</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('conductor_id') ? ' has-danger' : '' }}">
                    @if ($show) 
                    {!! Form::select('conductor_id', App\ListHelper::getConductor(), $rutaEmbarque->conductor_id ?? old('conductor_id') , ['class' => 'select2', 'id' => 'conductor_id', 'placeholder' => 'Seleccione el conductor...', 'required' => 'true']) !!}
                    @elseif ($edit)
                        {!! Form::select('conductor_id', App\ListHelper::getConductor(), $rutaEmbarque->conductor_id ?? old('conductor_id') , ['class' => 'select2', 'id' => 'conductor_id', 'placeholder' => 'Seleccione el conductor...', 'required' => 'true']) !!}
                        @include('alerts.feedback', ['field' => 'conductor_id'])
                    @else
                        {!! Form::select('conductor_id', App\ListHelper::getConductor(), old('conductor_id') , ['class' => 'select2', 'id' => 'conductor_id', 'placeholder' => 'Seleccione el conductor...', 'required' => 'true']) !!}
                        @include('alerts.feedback', ['field' => 'conductor_id'])
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Mensajero</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('mensajero_id') ? ' has-danger' : '' }}">
                    @if ($show) 
                    {!! Form::select('mensajero_id', App\ListHelper::getMensajero(), $rutaEmbarque->mensajero_id ?? old('mensajero_id') , ['class' => 'select2', 'id' => 'mensajero_id', 'placeholder' => 'Seleccione el mensajero...', 'required' => 'true']) !!}
                    @elseif ($edit)
                        {!! Form::select('mensajero_id', App\ListHelper::getMensajero(), $rutaEmbarque->mensajero_id ?? old('mensajero_id') , ['class' => 'select2', 'id' => 'mensajero_id', 'placeholder' => 'Seleccione el mensajero...', 'required' => 'true']) !!}
                        @include('alerts.feedback', ['field' => 'mensajero_id'])
                    @else
                        {!! Form::select('mensajero_id', App\ListHelper::getMensajero(), old('mensajero_id') , ['class' => 'select2', 'id' => 'mensajero_id', 'placeholder' => 'Seleccione el mensajero...', 'required' => 'true']) !!}
                        @include('alerts.feedback', ['field' => 'mensajero_id'])
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Tipo de Envio</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('tipo_envio_id') ? ' has-danger' : '' }}">
                    @if ($show) 
                    {!! Form::select('tipo_envio_id', App\ListHelper::getTipoEnvio(), $rutaEmbarque->tipo_envio_id ?? old('tipo_envio_id') , ['class' => 'select2', 'id' => 'tipo_envio_id', 'placeholder' => 'Seleccione el tipo de envío...', 'required' => 'true']) !!}
                    @elseif ($edit)
                        {!! Form::select('tipo_envio_id', App\ListHelper::getTipoEnvio(), $rutaEmbarque->tipo_envio_id ?? old('tipo_envio_id') , ['class' => 'select2', 'id' => 'tipo_envio_id', 'placeholder' => 'Seleccione el tipo de envío...', 'required' => 'true']) !!}
                        @include('alerts.feedback', ['field' => 'tipo_envio_id'])
                    @else
                        {!! Form::select('tipo_envio_id', App\ListHelper::getTipoEnvio(), old('tipo_envio_id') , ['class' => 'select2', 'id' => 'tipo_envio_id', 'placeholder' => 'Seleccione el tipo de envío...', 'required' => 'true']) !!}
                        @include('alerts.feedback', ['field' => 'tipo_envio_id'])
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Ruta</label>
            <div class="col-sm-7">
                <div class="form-group{{ $errors->has('ruta') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('ruta') ? ' is-invalid' : '' }}" name="ruta"
                            id="ruta" type="text" placeholder="ruta" @if ($create) value="{{ old('ruta') }}" @else value="{{ $rutaEmbarque->ruta, old('ruta') }}" @endif @if ($show)
                            disabled @endif/>
                    @include('alerts.feedback', ['field' => 'ruta'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Tiempo Estimado</label>
            <div class="col-sm-7">
                <div class="form-group{{ $errors->has('tiempo_estimado') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('tiempo_estimado') ? ' is-invalid' : '' }}" name="tiempo_estimado"
                            id="tiempo_estimado" type="text" placeholder="tiempo estimado" @if ($create) value="{{ old('tiempo_estimado') }}" @else value="{{ $rutaEmbarque->ruta, old('tiempo_estimado') }}" @endif @if ($show)
                            disabled @endif/>
                    @include('alerts.feedback', ['field' => 'tiempo_estimado'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Fecha Entrega Estimada</label>
            <div class="col-sm-7">
                <div class="form-group{{ $errors->has('fecha_entrega_estimada') ? ' has-danger' : '' }}">
                    <input type="text" data-date-format="YYYY-MM-DD HH:mm:ss"
                        class="form-control datetimepicker {{ $errors->has('fecha_entrega_estimada') ? ' is-invalid' : '' }}" name="fecha_entrega_estimada"
                            id="fecha_entrega_estimada" placeholder="fecha entrega estimada" @if ($create) value="{{ \Carbon\Carbon::now() }}" @else value="{{ $rutaEmbarque->fecha_entrega_estimada, old('fecha_entrega_estimada') }}" @endif required="true" aria-required="true" @if ($show)
                            disabled @endif/>
                    @include('alerts.feedback', ['field' => 'fecha_entrega_estimada'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Fecha Entrega</label>
            <div class="col-sm-7">
                <div class="form-group{{ $errors->has('fecha_entrega') ? ' has-danger' : '' }}">
                    <input type="text" data-date-format="YYYY-MM-DD HH:mm:ss"
                        class="form-control datetimepicker {{ $errors->has('fecha_entrega') ? ' is-invalid' : '' }}" name="fecha_entrega"
                            id="fecha_entrega" placeholder="fecha entrega estimada" @if ($create) value="{{ \Carbon\Carbon::now() }}" @else value="{{ $rutaEmbarque->fecha_entrega, old('fecha_entrega') }}" @endif required="true" aria-required="true" @if ($show)
                            disabled @endif/>
                    @include('alerts.feedback', ['field' => 'fecha_entrega'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Estatus</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('estatus') ? ' has-danger' : '' }}">
                    @if ($show) 
                    {!! Form::select('estatus', App\ListHelper::getEstatusRuta(), $rutaEmbarque->estatus ?? old('estatus') , ['class' => 'select2', 'id' => 'estatus', 'placeholder' => 'Seleccione el tipo de envío...', 'required' => 'true']) !!}
                    @elseif ($edit)
                        {!! Form::select('estatus', App\ListHelper::getEstatusRuta(), $rutaEmbarque->estatus ?? old('estatus') , ['class' => 'select2', 'id' => 'estatus', 'placeholder' => 'Seleccione el tipo de envío...', 'required' => 'true']) !!}
                        @include('alerts.feedback', ['field' => 'estatus'])
                    @else
                        {!! Form::select('estatus', App\ListHelper::getEstatusRuta(), old('estatus') , ['class' => 'select2', 'id' => 'estatus', 'placeholder' => 'Seleccione el tipo de envío...', 'required' => 'true']) !!}
                        @include('alerts.feedback', ['field' => 'estatus'])
                    @endif
                </div>
            </div>
        </div>
        </div>
        <div class="card-footer ml-auto mr-auto">
            @if ($create)
                <button type="submit" class="btn btn-success">Crear</button>
            @endif
            @if ($edit)
                <button type="submit" class="btn btn-success">Editar</button>
            @endif
            @if ($show)
                <form action="{{ route('ruta_embarque.destroy', $rutaEmbarque->id) }}" method="post">
                    @csrf
                    @method('delete')
                        <a rel="tooltip" class="btn btn-success" href="{{ route('ruta_embarque.edit', $rutaEmbarque->id) }}"
                            data-original-title="" title=""> Editar
                        </a>
                    <input type="text" id="ruta_embarque_id" name="ruta_embarque_id" aria-label="ruta_embarque" value={{$rutaEmbarque->id}} hidden @if ($show) disabled @endif/>
                    <input type="submit" class="btn btn-danger" value="Eliminar">
                    
                </form>
            @endif
        </div>
</div>
                