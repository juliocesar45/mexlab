@extends('layouts.app', ['activePage' => 'ruta embarque', 'menuParent' => 'procesos', 'titlePage' => __('Agregar Ruta Embarque')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('ruta_embarque.store') }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        <div class="card">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-route fa-2x"></i>
                                </div>
                                <h4 class="card-title">Agregar Salida</h4>
                            </div>
                            @include('ruta_embarques.layout.form',['show' => false,'edit' => false, 'create' => true])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
