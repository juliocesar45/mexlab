@extends('layouts.app', ['activePage' => 'Estatus Factura', 'menuParent' => 'catalogos', 'titlePage' => __('Estatus
Factura')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('estatus_factura.store') }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('post')

                        <div class="card ">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-file-invoice fa-2x"></i>
                                </div>
                                <h4 class="card-title">Agregar Estatus</h4>
                            </div>
                            @include('estatus_facturas.layout.form',['show' => false,'edit' => false, 'create' => true])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
