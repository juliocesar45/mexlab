@extends('layouts.app', ['activePage' => 'estatus factura', 'menuParent' => 'catalogos', 'titlePage' => __('Estatus
Factura')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data"
                        action="{{ route('estatus_factura.update', $estatusFactura) }}" autocomplete="off"
                        class="form-horizontal">
                        @csrf
                        @method('put')
                        <div class="card ">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-file-invoice fa-2x"></i>
                                </div>
                                <h4 class="card-title">Consulta Estatus</h4>
                            </div>
                            @include('estatus_facturas.layout.form',['show' => true,'edit' => false, 'create' => false])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
