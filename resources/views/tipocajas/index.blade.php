@extends('layouts.app', ['activePage' => 'tipocaja', 'menuParent' => 'catalogos', 'titlePage' => __('Tipos De Caja')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-box-open fa-2x"></i>
                            </div>
                            <h4 class="card-title">Tipos De Caja</h4>
                        </div>
                        <div class="card-body">
                            @can('create', App\User::class)
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <a href="{{ route('tipocaja.create') }}" class="btn btn-sm btn-success">Agregar
                                            Tipo De Caja</a>
                                    </div>
                                </div>
                            @endcan
                            <div class="table-responsive">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                    style="display:none;width:100%;">
                                    <thead class="text-primary">
                                        <th class="desktop">
                                            Nombre
                                        </th>
                                        @can('manage-users', App\User::class)
                                            <th class="text-right desktop">
                                                {{ __('Acciones') }}
                                            </th>
                                        @endcan
                                    </thead>
                                    <tbody>
                                        @foreach ($tipocajas as $tipocaja)
                                            <tr>
                                                <td>{{ $tipocaja->nombre }}</td>
                                                <td class="td-actions text-right">
                                                    <form action="{{ route('tipocaja.destroy', $tipocaja->id) }}"
                                                        method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <a rel="tooltip" class="btn btn-warning btn-link"
                                                            href="{{ route('tipocaja.show', $tipocaja->id) }}"
                                                            data-original-title="" title="">
                                                            <i class="material-icons">visibility</i>
                                                            <!-- <i class="material-icons">show</i> -->
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                        <a rel="tooltip" class="btn btn-success btn-link"
                                                            href="{{ route('tipocaja.edit', $tipocaja->id) }}"
                                                            data-original-title="" title="">
                                                            <i class="material-icons">edit</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                        <button type="button" class="btn btn-danger btn-link"
                                                            data-original-title="" title="" onclick="            Swal.fire({
                                                                  title: 'Estás seguro?',
                                                                  text: 'Se borrará el registro!',
                                                                  type: 'warning',
                                                                  showCancelButton: true,
                                                                  buttonsStyling: false, 
                                                                  confirmButtonClass: 'btn btn-danger',
                                                                  cancelButtonClass: 'btn btn-success',
                                                                  confirmButtonText: 'Si, borralo!',
                                                                  cancelButtonText: 'No, quiero conservarlo!'
                                                              }).then((result) => {
                                                                  if (result.value) {
                                                                      this.parentElement.submit()
                                                                  }
                                                              })">
                                                            <i class="material-icons">close</i>
                                                            <div class="ripple-container"></div>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $('#datatables').fadeIn(1100);
            $('#datatables').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Todos"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Buscar tipos de caja",
                },
                "columnDefs": [{
                    "orderable": false,
                    "targets": 1
                }, ],
            });
        });
    </script>
@endpush
