    <div class="card-body ">
        <div class="row">
            <div class="col-md-12 text-right">
                <a href="{{ route('tipocaja.index') }}"
                    class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Nombre</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre"
                        id="nombre" type="text" placeholder="Nombre" @if($create) value="{{ old('nombre') }}" @else value="{{ $tipoCaja->nombre,old('nombre') }}" @endif @if ($show) disabled @endif required="true"
                        aria-required="true" />
                    @include('alerts.feedback', ['field' => 'nombre'])
                </div>
            </div>
        </div>
        </div>

    <div class="card-footer ml-auto mr-auto">
        @if ($create)
            <button type="submit" class="btn btn-success">Crear</button>
        @endif
        @if ($edit)
            <button type="submit" class="btn btn-success">Editar</button>
        @endif
        @if ($show)
            <form action="{{ route('tipocaja.destroy', $tipoCaja->id) }}" method="post">
                @csrf
                @method('delete')
                <a rel="tooltip" class="btn btn-success" href="{{ route('tipocaja.edit', $tipoCaja->id) }}"
                    data-original-title="" title=""> Editar
                </a>

                <input type="submit" class="btn btn-danger" value="Eliminar">
            </form>
        @endif
    </div>
    </div>
