@extends('layouts.app', ['activePage' => 'tipocaja', 'menuParent' => 'catalogos', 'titlePage' => __('Tipo De Caja')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('tipocaja.update', $tipoCaja) }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('put')

                        <div class="card ">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-box-open fa-2x"></i>
                                </div>
                                <h4 class="card-title">Consulta Tipo De Caja</h4>
                            </div>
                            @include('tipocajas.layout.form',['show' => true,'edit' => false, 'create' => false])
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
