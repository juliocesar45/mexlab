@extends('layouts.app', ['activePage' => 'proveedor', 'menuParent' => 'catalogos', 'titlePage' => __('Proveedor')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('proveedor.update', $proveedor) }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('put')

                        <div class="card ">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-parachute-box fa-2x"></i>
                                </div>
                                <h4 class="card-title">Consulta Proveedor</h4>
                            </div>
                            @include('proveedores.layout.form',['show' => true,'edit' => false, 'create' => false])
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
