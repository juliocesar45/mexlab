@extends('layouts.app', ['activePage' => 'detalle_factura', 'menuParent' => 'procesos', 'titlePage' => __('Detalle Factura')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('detalle_factura.store') }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        <div class="card">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-file-invoice fa-2x"></i>
                                </div>
                                <h4 class="card-title">Agregar Detalle Factura</h4>
                            </div>
                            @include('detalle_facturas.layout.form',['show' => false,'edit' => false, 'create' => true])
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-danger card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-file-invoice fa-2x"></i>
                            </div>
                            <h4 class="card-title">Detalles de la factura</h4>
                        </div>
                        <div class="card-body">
                            <div class="row mb-4">
                                <div class="col-md-12">
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-12">
                                    <table class="table table-hover" id="tabla_detalle_facturas" style="width: 100%;"></table>
                                </div>
                            </div>
                            <div class="row mb-4">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div>
@endsection
@push('js')
<script>
$(document).ready(function() {
var token = "{{ csrf_token() }}";
tabla_detalles = $('#tabla_detalle_facturas').DataTable({
        processing: true,
        scrollX: true,
        ajax:{
            url: "{{ route('facturas.detalles') }}",
            dataSrc: "",
            data: function ( d ) {
                d._token = token;
                d.id_factura = $('#factura_id').val();
            }
        },
        columns:[
        { title: "#", data:"num", name:"num" },
        { title: "Cantidad", data:"cantidad", name:"cantidad" },
        { title: "Descripción", data:"descripcion", name:"descripcion" },
        { title: "Capturado Por", data:"capturado_por", name:"capturado_por" }
        ]
    });
});
</script>
@endpush
