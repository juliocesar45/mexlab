@extends('layouts.app', ['activePage' => 'detalle_factura', 'menuParent' => 'procesos', 'titlePage' => __('Detalle Factura')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('detalle_factura.update', $detallefactura) }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('put')
                        <div class="card">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-file-invoice fa-2x"></i>
                                </div>
                                <h4 class="card-title">Consultar Detalle de Factura</h4>
                            </div>
                            @include('detalle_facturas.layout.form',['show' => true,'edit' => false, 'create' => false])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
