<div class="card-body ">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="{{ route('factura.index') }}" class="btn btn-sm btn-success">{{ __('Regresar a las facturas') }}</a>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">ID Factura</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('factura_id') ? ' has-danger' : '' }}">
                <input type="text"   id="factura_id" name="factura_id" aria-label="Factura" value={{$id_factura}} readonly @if ($show) disabled @endif/>
            </div>
        </div>
    </div>
    {{--
    <div class="row">
        <label class="col-sm-2 col-form-label">Producto</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('tipo_factura_id') ? ' has-danger' : '' }}">
                <select class="select2" name="tipo_factura_id" aria-label="Producto" @if ($show) disabled @endif>
                    @foreach (App\Producto::all(['id', 'nombre']) as $tipo_factura)
                        @if ($create)
                            <option value="{{ $tipo_factura->id }}">{{ $tipo_factura->nombre }}</option>
                        @else
                            @if ($tipo_factura->id == $factura->tipo_factura_id)
                                <option value="{{ $tipo_factura->id }}" selected>{{ $tipo_factura->nombre }}</option>
                            @else
                                <option value="{{ $tipo_factura->id }}">{{ $tipo_factura->nombre }}</option>
                            @endif
                        @endif

                    @endforeach
                </select>
            </div>
        </div>
    </div>
    --}}
    <div class="row">
        <label class="col-sm-2 col-form-label">Cantidad</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('cantidad') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('cantidad') ? ' is-invalid' : '' }}" name="cantidad"
                    id="cantidad" type="text" number="true" placeholder="Cantidad" @if ($create) value="{{ old('cantidad') }}" @else value="{{ $detalleFactura->cantidad, old('cantidad') }}" @endif required="true" number="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'cantidad'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Notas</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('notas') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('notas') ? ' is-invalid' : '' }}" name="notas"
                    id="notas" type="text" number="true" placeholder="Notas" @if ($create) value="{{ old('notas') }}" @else value="{{ $detalleFactura->notas, old('notas') }}" @endif required="true" number="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'notas'])
            </div>
        </div>
    </div>
</div>
<div class="card-footer ml-auto mr-auto">
    @if ($create)
        <button type="submit" class="btn btn-success" onClick="reload">Crear</button>
    @endif
    @if ($edit)
        <button type="submit" class="btn btn-success">Editar</button>
    @endif
    @if ($show)
        <form action="{{ route('detalle_factura.destroy', $detalleFactura->id) }}" method="post">
            @csrf
            @method('delete')
            <a rel="tooltip" class="btn btn-success" href="{{ route('detalle_factura.edit', $detalleFactura->id) }}"
                data-original-title="" title=""> Editar
            </a>

            <input type="submit" class="btn btn-danger" value="Eliminar">
        </form>
    @endif
</div>
