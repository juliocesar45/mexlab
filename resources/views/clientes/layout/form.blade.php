    <div class="card-body ">
        <div class="row">
            <div class="col-md-12 text-right">
                <a href="{{ route('cliente.index') }}"
                    class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Nombre</label>
            <div class="col-sm-2">
                <div class="form-group{{ $errors->has('nombres') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('nombres') ? ' is-invalid' : '' }}" name="nombres"
                        id="nombres" type="text" placeholder="Nombre" @if ($create) value="{{ old('nombres') }}" @else value="{{ $cliente->nombres, old('nombres') }}" @endif @if ($show) disabled @endif required="true"
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'nombres'])
                </div>
            </div>
            <label class="col-sm-2 col-form-label">Apellido Paterno</label>
            <div class="col-sm-2">
                <div class="form-group{{ $errors->has('ap_paterno') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('ap_paterno') ? ' is-invalid' : '' }}" name="ap_paterno"
                        id="ap_paterno" type="text" placeholder="Apellido Paterno" @if ($create) value="{{ old('ap_paterno') }}" @else value="{{ $cliente->ap_paterno, old('ap_paterno') }}" @endif @if ($show) disabled @endif required="true"
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'ap_paterno'])
                </div>
            </div>
            <label class="col-sm-2 col-form-label">Apellido Materno</label>
            <div class="col-sm-2">
                <div class="form-group{{ $errors->has('ap_materno') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('ap_materno') ? ' is-invalid' : '' }}"
                        name="ap_materno" id="ap_materno" type="text" placeholder="Apellido Materno" @if ($create) value="{{ old('ap_materno') }}" @else value="{{ $cliente->ap_materno, old('ap_materno') }}" @endif @if ($show) disabled @endif required="true"
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'ap_materno'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">RFC</label>
            <div class="col-sm-2">
                <div class="form-group{{ $errors->has('rfc') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('rfc') ? ' is-invalid' : '' }}" name="rfc" id="rfc"
                        type="text" placeholder="RFC" @if ($create) value="{{ old('rfc') }}" @else value="{{ $cliente->rfc, old('rfc') }}" @endif @if ($show) disabled @endif required="true"
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'rfc'])
                </div>
            </div>
            <label class="col-sm-2 col-form-label">Domicilio Fiscal</label>
            <div class="col-sm-2">
                <div class="form-group{{ $errors->has('domicilio_fiscal') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('domicilio_fiscal') ? ' is-invalid' : '' }}"
                        name="domicilio_fiscal" id="domicilio_fiscal" type="text" placeholder="Domicilio Fiscal" @if ($create) value="{{ old('domicilio_fiscal') }}" @else value="{{ $cliente->domicilio_fiscal, old('domicilio_fiscal') }}" @endif @if ($show) disabled @endif required="true"
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'domicilio_fiscal'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Domicilio</label>
            <div class="col-sm-2">
                <div class="form-group{{ $errors->has('domicilio') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('domicilio') ? ' is-invalid' : '' }}" name="domicilio"
                        id="domicilio" type="text" placeholder="Domicilio" @if ($create) value="{{ old('domicilio') }}" @else value="{{ $cliente->domicilio, old('domicilio') }}" @endif @if ($show) disabled @endif
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'domicilio'])
                </div>
            </div>
            <label class="col-sm-2 col-form-label">Códgio postal</label>
            <div class="col-sm-2">
                <div class="form-group{{ $errors->has('codigo_postal') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('codigo_postal') ? ' is-invalid' : '' }}" name="codigo_postal"
                        id="codigo_postal" type="text" placeholder="Códgio postal" @if ($create) value="{{ old('codigo_postal') }}" @else value="{{ $cliente->codigo_postal, old('codigo_postal') }}" @endif @if ($show) disabled @endif
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'codigo_postal'])
                </div>
            </div>
            <label class="col-sm-2 col-form-label">Colonia</label>
            <div class="col-sm-2">
                <div class="form-group{{ $errors->has('colonia') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('colonia') ? ' is-invalid' : '' }}" name="colonia"
                        id="colonia" type="text" placeholder="Colonia" @if ($create) value="{{ old('colonia') }}" @else value="{{ $cliente->colonia, old('colonia') }}" @endif @if ($show) disabled @endif
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'colonia'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Ciudad</label>
            <div class="col-sm-2">
                <div class="form-group{{ $errors->has('ciudad') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('ciudad') ? ' is-invalid' : '' }}" name="ciudad"
                        id="ciudad" type="text" placeholder="Ciudad" @if ($create) value="{{ old('ciudad') }}" @else value="{{ $cliente->ciudad, old('ciudad') }}" @endif @if ($show) disabled @endif
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'ciudad'])
                </div>
            </div>
            <label class="col-sm-2 col-form-label">Estado</label>
            <div class="col-sm-2">
                <div class="form-group{{ $errors->has('estado') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" name="estado"
                        id="estado" type="text" placeholder="Estado" @if ($create) value="{{ old('estado') }}" @else value="{{ $cliente->estado, old('estado') }}" @endif @if ($show) disabled @endif
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'estado'])
                </div>
            </div>
            <label class="col-sm-2 col-form-label">País</label>
            <div class="col-sm-2">
                <div class="form-group{{ $errors->has('pais') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('pais') ? ' is-invalid' : '' }}" name="pais"
                        id="pais" type="text" placeholder="País" @if ($create) value="{{ old('pais') }}" @else value="{{ $cliente->pais, old('pais') }}" @endif @if ($show) disabled @endif
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'pais'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Teléfonos</label>
            <div class="col-sm-2">
                <div class="form-group{{ $errors->has('telefonos') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('telefonos') ? ' is-invalid' : '' }}" name="telefonos"
                        id="telefonos" type="text" placeholder="Teléfonos" @if ($create) value="{{ old('telefonos') }}" @else value="{{ $cliente->telefonos, old('telefonos') }}" @endif @if ($show) disabled @endif
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'telefonos'])
                </div>
            </div>
        </div>
    </div>

    <div class="card-footer ml-auto mr-auto">
        @if ($create)
            <button type="submit" class="btn btn-success">Crear</button>
        @endif
        @if ($edit)
            <button type="submit" class="btn btn-success">Editar</button>
        @endif
        @if ($show)
            <form action="{{ route('cliente.destroy', $cliente->id) }}" method="post">
                @csrf
                @method('delete')
                <a rel="tooltip" class="btn btn-success" href="{{ route('cliente.edit', $cliente->id) }}"
                    data-original-title="" title=""> Editar
                </a>

                <button type="button" class="btn btn-danger" data-original-title="" title="" onclick="            Swal.fire({
                          title: 'Estás seguro?',
                          text: 'Se borrará el registro!',
                          type: 'warning',
                          showCancelButton: true,
                          buttonsStyling: false, 
                          confirmButtonClass: 'btn btn-danger',
                          cancelButtonClass: 'btn btn-success',
                          confirmButtonText: 'Si, borralo!',
                          cancelButtonText: 'No, quiero conservarlo!'
                      }).then((result) => {
                          if (result.value) {
                              this.parentElement.submit()
                          }
                      })">
                      Eliminar
                </button>
            </form>
        @endif
    </div>
    </div>
