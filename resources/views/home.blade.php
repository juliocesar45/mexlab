@extends('layouts.app', [
'class' => 'off-canvas-sidebar',
'classPage' => 'login-page',
'activePage' => 'dashboard',
'title' => 'Mexlab',
'pageBackground' => asset("public/material").'/img/login.png',
'titlePage' => 'Facturas del día',
'menuParent' => 'dashboard',
])

@section('content')
<div style = "display:none">
    <label id = "idAuxFactura" > </label>
</div>
    <div class="content">
        <div class="container-fluid">
            <div class="row" style="display:none">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="card card-stats">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-file-invoice-dollar"></i>
                            </div>
                            <p class="card-category">Facturas Emitidas</p>
                            <h3 id="facturasHoy" class="card-title">0</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">update</i><span class="fechaActualizacion"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-file-invoice"></i>
                            </div>
                            <p class="card-category">Facturas Surtidas</p>
                            <h3 id="surtidoHoy" class="card-title">0</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">update</i><span class="fechaActualizacion"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="card card-stats">
                        <div class="card-header card-header-danger card-header-icon">
                            <div class="card-icon">
                                <i class="far fa-window-close"></i>
                            </div>
                            <p class="card-category">Facturas Canceladas</p>
                            <h3 id="cancelada" class="card-title">0</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">update</i><span class="fechaActualizacion"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header card-header-icon card-header-info">
                            <div class="card-icon">
                                <i class="fas fa-chart-pie fa-2x"></i>
                            </div>
                            <h4 class="card-title">Proceso de recolección</h4>
                        </div>
                        <div class="card-body">
                            <div id="grafica_pie" class="ct-chart"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header card-header-icon card-header-rose">
                            <div class="card-icon">
                                <i class="material-icons fa-2x">insert_chart</i>
                            </div>
                            <h4 class="card-title">Proceso de facturas del día</h4>
                        </div>
                        <div class="card-body">
                            <div id="grafica_barra" class="ct-chart ct-bar"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-file-invoice-dollar fa-2x"></i>
                            </div>
                            <h4 class="card-title">Lista de facturas del día</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="tabla_facturas" class="table table-striped table-no-bordered table-hover"
                                    style="width:100%">
                                    <thead class="text-primary">
                                        <th class="desktop">Clave</th>
                                        <th class="desktop">Cliente</th>
                                        <th class="desktop">Agente</th>
                                        <th class="desktop">Almacen</th>
                                        <th class="desktop">Estatus Factura</th>
                                        <th class="desktop">Tiempo</th>
                                        <th class="desktop">Fecha de subida</th>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $(document).ready(function() {
            updateValues()
            /* Tabla de facturas */
            $('#tabla_facturas').fadeIn(1100);

            table = $('#tabla_facturas').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [-1],
                    ["Todos"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Buscar Facturas",
                },
                scrollX: true,
                processing: true,
                serverSide: true,
                ordering: false,
                ajax: "{{ route('consulta_factura') }}",
                "columns": [{

                        title: "Clave",
                        data: "identificador",
                        // name: "identificador",
                        render: function(data, type, row) {
                            let  url = "{{ route('facturas.show',['factura'=>'identificador'])}}";
                            url = url.replace('identificador', data);
                            return '<a href="'+url+'">'+data+'</a>';
                            // if (data == 'Surtido')
                            //     return '<span class="badge badge-default" style="background-color:#08b31c">Surtido</span>'
                            // if (data == 'Por surtir')
                            //     return '<span class="badge badge-default" style="background-color:#fc8605">Por Surtir</span>'
                            // else
                            //     return '<span class="badge badge-default" style="background-color:#fc1105">Cancelada</span>'
                        }
                    },
                    {
                        title: "Cliente",
                        data: "cliente",
                        name: "cliente"
                    },
                    {
                        title: "Agente",
                        data: "agente",
                        name: "agente"
                    },
                    {
                        title: "Almacen",
                        data: "almacen",
                        name: "almacen"
                    },
                    {
                        title: "Estatus",
                        data: "estatus_factura",
                        render: function(data, type, row) {
                            if (data == 'Surtido')
                                return '<span class="badge badge-default" style="background-color:#08b31c">Surtido</span>'
                            if (data == 'Por surtir')
                                return '<span class="badge badge-default" style="background-color:#fc8605">Por Surtir</span>'
                            else
                                return '<span class="badge badge-default" style="background-color:#fc1105">Cancelada</span>'
                        }
                    },
                    {
                        title: "Tiempo",
                        data: "tiempo",
                        name: "tiempo"
                    },
                    {
                        title: "Fecha de subida",
                        data: "fecha_hora",
                        name: "fecha_hora"
                    }
                    
                ]
            });
            grafica()
            graficapie()
        });

        /* Actualización automatica de valores */
        function updateValues() {
            /* actualización de datos */

            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('consulta') }}",
                type: 'GET',
                dataType: "JSON",
                data: {
                    "_token": token,
                },
                success: function(response) {
                    $(".fechaActualizacion").empty();
                    $(".fechaActualizacion").append(response.fecha);
                    $("#facturasHoy").empty();
                    $('#facturasHoy').append(response.facturas_emitidas);
                    $("#surtidoHoy").empty();
                    $('#surtidoHoy').append(response.facturas_surtidas);
                    $("#cancelada").empty();
                    $('#cancelada').append(response.facturas_canceladas);
                    
                }
            });
        }

        function UpdateAux(){
            /* actualización de datos auxiliares */
             var token = "{{ csrf_token() }}";
           
            $.ajax({
                url: "{{ route('consulta') }}",
                type: 'GET',
                dataType: "JSON",
                data: {
                    "_token": token,
                    
                },
                success: function(response) {
                    var AuxCountFactura = $("#idAuxFactura").text();     
                    $("#idAuxFactura").empty();
                    $("#idAuxFactura").append(response.facturas_emitidas);
                    
                    if((response.facturas_emitidas > AuxCountFactura) && (AuxCountFactura>=0  ))  {
                        AuxCountFactura =response.facturas_emitidas;
                        var sonido = new Audio("./docs/Audio/alert.mp3");
                        sonido.play();
                    }

                    
                }
            });

        }

        /*Actualización de gráfica automatica*/
        function grafica() {
            var dataSimpleBarChart = {
                labels: ['Facturas emitdas', 'Facturas surtidas', 'Facturas canceladas'],
                series: [
                    [$("#facturasHoy").text(), $("#surtidoHoy").text(), $("#cancelada").text()]
                ]
            };

            var optionsSimpleBarChart = {
                seriesBarDistance: 10,
            };

            var responsiveOptionsSimpleBarChart = [
                ['screen and (max-width: 640px)', {
                    seriesBarDistance: 5,
                    axisX: {
                        labelInterpolationFnc: function(value) {
                            return value[0];
                        }
                    }
                }]
            ];

            var simpleBarChart = Chartist.Bar('#grafica_barra', dataSimpleBarChart, optionsSimpleBarChart,
                responsiveOptionsSimpleBarChart);
        }
        /*Actualización de gráfica pie automaticas*/
        function graficapie() {

            /*Actualización de datos grafica pie*/
            var token = "{{ csrf_token() }}"
            $.ajax({
                url: "{{ route('consulta_pie') }}",
                type: 'GET',
                dataType: "JSON",
                data: {
                    "_token": token,
                },
                success: function(response) {
                    var data = {
                        labels: response.recolector,
                        series: response.facturas_surtidas
                    };


                    var options = {
                        labelInterpolationFnc: function(value) {
                            return value[0]
                        }
                    };

                    var responsiveOptions = [
                        ['screen and (min-width: 640px)', {
                            chartPadding: 30,
                            labelOffset: 100,
                            labelDirection: 'explode',
                            labelInterpolationFnc: function(value) {
                                return value;
                            }
                        }],
                        ['screen and (min-width: 640px)', {
                            labelOffset: 80,
                            chartPadding: 20
                        }]
                    ];

                    new Chartist.Pie('#grafica_pie', data, options, responsiveOptions);
                }
            });


        }

        /* Actualización de datos automatica */
        setInterval(function() {
            updateValues()
        }, 5000);

        //Reload de la tabla de infracciones
        setInterval(function() {
            table.ajax.reload();
        }, 10000);

        //Reload de la grafica
        setInterval(function() {
            grafica();
        }, 5000);

        //Reload de la grafica2
        setInterval(function() {
            graficapie();
        }, 5000);

        //Reload del label auxiliar
        setInterval(function(){
            UpdateAux();
        },5000);

    </script>
@endpush
