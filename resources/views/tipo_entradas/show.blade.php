@extends('layouts.app', ['activePage' => 'entrada', 'menuParent' => 'catalogos', 'titlePage' => __('Tipo Entrada')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" enctype="multipart/form-data" action="{{ route('tipo_entrada.update', $tipoEntrada) }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')

            <div class="card ">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                  <i class="fas fa-shipping-fast fa-2x"></i>
                </div>
                <h4 class="card-title">Consulta Tipo Entrada</h4>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('tipo_entrada.index') }}" class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">Nombre</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" id="input-name" type="text" placeholder="Nombre" value="{{ $tipoEntrada->nombre }}" required="true" aria-required="true" disabled/>
                      @include('alerts.feedback', ['field' => 'nombre'])
                    </div>
                  </div>
                </div>

              <div class="card-footer ml-auto mr-auto">
                <form action="{{ route('tipo_entrada.destroy', $tipoEntrada->id) }}" method="post">
                    @csrf
                    @method('delete')
                    <a rel="tooltip" class="btn btn-success" href="{{ route('tipo_entrada.edit', $tipoEntrada->id) }}" data-original-title="" title=""> Editar
                    </a>
                    
                    <input type="submit" class="btn btn-danger" value="Eliminar">
                </form>
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
