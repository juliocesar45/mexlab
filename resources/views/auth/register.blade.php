@extends('layouts.app', [
  'class' => 'off-canvas-sidebar',
  'classPage' => 'register-page',
  'activePage' => 'register',
  'title' => __('Mexlab'),
  'pageBackground' => asset("public/material").'/img/login.png'
])

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 ml-auto mr-auto">
      <div class="card card-signup">
        <h2 class="card-title text-center">{{ __('Registro de usuario') }}</h2>
        <div class="card-body">
          <div class="row">
            <div class="col-md-5 ml-auto">
              <div class="info info-horizontal">
                <div class="description">
                  <img src="{{asset('public/material')}}/img/mexlab.png" alt="mexlab" class="img-fluid">
                </div>
              </div>
            </div>
            <div class="col-md-5 mr-auto">
              <form class="form" method="POST" action="{{ route('register') }}">
                @csrf

                <div class="has-default{{ $errors->has('name') ? ' has-danger' : '' }} mb-3">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">face</i>
                      </span>
                    </div>
                    <input type="text" name="name" class="form-control" placeholder="{{ __('Nombre...') }}" value="{{ old('name') }}" required>
                    @if ($errors->has('name'))
                      <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                        <strong class="errors-field-name">{{ $errors->first('name') }}</strong>
                      </div>
                     @endif
                  </div>
                </div>
                <div class="has-default{{ $errors->has('email') ? ' has-danger' : '' }} mb-3">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">mail</i>
                      </span>
                    </div>
                    <input type="text" class="form-control" name="email" placeholder="{{ __('Correo...') }}" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                      <div id="email-error" class="error text-danger pl-3" for="name" style="display: block;">
                        <strong class="errors-field-email">{{ $errors->first('email') }}</strong>
                      </div>
                     @endif
                  </div>
                </div>
                <div class="has-default{{ $errors->has('email') ? ' has-danger' : '' }} mb-3">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">recent_actors</i>
                      </span>
                    </div>
                      <select class="selectpicker" style="display:inline-block" name="user_type" data-style="select-with-transition" title="" data-size="100">
                        <option value="" selected hidden>{{ __('Rol del usuario') }}</option>
                        <option value="1" @if (old('user_type') == '1') selected="selected" @endif>{{ __('Admin') }}</option>
                        <option value="2" @if (old('user_type') == '2') selected="selected" @endif>{{ __('Creator') }}</option>
                        <option value="3" @if (old('user_type') == '3') selected="selected" @endif>{{ __('Member') }}</option>
                      </select>
                      @if ($errors->has('user_type'))
                        <div id="email-error" class="error text-danger pl-3" for="name" style="display: block;">
                          <strong class="errors-field-user">{{ $errors->first('user_type') }}</strong>
                        </div>
                      @endif
                  </div>
                </div>
                <div class="has-default{{ $errors->has('password') ? ' has-danger' : '' }} mb-3">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">lock_outline</i>
                      </span>
                    </div>
                    <input type="password" name="password" placeholder="{{ __('Contraseña...') }}" class="form-control" required>
                    @if ($errors->has('password'))
                      <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                        <strong class="errors-field-pass">{{ $errors->first('password') }}</strong>
                      </div>
                     @endif
                  </div>
                </div>
                <div class="has-default mb-3">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">lock_outline</i>
                      </span>
                    </div>
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{{ __('Confirma la contraseña...') }}" required>
                  </div>
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-primary btn-round mt-4">{{ __('Registrar') }}</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      md.checkFullPageBackgroundImage();
    });
  </script>
@endpush
