@extends('layouts.app', ['activePage' => 'Mensajero estatus ', 'menuParent' => 'procesos', 'titlePage' => __('Mensajero Estatus')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                 <i class="far fa-address-card fa-2x"></i>
                </div>
                <h4 class="card-title">Mensajero Estatus</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="datatables" class="table table-striped table-no-bordered table-hover" style="display:none;width:100%">
                    <thead class="text-primary">
                      <th class="desktop">
                      Fecha
                      </th>
                      <th class="desktop">
                      Cliente
                      </th>
                      <th class="desktop">
                      Factura
                      </th>
                      <th class="desktop">
                      Guia
                      </th> 
                      <th class="desktop">
                      Estatus 
                      </th> 
                      @can('manage-users', App\User::class)
                        <th class="text-right desktop">
                          {{ __('Acciones') }}
                        </th>
                      @endcan
                    </thead>
                    <tbody>
                      @foreach($salidas as $salida)
                        <tr>
                          <td>{{\Carbon\Carbon::parse($salida->fecha)->format('d/m/Y H:i')}}</td>
                          <td>{{$salida->cliente->nombre_completo ?? ''}}</td>
                          <td>{{$salida->factura->identificador ?? ''}}</td>
                          <td>{{$salida->guia->guia ?? ''}}</td> 
                          <td>
                            @if ($salida->factura->estatus_factura->nombre=='Surtido')
                            <span class="badge badge-default" style="background-color:#08b31c">Surtido</span>
                            @elseif ($salida->factura->estatus_factura->nombre=='Por surtir')
                            <span class="badge badge-default" style="background-color:#fc8605">Por Surtir</span>
                            @elseif($salida->factura->estatus_factura->nombre=="Cancelada")
                            <span class="badge badge-default" style="background-color:#e80303">Cancelada</span>
                            @else
                            <span class="badge badge-default" style="background-color:#6b02ab">Factura entregada al cliente</span>
                            @endif
                          </td> 
                          <td class="td-actions text-right">
                              <form action="{{ route('salida.destroy', $salida->id) }}" method="post">
                                  @csrf
                                  @method('delete')
                                  <!-- Button trigger modal -->
                                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onclick="mostrar({{$salida->id}})">
                                    <i class="fas fa-parachute-box"></i>
                                  </button>

                                  <a rel="tooltip" class="btn btn-warning btn-link" href="{{ route('salida.show', $salida->id) }}" data-original-title="" title="">
                                    <i class="material-icons">visibility</i>
                                    <div class="ripple-container"></div>
                                  </a>
                              </form>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                  <div class="modal-content">
                      <div class="modal-header">
                          <div class="card-icon">
                              <i class="fas fa-tags fa-2x" style="color:purple"></i>
                          </div>
                      <h5 class="modal-title" id="exampleModalLabel">Estatus Mensajero</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      </div>
                      <div class="modal-body">
                      <form method="post" action="{{route('salida.estatus.mensajero')}}" class="form-horizontal" enctype="multipart/form-data">
                          @csrf
                          @method('post')
                          <div class="row">
                            <div class="col-12">
                              <div class="col-6">
                                <div class="form-group">
                                  <select class="selectpicker col-sm-12 pl-0 pr-0" name="estatus" data-style="select-with-transition" title="" data-size="100">
                                    <option value="ENTREGADO" >{{ __('ENTREGADO') }}</option>
                                    <option value="NO ENTREGADO" >{{ __('NO ENTREGADO') }}</option>
                                    <option value="CANCELADO" >{{ __('CANCELADO') }}</option>
                                  </select>
                                </div>
                              </div>
                              <div class="col-6">
                                <textarea type="text" id="nota" name="nota" class="form-control" rows="3" placeholder="Escribe nota aquí..."></textarea>
                            </div>
                              <input type="hidden" name="id_salida" id="id_salida">
                          </div>
                      </div> 
                      <div class="row">
                        <div class="col-12 d-flex justify-content-center mt-4">
                            <button type="submit" class="btn btn-success">Guardar</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                      </div>
                      </form>
                  </div>
                  </div>
              </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables').fadeIn(1100);
      $('#datatables').DataTable({
        "order": [[ 0, "desc" ]],
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "Todos"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Buscar salidas",
        },
        "columnDefs": [
          { "orderable": false, "targets": 5 },
        ],
      });
    });

    function mostrar (id)
    {
      $("#id_salida").val(id);
    }
  </script>
@endpush