@extends('layouts.app', ['activePage' => 'Asignación mensajero', 'menuParent' => 'procesos', 'titlePage' =>
__('Asignación de salida')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-user-check fa-2x"></i>
                            </div>
                            <h4 class="card-title">Asignación de salida</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                    style="display:none;width:100%">
                                    <thead class="text-primary">
                                        <th class="desktop">
                                            Fecha
                                        </th>
                                        <th class="desktop">
                                            Cliente
                                        </th>
                                        <th class="desktop">
                                            Factura
                                        </th>
                                        <th class="desktop">
                                            Guia
                                        </th>
                                        <th class="desktop">
                                            Estatus
                                        </th>
                                        @can('manage-users', App\User::class)
                                            <th class="text-right desktop">
                                                {{ __('Acciones') }}
                                            </th>
                                        @endcan
                                    </thead>
                                    <tbody>
                                        @foreach ($salidas as $salida)
                                            <tr>
                                                <td>{{ \Carbon\Carbon::parse($salida->fecha)->format('d/m/Y H:i') }}
                                                </td>
                                                <td>{{ $salida->cliente->nombre_completo ?? '' }}</td>
                                                <td>{{ $salida->factura->identificador ?? '' }}</td>
                                                <td>{{ $salida->guia->guia ?? '' }}</td>
                                                <td>
                                                    @if ($salida->factura->estatus_factura->nombre == 'Embarque completo')
                                                        <span class="badge badge-default"
                                                            style="background-color:#0292ab">Embarque completo</span>
                                                    @endif
                                                </td>
                                                <td class="td-actions text-right">
                                                    <form action="{{ route('salida.destroy', $salida->id) }}"
                                                        method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <!-- Button trigger modal -->
                                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                            data-target="#exampleModal"
                                                            onclick="mostrar({{ $salida->id }})">
                                                            <i class="fas fa-user-plus"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <div class="card-icon">
                                                <i class="fas fa-tags fa-2x" style="color:purple"></i>
                                            </div>
                                            <h5 class="modal-title" id="exampleModalLabel">Asignación</h5>
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action="{{ route('salida.asignacion.mensajero') }}"
                                                class="form-horizontal" enctype="multipart/form-data">
                                                @csrf
                                                @method('post')
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <div
                                                                    class="form-group{{ $errors->has('mensajero_id') ? ' has-danger' : '' }}">
                                                                    <select class="select2" name="mensajero_id"
                                                                        aria-label="Mensajero" style="width: 100%">
                                                                        @foreach (App\Mensajero::all(['id', 'nombre']) as $mensajero)
                                                                            <option value="{{ $mensajero->id }}">
                                                                                {{ $mensajero->nombre }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="id_salida" id="id_salida">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 d-flex justify-content-center mt-4">
                                                        <button type="submit" class="btn btn-success">Guardar</button>
                                                        <button type="button" class="btn btn-danger"
                                                            data-dismiss="modal">Cerrar</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @push('js')
        <script>
            $(document).ready(function() {
                $('#datatables').fadeIn(1100);
                $('#datatables').DataTable({
                    "order": [
                        [0, "desc"]
                    ],
                    "pagingType": "full_numbers",
                    "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "Todos"]
                    ],
                    responsive: true,
                    language: {
                        search: "_INPUT_",
                        searchPlaceholder: "Buscar salidas",
                    },
                    "columnDefs": [{
                        "orderable": false,
                        "targets": 5
                    }, ],
                });
            });

            function mostrar(id) {
                $("#id_salida").val(id);
            }
        </script>
    @endpush
