<div class="card-body ">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="{{ route('salida.index') }}" class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
        </div>
    </div>
    @if ($show && $salida->nota)
    <div class="row mt-4">
        <label class="col-sm-2 col-form-label"></label>
        <div class="col-sm-7">
            <div class="alert alert-warning" role="alert" style="width: 100%;">
                Esta salida tiene una nota
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <label class="col-sm-2 col-form-label">Fecha</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('fecha') ? ' has-danger' : '' }}">
                <input type="text" data-date-format="YYYY-MM-DD HH:mm"
                    class="form-control datetimepicker {{ $errors->has('fecha') ? ' is-invalid' : '' }}" name="fecha"
                    id="fecha" placeholder="fecha" @if ($create) value="{{ \Carbon\Carbon::now() }}" @else value="{{ $salida->fecha, old('fecha') }}" @endif required="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'fecha'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Tipo Salida</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('tipo_salida_id') ? ' has-danger' : '' }}">
                <select class="select2" name="tipo_salida_id" aria-label="TipoSalida" @if ($show) disabled @endif>
                    @foreach (App\TipoSalida::all(['id', 'nombre']) as $tipoSalida)
                        @if ($create)
                            <option value="{{ $tipoSalida->id }}">{{ $tipoSalida->nombre }}</option>
                        @else
                            @if ($tipoSalida->id == $salida->tipo_salida_id)
                                <option value="{{ $tipoSalida->id }}" selected>{{ $tipoSalida->nombre }}</option>
                            @else
                                <option value="{{ $tipoSalida->id }}">{{ $tipoSalida->nombre }}</option>
                            @endif
                        @endif
  
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Clientes</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('cliente_id') ? ' has-danger' : '' }}">
                <select class="select2" name="cliente_id" aria-label="Cliente" @if ($show) disabled @endif>
                    @foreach (App\Cliente::all(['id', 'nombre_completo']) as $clientes)
                        @if ($create)
                            <option value="{{ $clientes->id }}">{{ $clientes->nombre_completo }}</option>
                        @else
                            @if ($clientes->id == $salida->cliente_id)
                                <option value="{{ $clientes->id }}" selected>{{ $clientes->nombre_completo }}</option>
                            @else
                                <option value="{{ $clientes->id }}">{{ $clientes->nombre_completo }}</option>
                            @endif
                        @endif
  
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Facturas</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('factura_id') ? ' has-danger' : '' }}">
                <select class="select2" name="factura_id" aria-label="Factura" @if ($show) disabled @endif>
                    @foreach (App\Factura::all(['id','identificador']) as $facturas)
                        @if ($create)
                            <option value="{{ $facturas->id }}">{{ $facturas->identificador}}</option>
                        @else
                            @if ($facturas->id == $salida->factura_id)
                                <option value="{{ $facturas->id }}" selected>{{ $facturas->identificador}}</option>
                            @else
                                <option value="{{ $facturas->id }}">{{ $facturas->identificador}}</option>
                            @endif
                        @endif
  
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Guía</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('guia_id') ? ' has-danger' : '' }}">
                <select class="select2" name="guia_id" aria-label="Factura" @if ($show) disabled @endif>
                    @foreach (App\Guia::all(['id','guia']) as $guias)
                        @if ($create)
                            <option value="{{ $guias->id }}">{{ $guias->guia}}</option>
                        @else
                            @if ($guias->id == $salida->guia_id)
                                <option value="{{ $guias->id }}" selected>{{ $guias->guia}}</option>
                            @else
                                <option value="{{ $guias->id }}">{{ $guias->guia}}</option>
                            @endif
                        @endif
  
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Notas</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('notas') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('notas') ? ' is-invalid' : '' }}" name="notas"
                    id="notas" type="text" placeholder="notas" @if ($create) value="{{ old('notas') }}" @else value="{{ $salida->notas, old('notas') }}" @endif @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'notas'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Ruta de Embarque</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('ruta_embarque_id') ? ' has-danger' : '' }}">
                <select class="select2" name="ruta_embarque_id" aria-label="Ruta Embarque" @if ($show) disabled @endif>
                    @foreach (App\RutaEmbarque::all(['id','direccion_factura']) as $ruta_embarques)
                        @if ($create)
                            <option value="{{ $ruta_embarques->id }}">{{ $ruta_embarques->direccion_factura}}</option>
                        @else
                            @if ($ruta_embarques->id == $salida->ruta_embarque_id)
                                <option value="{{ $ruta_embarques->id }}" selected>{{ $ruta_embarques->direccion_factura}}</option>
                            @else
                                <option value="{{ $ruta_embarques->id }}">{{ $ruta_embarques->direccion_factura}}</option>
                            @endif
                        @endif
  
                    @endforeach
                </select>
            </div>
        </div>
    </div> 
</div>
    <div class="card-footer ml-auto mr-auto">
        @if ($create)
            <button type="submit" class="btn btn-success">Crear</button>
        @endif
        @if ($edit)
            <button type="submit" class="btn btn-success">Editar</button>
        @endif
    </div>
  