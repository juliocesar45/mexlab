@extends('layouts.app', ['activePage' => 'salida', 'menuParent' => 'procesos', 'titlePage' => __('Agregar salida')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('salida.store') }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        <div class="card">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-door-closed fa-2x"></i>
                                </div>
                                <h4 class="card-title">Agregar Salida</h4>
                            </div>
                            @include('salidas.layout.form',['show' => false,'edit' => false, 'create' => true])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
