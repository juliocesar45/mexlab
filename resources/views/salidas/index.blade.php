@extends('layouts.app', ['activePage' => 'salida', 'menuParent' => 'procesos', 'titlePage' => __('Salidas')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                  <i class="fas fa-door-closed fa-2x"></i>
                </div>
                <h4 class="card-title">Salidas</h4>
              </div>
              <div class="card-body">
                @can('create', App\User::class)
                  <div class="row">
                    <div class="col-12 text-right">
                      <a href="{{ route('salida.create') }}" class="btn btn-sm btn-success">Agregar Salida</a>
                    </div>
                  </div>
                @endcan
                <div class="table-responsive">
                  <table id="datatables" class="table table-striped table-no-bordered table-hover" style="display:none;width:100%">
                    <thead class="text-primary">
                      <th class="desktop">
                      Fecha
                      </th>
                      <th class="desktop">
                      Tipo Salida
                      </th>
                      <th class="desktop">
                      Cliente
                      </th>
                      <th class="desktop">
                      Factura
                      </th>
                      <th class="desktop">
                      Guia
                      </th> 
                      <th class="desktop">
                      Resultado de envio
                      </th>
                      <th class="desktop">
                      Ruta de Embarque
                      </th> 
                      @can('manage-users', App\User::class)
                        <th class="text-right desktop">
                          {{ __('Acciones') }}
                        </th>
                      @endcan
                    </thead>
                    <tbody>
                      @foreach($salidas as $salida)
                        <tr>
                          <td>{{\Carbon\Carbon::parse($salida->fecha)->format('d/m/Y H:i')}}</td>
                          <td>{{$salida->tipo_salida->nombre ?? ''}}</td>
                          <td>{{$salida->cliente->nombre_completo ?? ''}}</td>
                          <td>{{$salida->factura->identificador ?? ''}}</td>
                          <td>{{$salida->guia->guia ?? ''}}</td> 
                          <td>{{$salida->resultado_envio}}</td>
                          <td>{{$salida->ruta_embarque->direccion_factura ?? ''}}</td> 
                          <td class="td-actions text-right">
                              <form action="{{ route('salida.destroy', $salida->id) }}" method="post">
                                  @csrf
                                  @method('delete')
                                  <a rel="tooltip" class="btn btn-warning btn-link" href="{{ route('salida.show', $salida->id) }}" data-original-title="" title="">
                                    <i class="material-icons">visibility</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('salida.edit', $salida->id) }}" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="Swal.fire({
                                    title: 'Estás seguro?',
                                    text: 'Se borrará el registro!',
                                    type: 'warning',
                                    showCancelButton: true,
                                    buttonsStyling: false, 
                                    confirmButtonClass: 'btn btn-danger',
                                    cancelButtonClass: 'btn btn-success',
                                    confirmButtonText: 'Si, borralo!',
                                    cancelButtonText: 'No, quiero conservarlo!'
                                }).then((result) => {
                                    if (result.value) {
                                        this.parentElement.submit()
                                    }
                                })">
                                      <i class="material-icons">close</i>
                                      <div class="ripple-container"></div>
                                  </button>
                              </form>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables').fadeIn(1100);
      $('#datatables').DataTable({
        "order": [[ 0, "desc" ]],
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "Todos"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Buscar salidas",
        },
        "columnDefs": [
          { "orderable": false, "targets": 7 },
        ],
      });
    });
  </script>
@endpush