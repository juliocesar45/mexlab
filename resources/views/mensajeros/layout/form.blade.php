    <div class="card-body ">
        <div class="row">
            <div class="col-md-12 text-right">
                <a href="{{ route('mensajero.index') }}"
                    class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Nombre</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre"
                        id="nombre" type="text" placeholder="Nombre" @if($create) value="{{ old('nombre') }}" @else value="{{ $mensajero->nombre,old('nombre') }}" @endif @if ($show) disabled @endif required="true"
                        aria-required="true" />
                    @include('alerts.feedback', ['field' => 'nombre'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Teléfono</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('telefono') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }}" name="telefono"
                        id="telefono" type="text" placeholder="Teléfono" @if($create) value="{{ old('telefono') }}" @else value="{{ $mensajero->telefono,old('telefono') }}" @endif @if ($show) disabled @endif required="true"
                        aria-required="true" />
                    @include('alerts.feedback', ['field' => 'telefono'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Usuario</label>
            <div class="col-sm-7">
                <div class="form-group{{ $errors->has('user_id') ? ' has-danger' : '' }}">
                    <select class="select2" name="user_id" aria-label="Usuario" @if ($show) disabled @endif>
                        @foreach (App\User::where('mensajero',1)->get() as $user)
                            @if ($create)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @else
                                @if ($user->id == $mensajero->user_id)
                                    <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
                                @else
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endif
                            @endif
      
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        </div>

    <div class="card-footer ml-auto mr-auto">
        @if ($create)
            <button type="submit" class="btn btn-success">Crear</button>
        @endif
        @if ($edit)
            <button type="submit" class="btn btn-success">Editar</button>
        @endif
        @if ($show)
            <form action="{{ route('mensajero.destroy', $mensajero->id) }}" method="post">
                @csrf
                @method('delete')
                <a rel="tooltip" class="btn btn-success" href="{{ route('mensajero.edit', $mensajero->id) }}"
                    data-original-title="" title=""> Editar
                </a>

                <input type="submit" class="btn btn-danger" value="Eliminar">
            </form>
        @endif
    </div>
    </div>
