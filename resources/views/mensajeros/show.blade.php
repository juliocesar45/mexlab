@extends('layouts.app', ['activePage' => 'mensajero', 'menuParent' => 'catalogos', 'titlePage' => __('Mensajero')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('mensajero.update', $mensajero) }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('put')

                        <div class="card ">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="far fa-address-card fa-2x"></i>
                                </div>
                                <h4 class="card-title">Consulta Mensajero</h4>
                            </div>
                            @include('mensajeros.layout.form',['show' => true,'edit' => false, 'create' => false])
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
