<footer class="footer">
  <div class="container-fluid">
    <div class="copyright float-right">
      AutoAlmacen V 0.9.1
      &copy;
      <script>
        document.write(new Date().getFullYear())
      </script>, {{ __('hecho con') }} <i class="material-icons">favorite</i> por
      <a href="https://www.perspective.com.mx" target="_blank">Perspective Global de México</a>
    </div>
  </div>
</footer>