<div class="sidebar" data-color="green" data-background-color="black"
    data-image="{{ asset('public/material') }}/img/sidebar-2.jpg">
    <!--
    Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

    Tip 2: you can also add an image using data-image tag
-->
    <div class="logo">
        <a href="{{ route('home') }}" class="simple-text logo-mini">
            {{ __('ML') }}
        </a>
        <a href="{{ route('home') }}" class="simple-text logo-normal">
            {{ __('Mexlab') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{ asset('public/material') }}/img/user.png" />
            </div>
            <div class="user-info">
                <a data-toggle="collapse" href="#collapseExample" class="username">
                    <span>
                        {{ auth()->user()->name }}
                        <b class="caret"></b>
                    </span>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('profile.edit') }}">
                                <span class="sidebar-mini"><i class="material-icons">person</i></span>
                                <span class="sidebar-normal"> Mi Perfil </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span class="sidebar-mini"><i class="material-icons">logout</i></span>
                                <span class="sidebar-normal" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    Cerrar sesión </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
            <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="material-icons">dashboard</i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            <li class="nav-item{{ $activePage == 'Subir factura' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('facturas.upload') }}">
                    <i class="fas fa-file-invoice-dollar fa-2x"></i>
                    <p>{{ __('Subir factura') }}</p>
                </a>
            </li>
            <li class="nav-item{{ $activePage == 'Subir producto ' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('productos.upload') }}">
                    <i class="fas fa-file-upload fa-2x"></i>
                    <p>{{ __('Subir producto') }}</p>
                </a>
            </li>

            <li class="nav-item{{ $activePage == 'Mensajero estatus ' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('salidas.upload') }}">
                    <i class="far fa-address-card fa-2x"></i>
                    <p>{{ __('Mensajero estatus') }}</p>
                </a>
            </li>

            <li class="nav-item{{ $activePage == 'Asignación mensajero' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('salidas.asignacion') }}">
                    <i class="fas fa-user-check"></i>
                    <p>{{ __('Asignación de salidas') }}</p>
                </a>
            </li>

            <li class="nav-item {{ $menuParent == 'laravel' ? ' active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#laravelExample"
                    {{ $menuParent == 'laravel' || $activePage == 'dashboard' ? ' aria-expanded="true"' : '' }}>
                    <i class="material-icons">manage_accounts</i>
                    <p>{{ __('Gestión de usuarios') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $menuParent == 'laravel' ? ' show' : '' }}" id="laravelExample">
                    <ul class="nav">
                        <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('profile.edit') }}">
                                <span class="sidebar-mini"><i class="material-icons">person</i></span>
                                <span class="sidebar-normal">{{ __('Perfil de usuario') }} </span>
                            </a>
                        </li>
                        @can('manage-users', App\User::class)
                            <li class="nav-item{{ $activePage == 'role-management' ? ' active' : '' }}">
                                <a class="nav-link" href="{{ route('role.index') }}">
                                    <span class="sidebar-mini"><i class="material-icons">badge</i></span>
                                    <span class="sidebar-normal"> {{ __('Manejo de roles') }} </span>
                                </a>
                            </li>
                        @endcan
                        @can('manage-users', App\User::class)
                            <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
                                <a class="nav-link" href="{{ route('user.index') }}">
                                    <span class="sidebar-mini"><i class="material-icons">person_add</i></span>
                                    <span class="sidebar-normal"> {{ __('Administración de usuarios') }} </span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </div>
            </li>
            <li class="nav-item {{ $menuParent == 'procesos' ? ' active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#procesos"
                    {{ $menuParent == 'procesos' || $activePage == 'dashboard' ? ' aria-expanded="true"' : '' }}>
                    <i class="material-icons">donut_large</i>
                    <p>{{ __('Procesos') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $menuParent == 'procesos' ? ' show' : '' }}" id="procesos">
                    <ul class="nav">
                        <li class="nav-item{{ $activePage == 'entrada' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('entrada.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-dungeon fa-2x"></i></span>
                                <span class="sidebar-normal">Entrada</span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'producto' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('producto.index') }}">
                                <span class="sidebar-mini"><i class="fab fa-product-hunt fa-2x"></i></span>
                                <span class="sidebar-normal">Producto</span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'factura' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('factura.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-file-invoice fa-2x"></i></span>
                                <span class="sidebar-normal">Facturas</span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'salida' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('salida.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-door-closed fa-2x"></i> </span>
                                <span class="sidebar-normal">Salidas</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <!-- Consultas -->
            <li class="nav-item {{ $menuParent == 'consultas' ? ' active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#consultas"
                    {{ $menuParent == 'consultas' ? ' aria-expanded="true"' : '' }}>
                    <i class="material-icons">visibility</i>
                    <p>{{ __('Consultas') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $menuParent == 'consultas' ? ' show' : '' }}" id="consultas">
                    <ul class="nav">
                        <li class="nav-item{{ $activePage == 'visita' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('visita') }}">
                                <span class="sidebar-mini"> <i class="fas fa-search fa-2x"></i> </span>
                                <span class="sidebar-normal">Consulta de productos</span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'guia' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('guia.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-receipt fa-2x"></i> </span>
                                <span class="sidebar-normal">Guías</span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'ruta embarque' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('ruta_embarque.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-route fa-2x"></i> </span>
                                <span class="sidebar-normal">Rutas de embarque</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item {{ $menuParent == 'catalogos' ? ' active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#catalogos"
                    {{ $menuParent == 'catalogos' ? ' aria-expanded="true"' : '' }}>
                    <i class="material-icons">all_inbox</i>
                    <p>{{ __('Catálogos') }}
                        <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse {{ $menuParent == 'catalogos' ? ' show' : '' }}" id="catalogos">
                    <ul class="nav">
                        <li class="nav-item{{ $activePage == 'almacen' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('almacen.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-warehouse fa-2x"></i> </span>
                                <span class="sidebar-normal">Almacenes</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'cliente' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('cliente.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-user-tag fa-2x"></i></span>
                                <span class="sidebar-normal">Clientes</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'conductor' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('conductor.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-truck-moving fa-2x"></i> </span>
                                <span class="sidebar-normal">Conductores</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'estatus factura' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('estatus_factura.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-file-invoice fa-2x"></i> </span>
                                <span class="sidebar-normal">Estatus de Factura</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'familia producto' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('familia_producto.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-shopping-basket fa-2x"></i> </span>
                                <span class="sidebar-normal">Familias de productos</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'mensajero' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('mensajero.index') }}">
                                <span class="sidebar-mini"> <i class="far fa-address-card fa-2x"></i></span>
                                <span class="sidebar-normal">Mensajeros</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'paqueteria' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('paqueteria.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-people-carry fa-2x"></i> </span>
                                <span class="sidebar-normal">Paqueterías</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'proveedor' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('proveedor.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-parachute-box fa-2x"></i> </span>
                                <span class="sidebar-normal">Proveedores</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'rack' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('rack.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-grip-vertical fa-2x"></i> </span>
                                <span class="sidebar-normal">Pasillos</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'recolector' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('recolector.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-hiking fa-2x"></i> </span>
                                <span class="sidebar-normal">Recolectores</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'tipocaja' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('tipocaja.index') }}">
                                <span class="sidebar-mini"><i class="fas fa-box-open fa-2x"></i></span>
                                <span class="sidebar-normal">Tipos de caja</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'tipo entrada' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('tipo_entrada.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-door-open fa-2x"></i> </span>
                                <span class="sidebar-normal">Tipos de entrada</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'tipo envio' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('tipo_envio.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-shipping-fast fa-2x"></i> </span>
                                <span class="sidebar-normal">Tipos de envio</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'tipo salida' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('tipo_salida.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-door-closed fa-2x"></i> </span>
                                <span class="sidebar-normal">Tipos de salida</span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'ubicacion' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('ubicacion.index') }}">
                                <span class="sidebar-mini"> <i class="fas fa-map-marked-alt fa-2x"></i> </span>
                                <span class="sidebar-normal">Ubicaciónes</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
