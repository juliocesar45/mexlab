<div class="card ">
    <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
            <i class="fas fa-grip-vertical fa-2x"></i>
        </div>
        <h4 class="card-title">Agregar Rack</h4>
      </div>
      <div class="card-body ">
        <div class="row">
          <div class="col-md-12 text-right">
              <a href="{{ route('rack.index') }}" class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">Almacen</label>
          <div class="col-sm-7">
            <div class="form-group{{ $errors->has('almacen_id') ? ' has-danger' : '' }}">
              <select class="select2" name="almacen_id" aria-label="Almacen" @if ($show) disabled @endif>
                @foreach (App\Almacen::all(['id','nombre']); as $almacen)
                @if ($create)
                    <option value="{{$almacen->id}}">{{$almacen->nombre}}</option>
                @else
                    @if($almacen->id==$rack->almacen_id)
                        <option value="{{$almacen->id}}" selected>{{$almacen->nombre}}</option>
                    @else
                        <option value="{{$almacen->id}}">{{$almacen->nombre}}</option>
                    @endif
                @endif
                 
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">Pasillo</label>
          <div class="col-sm-7">
            <div class="form-group{{ $errors->has('pasillo') ? ' has-danger' : '' }}">
              <input class="form-control{{ $errors->has('pasillo') ? ' is-invalid' : '' }}" name="pasillo" id="input-name" type="text" placeholder="Pasillo" @if($create) value="{{ old('pasillo') }}" @else value="{{ $rack->pasillo,old('pasillo') }}" @endif required="true" aria-required="true" @if ($show) disabled @endif/>
              @include('alerts.feedback', ['field' => 'pasillo'])
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">Cantidad De Espacio</label>
          <div class="col-sm-7">
            <div class="form-group{{ $errors->has('cantidad_slots') ? ' has-danger' : '' }}">
              <input class="form-control{{ $errors->has('cantidad_slots') ? ' is-invalid' : '' }}" name="cantidad_slots" id="input-name" type="number" placeholder="Cantidad De Espacio" @if($create) value="{{ old('cantidad_slots') }}" @else value="{{ $rack->cantidad_slots,old('cantidad_slots') }}" @endif required="true" aria-required="true" @if ($show) disabled @endif/>
              @include('alerts.feedback', ['field' => 'cantidad_slots'])
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">Largo</label>
          <div class="col-sm-7">
            <div class="form-group{{ $errors->has('largo') ? ' has-danger' : '' }}">
              <input class="form-control{{ $errors->has('largo') ? ' is-invalid' : '' }}" name="largo" id="input-name" type="number" placeholder="Largo" @if($create) value="{{ old('largo') }}" @else value="{{ $rack->largo,old('largo') }}" @endif required="true" aria-required="true" @if ($show) disabled @endif/>
              @include('alerts.feedback', ['field' => 'largo'])
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">Ancho</label>
          <div class="col-sm-7">
            <div class="form-group{{ $errors->has('ancho') ? ' has-danger' : '' }}">
              <input class="form-control{{ $errors->has('ancho') ? ' is-invalid' : '' }}" name="ancho" id="input-name" type="number" placeholder="Ancho" @if($create) value="{{ old('ancho') }}" @else value="{{ $rack->ancho,old('ancho') }}" @endif required="true" aria-required="true" @if ($show) disabled @endif/>
              @include('alerts.feedback', ['field' => 'ancho'])
            </div>
          </div>
        </div><div class="row">
          <label class="col-sm-2 col-form-label">Slot Alto</label>
          <div class="col-sm-7">
            <div class="form-group{{ $errors->has('cant_slot_alto') ? ' has-danger' : '' }}">
              <input class="form-control{{ $errors->has('cant_slot_alto') ? ' is-invalid' : '' }}" name="cant_slot_alto" id="input-name" type="number" placeholder="Slot Alto" @if($create) value="{{ old('cant_slot_alto') }}" @else value="{{ $rack->cant_slot_alto,old('cant_slot_alto') }}" @endif required="true" aria-required="true" @if ($show) disabled @endif/>
              @include('alerts.feedback', ['field' => 'cant_slot_alto'])
            </div>
          </div>
        </div><div class="row">
          <label class="col-sm-2 col-form-label">Slot Ancho</label>
          <div class="col-sm-7">
            <div class="form-group{{ $errors->has('cant_slot_ancho') ? ' has-danger' : '' }}">
              <input class="form-control{{ $errors->has('cant_slot_ancho') ? ' is-invalid' : '' }}" name="cant_slot_ancho" id="input-name" type="number" placeholder="Slot Ancho" @if($create) value="{{ old('cant_slot_alto') }}" @else value="{{ $rack->cant_slot_ancho,old('cant_slot_alto') }}" @endif required="true" aria-required="true" @if ($show) disabled @endif/>
              @include('alerts.feedback', ['field' => 'cant_slot_ancho'])
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer ml-auto mr-auto">
        @if ($create)
        <button type="submit" class="btn btn-success">Crear</button>
        @endif
        @if ($edit)
        <button type="submit" class="btn btn-success">Editar</button>
        @endif
        @if ($show)
        <form action="{{ route('rack.destroy', $rack->id) }}" method="post">
            @csrf
            @method('delete')
            <a rel="tooltip" class="btn btn-success" href="{{ route('rack.edit', $rack->id) }}" data-original-title="" title=""> Editar
            </a>
            
            <input type="submit" class="btn btn-danger" value="Eliminar">
        </form>
        @endif
      </div>
    </div>