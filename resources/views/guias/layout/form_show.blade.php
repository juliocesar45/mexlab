
@section('style')
<style>
    .contenedor {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Roboto', sans-serif;
    }

    .contenedor {
        width: 100%;
        margin: auto;
        overflow: hidden;
        padding: 30px;
    }

    .page-break {
        page-break-after: always;
    }

    .page-break:last-child {
        page-break-after: auto;
    }

    @page {
        size: 9cm 15.5cm landscape;
    }

    @media print {

        html,
        body {
            height: 99%;
        }
    }

    .text-left {
        text-align: left;
        font-size: 16px;
        font-weight: 400;
        width: 10%;
    }

    .td-10 {
        width: 10%;
    }

    .td-15 {
        width: 10%;
    }

    table {
        margin-top: 10px;
        margin-bottom: 10px;
        width: 100%;
    }

    p {
        font-size: 16px;
        font-weight: 400;
    }

    img {
        width: 50%;
    }


    .contenedor h4 {
        font-weight: bold;
    }

    #imagen {
        overflow: hidden;
        border: 3px solid red;
    }
</style>
@endsection

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="fas fa-receipt fa-2x"></i>
                </div>
                <h4 class="card-title">Guías</h4>
            </div>
            <div class="card-body">
                <div id="contenedor" class="contenedor">
                    <table>
                        <tr>
                            <td class="td-10"><img src="{{asset('public/material') }}/img/mexlab.png" /></td>
        
                            <td class="text-left">
                                <p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp; &nbsp; &nbsp; &nbsp; Caja:_____ De:_____ </p>
                                <br>
                                <h4><b>Corporativo Grupo Mexlab S.A. de C.V. <br>
                                        &nbsp; &nbsp; &nbsp; Susana Gómez Palafox #5486<br>
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Paseos del Sol C.P.45079<br>
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Zapopan, Jalisco, México<br>
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Tel:33.36.34.23.61<br>
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 01.800.111.4343
                                    </b></h4>
                            </td>
                        </tr>
                    </table>
        
                    <p>DESTINATARIO</p>
                    <table>
                        <tr>
                            <td class="td-15">
                                <p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Paquetería: {{ $guia->c_paqueteria->nombre??'No se especifica' }}
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    Contrato:________________________________ </p>
                                <p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    Servicio:_______________________________________________________________ </p>
                                <br>
                                <p>Nombre/R.F.C.: {{ $guia->cliente->nombre_completo }}</p>
                                <p>Domicilio/Col: {{ $guia->direccion }}</p>
                            </td>
                        </tr>
                    </table>
        
                </div>


                <div id="container-imagen" style="display: none">
                    <p>Presione click derecho sobre el recuadro rojo.</p> 
                    <p>Posteriormente, seleccione la opción de 'Guardar imagen como...'.</p> 
                    <p>Por último, seleccione la ubicación donde desea guardar su imagen, y...</p> 
                    <p>¡Listo!, Habrá guardado su imagen correctamente.</p>
                    <div id="imagen"></div>
                </div>

                <div class="page-break"></div>
                <div class="card-footer ml-auto mr-auto">
                    <button id="btnCapturar" class="btn btn-primary">DESCARGAR</button>
                    <form action="{{ route('guia.destroy', $guia->id) }}" method="post">
                        @csrf
                        @method('delete')
                        @if ($showFactura)
                            <a rel="tooltip" class="btn btn-success" href="{{ route('guia.editar', [$guia->id,$factura]) }}"
                        @else
                            <a rel="tooltip" class="btn btn-success" href="{{ route('guia.edit', $guia->id) }}"
                        @endif
                            data-original-title="" title=""> Editar
                        </a>
                        <input type="text" id="guia_id" name="guia_id" aria-label="guia" value={{$guia->id}} hidden disabled/>
                        @if ($showGuia)
                            <input type="submit" class="btn btn-danger" value="Eliminar">
                        @endif
            
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

@push('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.1/dist/html2canvas.min.js"></script>

    <script>
        const $boton = document.querySelector("#btnCapturar"), // El botón que desencadena
        $objetivo = document.querySelector("#contenedor"), // A qué le tomamos la foto
        $contenedorCanvas = document.querySelector("#imagen"); // En dónde ponemos el elemento canvas
        $containerImagen = document.querySelector("#container-imagen"); // En dónde ponemos el elemento canvas

        // Agregar el listener al botón
        $boton.addEventListener("click", () => {
        html2canvas($objetivo) // Llamar a html2canvas y pasarle el elemento
            .then(canvas => {
                $containerImagen.style.display = 'block';
                // Cuando se resuelva la promesa traerá el canvas
                if($contenedorCanvas.firstChild)
                    $contenedorCanvas.replaceChild(canvas,$contenedorCanvas.firstChild); // Lo agregamos como hijo del div
                else
                    $contenedorCanvas.appendChild(canvas); // Lo agregamos como hijo del div
            });
        });
    </script>
@endpush
