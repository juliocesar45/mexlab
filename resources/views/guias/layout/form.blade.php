<div class="card-body ">
    <div class="row">
        <div class="col-md-12 text-right">
            @if (isset($factura))
                <a href="{{ route('factura.show',$factura) }}" class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
            @else
                <a href="{{ route('guia.index') }}" class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
            @endif
        </div>
    </div>
    @if (isset($factura))
        <input type="hidden" name="factura" value="{{$factura}}">
    @endif
    <div class="row">
        <label class="col-sm-2 col-form-label">Guía</label>
        <div class="col-sm-10">
            <div class="form-group{{ $errors->has('guia') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('guia') ? ' is-invalid' : '' }}" name="guia"
                    id="guia" type="text" placeholder="guia" @if ($create) value="{{ old('guia') }}" @else value="{{ old('guia') ?? $guia->guia }}" @endif @if ($show)
                disabled @endif required/>
                @include('alerts.feedback', ['field' => 'guia'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Paquetería</label>
        <div class="col-sm-4">
            <div class="form-group{{ $errors->has('c_paqueteria_id') ? ' has-danger' : '' }}">
                @if ($show) 
                {!! Form::select('c_paqueteria_id', App\ListHelper::getPaqueteria(), $guia->c_paqueteria_id ?? old('c_paqueteria_id') , ['class' => 'select2', 'id' => 'c_paqueteria_id', 'placeholder' => 'Seleccione la paquetería...', 'required' => 'true']) !!}
                @elseif ($edit)
                    {!! Form::select('c_paqueteria_id', App\ListHelper::getPaqueteria(), $guia->c_paqueteria_id ?? old('c_paqueteria_id') , ['class' => 'select2', 'id' => 'c_paqueteria_id', 'placeholder' => 'Seleccione la paquetería...', 'required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'c_paqueteria_id'])
                @else
                    {!! Form::select('c_paqueteria_id', App\ListHelper::getPaqueteria(), old('c_paqueteria_id') , ['class' => 'select2', 'id' => 'c_paqueteria_id', 'placeholder' => 'Seleccione la paquetería...', 'required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'c_paqueteria_id'])
                @endif
            </div>
        </div>
        <label class="col-sm-2 col-form-label">Clientes</label>
        <div class="col-sm-4">
            <div class="form-group{{ $errors->has('cliente_id') ? ' has-danger' : '' }}">
                @if ($show) 
                {!! Form::select('cliente_id', App\ListHelper::getClientes(), $guia->cliente_id ?? old('cliente_id') , ['class' => 'select2', 'id' => 'cliente_id', 'placeholder' => 'Seleccione el cliente...', 'required' => 'true']) !!}
                @elseif ($edit)
                    {!! Form::select('cliente_id', App\ListHelper::getClientes(), $guia->cliente_id ?? old('cliente_id') , ['class' => 'select2', 'id' => 'cliente_id', 'placeholder' => 'Seleccione el cliente...', 'required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'cliente_id'])
                @else
                    {!! Form::select('cliente_id', App\ListHelper::getClientes(), old('cliente_id') , ['class' => 'select2', 'id' => 'cliente_id', 'placeholder' => 'Seleccione el cliente...', 'required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'cliente_id'])
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Dirección</label>
        <div class="col-sm-10">
            <div class="form-group{{ $errors->has('direccion') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion"
                    id="direccion" type="text" placeholder="direccion" @if ($create) value="{{ old('direccion') }}" @else value="{{ old('direccion') ?? $guia->direccion }}" @endif @if ($show)
                disabled @endif required/>
                @include('alerts.feedback', ['field' => 'direccion'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Fecha de Recolección</label>
        <div class="col-sm-4">
            <div class="form-group{{ $errors->has('fecha_recoleccion') ? ' has-danger' : '' }}">
                <input type="text" data-date-format="YYYY-MM-DD HH:mm"
                    class="form-control datetimepicker {{ $errors->has('fecha_recoleccion') ? ' is-invalid' : '' }}" name="fecha_recoleccion"
                    id="fecha_recoleccion" placeholder="Fecha de recolección" @if ($create) value="{{ \Carbon\Carbon::now() }}" @else value="{{ old('fecha_recoleccion') ?? $guia->fecha_recoleccion }}" @endif required="true" aria-required="true" @if ($show)
                disabled @endif required/>
                @include('alerts.feedback', ['field' => 'fecha_recoleccion'])
            </div>
        </div>
        <label class="col-sm-2 col-form-label">Salida</label>
        <div class="col-sm-4">
            <div class="form-group{{ $errors->has('salida_id') ? ' has-danger' : '' }}">
                @if ($show) 
                    {!! Form::select('salida_id', App\ListHelper::getSalidas(), $guia->salida_id ?? old('salida_id') , ['class' => 'select2', 'id' => 'salida_id', 'placeholder' => 'Seleccione la salida...']) !!}
                @elseif ($edit)
                    {!! Form::select('salida_id', App\ListHelper::getSalidas(), $guia->salida_id ?? old('salida_id') , ['class' => 'select2', 'id' => 'salida_id', 'placeholder' => 'Seleccione la salida...']) !!}
                    @include('alerts.feedback', ['field' => 'salida_id'])
                @else
                    {!! Form::select('salida_id', App\ListHelper::getSalidas(), old('salida_id') , ['class' => 'select2', 'id' => 'salida_id', 'placeholder' => 'Seleccione la salida...']) !!}
                    @include('alerts.feedback', ['field' => 'salida_id'])
                @endif
            </div>
        </div>
    </div>
</div>
    <div class="card-footer ml-auto mr-auto">
        @if ($create)
            <button type="submit" class="btn btn-success">Crear</button>
        @endif
        @if ($edit)
            <button type="submit" class="btn btn-success">Editar</button>
        @endif
        @if ($show)
            <form action="{{ route('guia.destroy', $guia->id) }}" method="post">
                @csrf
                @method('delete')
                <a rel="tooltip" class="btn btn-success" href="{{ route('guia.edit', $guia->id) }}"
                    data-original-title="" title=""> Editar
                </a>
                <input type="text" id="guia_id" name="guia_id" aria-label="guia" value={{$guia->id}} hidden @if ($show) disabled @endif/>
                <input type="submit" class="btn btn-danger" value="Eliminar">
    
            </form>
        @endif
    </div>
  