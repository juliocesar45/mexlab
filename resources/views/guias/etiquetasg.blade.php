<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- <title>Etiquetas Mexlab</title> --}}
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Roboto', sans-serif;
        }

        .contenedor {
            width: 100%;
            margin: auto;
            overflow: hidden;
            padding: 30px;
        }

        .page-break {
            page-break-after: always;
        }

        .page-break:last-child {
            page-break-after: auto;
        }

        @page {
            size: 9cm 15.5cm landscape;
        }

        @media print {

            html,
            body {
                height: 99%;
            }
        }

        .text-left {
            text-align: left;
            font-size: 11px;
            width: 10%;
        }

        .td-10 {
            width: 10%;
        }

        .td-15 {
            width: 10%;
        }

        table {
            margin-top: 10px;
            margin-bottom: 10px;
            width: 100%;
        }

        p {
            font-size: 11px;
        }

        img {
            width: 240%;
        }

    </style>
</head>

<body>
    {{-- <h1>VAMOS</h1> --}}
    {{-- {{$guia}}{{$cantidad}} --}}

    {{-- <p>{{public_path().'public\material'}}</p> --}}

    @for ($i = 0; $i < $cantidad; $i++)
        <div class="contenedor">
            <table>
                <tr>

                    <td class="td-10"><img src="{{ public_path() . '/material/img/mexlab.png' }}" /></td>

                    <td class="text-left">
                        <p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; Caja:_____ De:_____ </p>
                        <br>
                        <h4><b>Corporativo Grupo Mexlab S.A. de C.V. <br>
                                &nbsp; &nbsp; &nbsp; Susana Gómez Palafox #5486<br>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Paseos del Sol C.P.45079<br>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Zapopan, Jalisco, México<br>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Tel:33.36.34.23.61<br>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 01.800.111.4343
                            </b></h4>
                    </td>
                </tr>
            </table>

            <p>DESTINATARIO</p>
            <table>
                <tr>
                    <td class="td-15">
                        <p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Paquetería: {{ $guia->c_paqueteria->nombre }}
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Contrato:________________________________ </p>
                        <p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            Servicio:_______________________________________________________________ </p>
                        <br>
                        <p>Nombre/R.F.C.: {{ $guia->cliente->nombre_completo }}</p>
                        <p>Domicilio/Col: {{ $guia->direccion }}</p>
                    </td>
                </tr>
            </table>

        </div>
        <div class="page-break"></div>
    @endfor
</body>

</html>
