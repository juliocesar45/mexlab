@extends('layouts.app', ['activePage' => 'guia', 'menuParent' => 'consultas', 'titlePage' => __('Guias')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-receipt fa-2x"></i>
                            </div>
                            <h4 class="card-title">Guías</h4>
                        </div>
                        <div class="card-body">
                            @can('create', App\User::class)
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <a href="{{ route('guia.create') }}" class="btn btn-sm btn-success">Agregar Guía</a>
                                    </div>
                                </div>
                            @endcan
                            <div class="table-responsive">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                    style="display:none;width:100%">
                                    <thead class="text-primary">
                                        <th class="desktop">
                                            Guía
                                        </th>
                                        <th class="desktop">
                                            Paquetería
                                        </th>
                                        <th class="desktop">
                                            Cliente
                                        </th>
                                        <th class="desktop">
                                            Dirección
                                        </th>
                                        <th class="desktop">
                                            Fecha de recolección
                                        </th>
                                        <th class="desktop">
                                            Salida
                                        </th>
                                        @can('manage-users', App\User::class)
                                            <th class="text-right desktop">
                                                {{ __('Acciones') }}
                                            </th>
                                        @endcan
                                    </thead>
                                    <tbody>
                                        @foreach ($guias as $guia)
                                            <tr>
                                                <td>{{ $guia->guia }}</td>
                                                <td>{{ $guia->c_paqueteria->nombre ?? '' }}</td>
                                                <td>{{ $guia->cliente->nombre_completo ?? '' }}</td>
                                                <td>{{ $guia->direccion }}</td>
                                                <td>{{ \Carbon\Carbon::parse($guia->fecha_recoleccion)->format('d/m/y H:i')}}</td>
                                                <td>{{ $guia->salida->id ?? '' }}</td>
                                                <td class="td-actions text-right">
                                                    <form action="{{ route('guia.destroy', $guia->id) }}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <!-- Button trigger modal -->
                                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                            data-target="#exampleModal"
                                                            onclick="mostrar({{ $guia->id }})">
                                                            <i class="fas fa-tags"></i>
                                                        </button>

                                                        <a rel="tooltip" class="btn btn-warning btn-link"
                                                            href="{{ route('guia.show', $guia->id) }}"
                                                            data-original-title="" title="">
                                                            <i class="material-icons">visibility</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                        <a rel="tooltip" class="btn btn-success btn-link"
                                                            href="{{ route('guia.edit', $guia->id) }}"
                                                            data-original-title="" title="">
                                                            <i class="material-icons">edit</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                        <button type="button" class="btn btn-danger btn-link"
                                                            data-original-title="" title="" onclick="Swal.fire({
                                        title: 'Estás seguro?',
                                        text: 'Se borrará el registro!',
                                        type: 'warning',
                                        showCancelButton: true,
                                        buttonsStyling: false, 
                                        confirmButtonClass: 'btn btn-success',
                                        cancelButtonClass: 'btn btn-danger',
                                        confirmButtonText: 'Si, borralo!',
                                        cancelButtonText: 'No, quiero conservarlo!'
                                    }).then((result) => {
                                        if (result.value) {
                                            this.parentElement.submit()
                                        }
                                    })">
                                                            <i class="material-icons">close</i>
                                                            <div class="ripple-container"></div>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <div class="card-icon">
                                                <i class="fas fa-tags fa-2x" style="color:purple"></i>
                                            </div>
                                            <h5 class="modal-title" id="exampleModalLabel">Etiqueta guía</h5>
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action="{{ route('guia.imprimir.etiqueta') }}"
                                                class="form-horizontal" enctype="multipart/form-data">
                                                @csrf
                                                @method('post')
                                                <div class="row">
                                                    <div class="col-4"></div>
                                                    <div class="col-4 text-center">
                                                      <label for="cantidad">Cantidad</label>
                                                      <input class="form-control" type="number" min="1" pattern="^[0-9]+" digits="true" name="cantidad" id="">
                                                    </div>
                                                    <input type="hidden" name="id_guia" id="id_guia">
                                                </div>
                                        </div>
                                        <div class="modal-footer ml-auto mr-auto">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Cerrar</button>
                                            <button type="submit" class="btn btn-primary">Imprimir</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $('#datatables').fadeIn(1100);
            $('#datatables').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Todos"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Buscar guias",
                },
                "columnDefs": [{
                    "orderable": false,
                    "targets": 6
                }, ],
            });
        });

        function mostrar(id){    
            $("#id_guia").val(id);
        }
    </script>
@endpush
