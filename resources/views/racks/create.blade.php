@extends('layouts.app', ['activePage' => 'rack', 'menuParent' => 'catalogos', 'titlePage' => __('Pasillo')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('rack.store') }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        <div class="card">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-grip-vertical fa-2x"></i>
                                </div>
                                <h4 class="card-title">Agregar Pasillo</h4>
                            </div>
                            @include('racks.layout.form',['show' => false,'edit' => false, 'create' => true])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
