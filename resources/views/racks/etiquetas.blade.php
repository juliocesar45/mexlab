<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Etiquetas Qr Mexlab</title>
    <style>
        @media print {

            html,
            body {
                height: 99%;
            }
        }

        .page-break {
            page-break-after: always;
        }

        .page-break:last-child {
            page-break-after: auto;
        }

        table {
            width: 100%;
        }

        tr{
            margin-top: 5px;
        }

    </style>
</head>

<body>
    @php
        $contador = 0;
    @endphp
    <table>
        @foreach ($qrcode as $qr)
            @if ($contador == 0)
                @php
                    $contador = 3;
                @endphp
                <tr>
            @endif
            <td style="border: 1px solid black">
                <img style="margin-bottom: 0;margin-top:30px;" src="data:image/png;base64, {!! $qr['qr'] !!}">
                <p style="text-align: center;margin-top: 0;">{{ $qr['slot'] }}</p>
            </td>
            @if ($contador == 0)
                </tr>
            @endif
            @php
                $contador--;
            @endphp
        @endforeach
    </table>
</body>

</html>
