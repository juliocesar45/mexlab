<div class="card-body ">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="{{ route('rack.index') }}" class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Almacen</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('c_almacene_id') ? ' has-danger' : '' }}">
                <select class="select2" name="c_almacene_id" aria-label="Almacen" @if ($show) disabled @endif>
                    @foreach (App\Almacen::all(['id', 'nombre']) as $almacen)
                        @if ($create)
                            <option value="{{ $almacen->id }}">{{ $almacen->nombre }}</option>
                        @else
                            @if ($almacen->id == $rack->c_almacene_id)
                                <option value="{{ $almacen->id }}" selected>{{ $almacen->nombre }}</option>
                            @else
                                <option value="{{ $almacen->id }}">{{ $almacen->nombre }}</option>
                            @endif
                        @endif

                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Pasillo</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('pasillo') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('pasillo') ? ' is-invalid' : '' }}" name="pasillo"
                    id="pasillo" type="text" placeholder="Pasillo" @if ($create) value="{{ old('pasillo') }}" @else value="{{ $rack->pasillo, old('pasillo') }}" @endif required="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'pasillo'])
            </div>
        </div>
    </div>
    {{-- <div class="row mt-4">
        <h5 class="col-sm-2 text-right">Primera Fila</h5>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Fila</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('fila1') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('fila1') ? ' is-invalid' : '' }}"
                    name="fila1" id="fila1" type="text" placeholder="Nombre Fila" @if ($create) value="{{ old('fila1') }}" @else value="{{ $rack->fila1, old('fila1') }}" @endif required="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'fila1'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Slots De Alto</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('alto_fila1') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('alto_fila1') ? ' is-invalid' : '' }}"
                    name="alto_fila1" id="alto_fila1" type="text" placeholder="Cantidad de slots" @if ($create) value="{{ old('alto_fila1') }}" @else value="{{ $rack->alto_fila1, old('alto_fila1') }}" @endif required="true" digits="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'alto_fila1'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Slots De Ancho</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('ancho_fila1') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('ancho_fila1') ? ' is-invalid' : '' }}"
                    name="ancho_fila1" id="ancho_fila1" type="text" placeholder="Cantidad de slots" @if ($create) value="{{ old('ancho_fila1') }}" @else value="{{ $rack->ancho_fila1, old('ancho_fila1') }}" @endif required="true" digits="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'ancho_fila1'])
            </div>
        </div>
    </div>
    
    <div class="row mt-4">
        <h5 class="col-sm-2 text-right">Segunda Fila</h5>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Fila</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('fila2') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('fila2') ? ' is-invalid' : '' }}"
                    name="fila2" id="fila2" type="text" placeholder="Nombre Fila" @if ($create) value="{{ old('fila2') }}" @else value="{{ $rack->fila2, old('fila2') }}" @endif required="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'fila2'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Slots De Alto</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('alto_fila2') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('alto_fila2') ? ' is-invalid' : '' }}"
                    name="alto_fila2" id="alto_fila2" type="text" placeholder="Cantidad de slots" @if ($create) value="{{ old('alto_fila2') }}" @else value="{{ $rack->alto_fila2, old('alto_fila2') }}" @endif required="true" digits="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'alto_fila2'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Slots De Ancho</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('ancho_fila2') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('ancho_fila2') ? ' is-invalid' : '' }}"
                    name="ancho_fila2" id="ancho_fila2" type="text" placeholder="Cantidad de slots" @if ($create) value="{{ old('ancho_fila2') }}" @else value="{{ $rack->ancho_fila2, old('ancho_fila2') }}" @endif required="true" digits="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'ancho_fila2'])
            </div>
        </div>
    </div> --}}
</div>
<div class="card-footer ml-auto mr-auto">
    @if ($create)
        <button type="submit" class="btn btn-success">Crear</button>
    @endif
    @if ($edit)
        <button type="submit" class="btn btn-success">Editar</button>
    @endif
    @if ($show)
        <form action="{{ route('rack.destroy', $rack->id) }}" method="post">
            @csrf
            @method('delete')
            <a rel="tooltip" class="btn btn-success" href="{{ route('rack.edit', $rack->id) }}"
                data-original-title="" title=""> Editar
            </a>

            <input type="submit" class="btn btn-danger" value="Eliminar">
        </form>
    @endif
</div>
