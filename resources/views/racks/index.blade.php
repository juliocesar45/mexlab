@extends('layouts.app', ['activePage' => 'rack', 'menuParent' => 'catalogos', 'titlePage' => __('Pasillo')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">

          @isset($alert)
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <strong>{{$alert}}</strong>
                  <br>

                  @if (count($qrCodes))
                      <button class='btn btn-sm btn-primary' onclick="clickbutton()">Descargar Qrs</button>
                  @endif
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <br>
                  @php($cont=0)
                  @foreach ($qrCodes as $qrCode)
                          <a id="enlace-{{$cont++}}" class="btn btn-sm btn-primary" href="data:image/png;base64, {{$qrCode['img']}}" download="{{$qrCode['nombre']}}">{{$qrCode['nombre']}}</a>
                  @endforeach
              </div>
          @endisset

            <div class="card">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                  <i class="fas fa-grip-vertical fa-2x"></i>
                </div>
                <h4 class="card-title">Pasillos</h4>
              </div>
              <div class="card-body">
                @can('create', App\User::class)
                  <div class="row">
                    <div class="col-12 text-right">
                      <a href="{{ route('rack.create') }}" class="btn btn-sm btn-success">Agregar Pasillo</a>
                    </div>
                  </div>
                @endcan
                <div class="table-responsive">
                  <table id="datatables" class="table table-striped table-no-bordered table-hover" style="display:none;width:100%">
                    <thead class="text-primary">
                      <th class="desktop">Almacen</th>
                      <th class="desktop">Pasillo</th>
                      {{-- <th class="desktop">Fila 1</th>
                      <th class="desktop">Fila 2</th> --}}
                      @can('manage-users', App\User::class)
                        <th class="text-right desktop">
                          {{ __('Acciones') }}
                        </th>
                      @endcan
                    </thead>
                    <tbody>
                      @foreach($racks as $rack)
                        <tr>
                          <td>{{$rack->almacen->nombre}}</td>
                          <td>{{$rack->pasillo}}</td>
                          {{-- <td>{{$rack->fila1}}</td>
                          <td>{{$rack->fila2}}</td> --}}
                          <td class="td-actions text-right">
                              <form action="{{ route('rack.destroy', $rack->id) }}" method="post">
                                  @csrf
                                  @method('delete')
                                  <!-- Button trigger modal -->
                                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onclick="mostrar({{$rack->id}})">
                                    <i class="fas fa-tags"></i>
                                  </button>
                                  <a rel="tooltip" class="btn btn-warning btn-link" href="{{ route('rack.show', $rack->id) }}" data-original-title="" title="">
                                    <i class="material-icons">visibility</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('rack.edit', $rack->id) }}" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="Swal.fire({
                                    title: 'Estás seguro?',
                                    text: 'Se borrará el registro!',
                                    type: 'warning',
                                    showCancelButton: true,
                                    buttonsStyling: false,
                                    confirmButtonClass: 'btn btn-danger',
                                    cancelButtonClass: 'btn btn-success',
                                    confirmButtonText: 'Si, borralo!',
                                    cancelButtonText: 'No, quiero conservarlo!'
                                }).then((result) => {
                                    if (result.value) {
                                        this.parentElement.submit()
                                    }
                                })">
                                      <i class="material-icons">close</i>
                                      <div class="ripple-container"></div>
                                  </button>
                              </form>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                  <div class="modal-content">
                      <div class="modal-header">
                          <div class="card-icon">
                              <i class="fas fa-tags fa-2x" style="color:purple"></i>
                          </div>
                      <h5 class="modal-title" id="exampleModalLabel">Etiqueta Qr</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      </div>
                      <div class="modal-body">
                      <form method="post" action="{{route('rack.imprimir.etiqueta')}}" class="form-horizontal" enctype="multipart/form-data">
                          @csrf
                          @method('post')
                          <div class="row">
                            <div class="col-4"></div>
                              <div class="col-4 text-center">
                                  <label for="cantidad">Pasillo</label>
                                  <input class="form-control" type="text" name="pasillo" id="pasillo" disabled>
                              </div>
                              <input type="hidden" name="id_rack" id="id_rack">
                          </div>
                      </div>
                      <div class="modal-footer ml-auto mr-auto">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      <button type="submit" class="btn btn-primary">Imprimir</button>
                      </div>
                  </form>
                  </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
</div>
</div>
@endsection
@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables').fadeIn(1100);
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "Todos"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Buscar racks",
        },
        "columnDefs": [
          { "orderable": false, "targets": 4 },
        ],
      });
    });
    function mostrar(id){
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('rack.info.etiqueta') }}",
                type: 'GET',
                dataType: "JSON",
                data: {
                    "_token": token,
                     "id_rack": id
                },
                success: function(response) {
                    console.log(response)
                    $("#pasillo").val(response.pasillo);
                    $("#id_rack").val(response.id);
                    console(response);
                }
            });
        }

      function clickbutton() {
          let size = {{count($qrCodes)}};
          let i=0;

          for(i=0; i<size; i++){
              // console.log('enlace-'+(i));
              document.getElementById('enlace-'+(i)).click();
          }
          // while(true){
          //     console.log('enlace-'+(i++));
          //     try{
          //         document.getElementById('enlace-'+(i++)).click();
          //     }catch(error){
          //         break;
          //     }
          // }
      }
    </script>
@endpush