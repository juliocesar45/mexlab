<div class="card-body ">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="{{ route('recolector.index') }}"
                class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Nombre</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre"
                    id="nombre" type="text" placeholder="Nombre" required="true"
                    @if ($create) value="{{ old('nombre') }}" @else value="{{ $recolector->nombre, old('nombre') }}" @endif  aria-required="true" @if ($show) disabled @endif />
                @include('alerts.feedback', ['field' => 'nombre'])
            </div>
        </div>
    </div>

    <div class="row">
        <label class="col-sm-2 col-form-label">Contraseña</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('pass') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('pass') ? ' is-invalid' : '' }}" name="pass"
                    id="pass" type="text" placeholder="contraseña" required="true"
                    @if ($create) value="{{ old('pass') }}" @else value="{{ $recolector->pass, old('pass') }}" @endif  aria-required="true" @if ($show) disabled @endif />
                @include('alerts.feedback', ['field' => 'pass'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Almacen</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('almacen_id') ? ' has-danger' : '' }}">
                <select class="select2" name="almacen_id" aria-label="Almacen" @if ($show) disabled @endif>
                    @foreach (App\Almacen::all(['id', 'nombre']) as $almacen)
                        @if ($create)
                            <option value="{{ $almacen->id }}">{{ $almacen->nombre }}</option>
                        @else
                            @if ($almacen->id == $recolector->almacen_id)
                                <option value="{{ $almacen->id }}" selected>{{ $almacen->nombre }}</option>
                            @else
                                <option value="{{ $almacen->id }}">{{ $almacen->nombre }}</option>
                            @endif
                        @endif

                    @endforeach
                </select>
            </div>
        </div>
    </div>
    @if (!$create)
    <div class="row">
        <label class="col-sm-2 col-form-label">Estatus</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('estatus') ? ' has-danger' : '' }}">
                @if ($show) 
                    {!! Form::select('estatus', App\ListHelper::getEstatus(), $recolector->estatus ?? old('estatus') , ['class' => 'select2', 'id' => 'estatus', 'placeholder' => 'Seleccione el estatus...','required' => 'true', 'disabled' => 'true']) !!}
                @else
                    {!! Form::select('estatus', App\ListHelper::getEstatus(), $recolector->estatus ?? old('estatus') , ['class' => 'select2', 'id' => 'estatus', 'placeholder' => 'Seleccione el estatus...','required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'estatus'])
                    @endif
            </div>
        </div>
    </div>
    @else
    <input type="hidden" name="estatus" value="ACTIVO">
    @endif

</div>
<div class="card-footer ml-auto mr-auto">
    @if ($create)
        <button type="submit" class="btn btn-success">Crear</button>
    @endif
    @if ($edit)
        <button type="submit" class="btn btn-success">Editar</button>
    @endif
    @if ($show)
        <form action="{{ route('recolector.destroy', $recolector->id) }}" method="post">
            @csrf
            @method('delete')
            <a rel="tooltip" class="btn btn-success" href="{{ route('recolector.edit', $recolector->id) }}"
                data-original-title="" title=""> Editar
            </a>

            <input type="submit" class="btn btn-danger" value="Eliminar">
        </form>
    @endif
</div>

