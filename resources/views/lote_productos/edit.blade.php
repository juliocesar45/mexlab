@extends('layouts.app', ['activePage' => 'lote_producto', 'menuParent' => 'procesos', 'titlePage' => __('Lote Producto')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('lote_producto.update', $loteProducto->id) }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('put')
                        <div class="card ">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-sort-amount-up-alt fa-2x"></i>
                                </div>
                                <h4 class="card-title">Editar Lote Producto</h4>
                            </div>
                            @include('lote_productos.layout.form',['show' => false,'edit' => true, 'create' => false])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
