<div class="card-body ">
    <div class="row">
        <div class="col-md-12 text-right">
            @if ($create)
                <a href="{{ route('producto.show', $producto) }}"
                    class="btn btn-sm btn-success">{{ __('Regresar al producto') }}</a>
            @else
                <a href="{{ route('producto.show', $loteProducto->producto_id) }}"
                    class="btn btn-sm btn-success">{{ __('Regresar al producto') }}</a>
            @endif
        </div>
    </div>
    <div class="row">
        <label class="col-sm-1 col-form-label">Nombre</label>
        <div class="col-sm-11">
            <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                <input data-date-format="YYYY-MM-DD"
                    class="form-control {{ $errors->has('nombre') ? ' is-invalid' : '' }}"
                    name="nombre" id="nombre" type="text" placeholder="Nombre" @if ($create) value="{{ old('nombre') }}" @else value="{{ old('nombre') ?? $loteProducto->nombre }}" @endif required aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'nombre'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-1 col-form-label">Producto</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('producto_id') ? ' has-danger' : '' }}">
                @if ($edit)
                    {!! Form::select('producto', App\ListHelper::getProductos(), $loteProducto->producto_id ?? old('producto_id'), ['class' => 'select2', 'id' => 'producto', 'placeholder' => 'Seleccione el producto...', 'required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'recolector_id'])
                    <input type="hidden" name="producto_id" value="{{$loteProducto->producto_id}}">
                @elseif($create)
                    {!! Form::select('producto', App\ListHelper::getProductos(), $producto->id ?? old('producto_id'), ['class' => 'select2', 'id' => 'producto', 'placeholder' => 'Seleccione el producto...', 'required' => 'true', 'disabled' => 'true']) !!}
                    <input type="hidden" name="producto_id" value="{{$producto->id}}">
                @else
                    {!! Form::select('producto', App\ListHelper::getProductos(), $loteProducto->producto_id, ['class' => 'select2', 'id' => 'producto', 'placeholder' => 'Seleccione el producto...', 'required' => 'true', 'disabled' => 'true']) !!}
                @endif
            </div>
        </div>
        <label class="col-sm-1 col-form-label">Lote</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('lote') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('lote') ? ' is-invalid' : '' }}" name="lote" id="lote"
                    type="text" placeholder="Lote" @if ($create) value="{{ old('lote') }}" @else value="{{ old('lote') ?? $loteProducto->lote }}" @endif required aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'lote'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-1 col-form-label">Caducidad</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('caducidad') ? ' has-danger' : '' }}">
                <input data-date-format="YYYY-MM-DD"
                    class="form-control datepicker {{ $errors->has('caducidad') ? ' is-invalid' : '' }}"
                    name="caducidad" id="caducidad" type="text" placeholder="Caducidad" @if ($create) value="{{ old('caducidad') }}" @else value="{{ old('caducidad') ?? $loteProducto->caducidad }}" @endif required aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'caducidad'])
            </div>
        </div>
        <label class="col-sm-1 col-form-label">Cantidad</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('cantidad') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('cantidad') ? ' is-invalid' : '' }}" 
                    name="cantidad" id="cantidad" type="text" placeholder="Cantidad" @if ($create) value="{{ old('cantidad') }}" @else value="{{ old('cantidad') ?? $loteProducto->cantidad }}" @endif required digits="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'cantidad'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-1 col-form-label">Comodin</label>
        <div class="col-sm-5">
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" name="comodin" type="checkbox" @if ($create)  @if (old('comodin'))
                    checked @endif
                @else
                    @if ($loteProducto->comodin)
                        checked
                    @endif
                    @endif

                    @if ($show)
                        @if ($loteProducto->comodin)
                            checked
                        @endif
                        disabled
                    @endif
                    >
                    <span class="form-check-sign">
                        <span class="check"></span>
                    </span>
                </label>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-1 col-form-label">Código</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('codigo') ? ' has-danger' : '' }}">
                <input data-date-format="YYYY-MM-DD"
                    class="form-control {{ $errors->has('codigo') ? ' is-invalid' : '' }}"
                    name="codigo" id="codigo" type="text" placeholder="Código" @if ($create) value="{{ old('codigo') }}" @else value="{{ old('codigo') ?? $loteProducto->codigo }}" @endif digits="true" aria-required="true" required @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'codigo'])
            </div>
        </div>
        <label class="col-sm-1 col-form-label">Código 2</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('codigo2') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('codigo2') ? ' is-invalid' : '' }}" 
                    name="codigo2" id="codigo2" type="text" placeholder="Código 2" @if ($create) value="{{ old('codigo2') }}" @else value="{{ old('codigo2') ?? $loteProducto->codigo2 }}" @endif digits="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'codigo2'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-1 col-form-label">Descripción</label>
        <div class="col-sm-11">
            <div class="form-group{{ $errors->has('descripcion') ? ' has-danger' : '' }}">
                <input data-date-format="YYYY-MM-DD"
                    class="form-control {{ $errors->has('descripcion') ? ' is-invalid' : '' }}"
                    name="descripcion" id="descripcion" type="text" placeholder="Descripción" @if ($create) value="{{ old('descripcion') }}" @else value="{{ old('descripcion') ?? $loteProducto->descripcion }}" @endif aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'descripcion'])
            </div>
        </div>
    </div>
    @if (!$create)
        <div class="row">
            <label class="col-sm-1 col-form-label">Estatus</label>
            <div class="col-sm-5">
                <div class="form-group{{ $errors->has('estatus') ? ' has-danger' : '' }}">
                    @if ($show)
                        {!! Form::select('estatus', App\ListHelper::getEstatus(), $loteProducto->estatus ?? old('estatus'), ['class' => 'select2', 'id' => 'estatus', 'placeholder' => 'Seleccione el estatus...', 'required' => 'true', 'disabled' => 'true']) !!}
                    @else
                        {!! Form::select('estatus', App\ListHelper::getEstatus(), $loteProducto->estatus ?? old('estatus'), ['class' => 'select2', 'id' => 'estatus', 'placeholder' => 'Seleccione el estatus...', 'required' => 'true']) !!}
                        @include('alerts.feedback', ['field' => 'estatus'])
                    @endif
                </div>
            </div>
        </div>
    @else
        <input type="hidden" name="estatus" value="ACTIVO">
    @endif

    <hr>
    
    @if ($show)
        @for ($i = 0; $i < count($ubicaciones); $i++)
            @include('lote_productos.layout.form_ubicacion',['show' => $show,'edit' => $edit, 'create' => $create])
        @endfor
    @endif
    
</div>
<div class="card-footer ml-auto mr-auto">
    @if ($create)
        <button type="submit" class="btn btn-success">Crear</button>
    @endif
    @if ($edit)
        <button type="submit" class="btn btn-success">Editar</button>
    @endif
    @if ($show)
        <form action="{{ route('lote_producto.destroy', $loteProducto->id) }}" method="post">
            @csrf
            @method('delete')
            <a rel="tooltip" class="btn btn-success" href="{{ route('lote_producto.edit', $loteProducto->id) }}"
                data-original-title="" title=""> Editar
            </a>

            <button type="button" class="btn btn-danger" data-original-title="" title="" onclick="Swal.fire({
                title: 'Estás seguro?',
                text: 'Se borrará el registro!',
                type: 'warning',
                showCancelButton: true,
                buttonsStyling: false, 
                confirmButtonClass: 'btn btn-danger',
                cancelButtonClass: 'btn btn-success',
                confirmButtonText: 'Si, borralo!',
                cancelButtonText: 'No, quiero conservarlo!'
            }).then((result) => {
                if (result.value) {
                    this.parentElement.submit()
                }
            })">Eliminar</button>
        </form>
    @endif
</div>
