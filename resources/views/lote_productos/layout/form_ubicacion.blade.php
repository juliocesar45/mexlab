
    <div class="row">
        <input type="hidden" name="guia_{{$i}}" value="true">
        <label class="col-sm-1 col-form-label">Cantidad</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('cantidad') ? ' has-danger' : '' }}">
                <input
                    class="form-control {{ $errors->has('cantidad') ? ' is-invalid' : '' }}"
                    name="cantidad_{{$i}}" id="cantidad" type="text" placeholder="Código" @if ($create) value="{{ old('cantidad') }}" @else value="{{ old('cantidad') ?? $ubicaciones[$i]->cantidad }}" @endif digits="true" aria-required="true" required @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'cantidad'])
            </div>
        </div>
        <label class="col-sm-1 col-form-label">Ubicación</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('ubicacion') ? ' has-danger' : '' }}">
                @if ($show)
                    {!! Form::select('ubicacion_'.$i, App\ListHelper::getListaUbicaciones(), $ubicaciones[$i]->c_ubicacione_id ?? old('ubicacion'), ['class' => 'select2', 'id' => 'ubicacion_'.$i, 'placeholder' => 'Seleccione el estatus...', 'required' => 'true', 'disabled' => 'true']) !!}
                @else
                    {!! Form::select('ubicacion_'.$i, App\ListHelper::getListaUbicaciones(), $ubicaciones[$i]->c_ubicacione_id ?? old('ubicacion'), ['class' => 'select2', 'id' => 'ubicacion_'.$i, 'placeholder' => 'Seleccione el estatus...', 'required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'estatus'])
                @endif
            </div>
        </div>
    </div>