@extends('layouts.app', ['activePage' => 'familia producto', 'menuParent' => 'catalogos', 'titlePage' => __('Familia Producto')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('familia_producto.store') }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        <div class="card">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                  <i class="fas fa-shopping-basket fa-2x"></i>
                                </div>
                                <h4 class="card-title">Agregar Familia Producto</h4>
                            </div>
                            @include('familia_productos.layout.form',['show' => false,'edit' => false, 'create' => true])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection