<div class="card-body ">
  <div class="row">
      <div class="col-md-12 text-right">
          <a href="{{ route('familia_producto.index') }}" class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
      </div>
  </div>
  <div class="row">
      <label class="col-sm-2 col-form-label">Proveedor</label>
      <div class="col-sm-7">
          <div class="form-group{{ $errors->has('proveedor') ? ' has-danger' : '' }}">
              <select class="select2" name="proveedor_id" aria-label="Proveedor" @if ($show) disabled @endif>
                  @foreach (App\Proveedor::all(['id', 'nombre']) as $proveedores)
                      @if ($create)
                          <option value="{{ $proveedores->id }}">{{ $proveedores->nombre }}</option>
                      @else
                          @if ($proveedores->id == $familiaProducto->proveedor_id)
                              <option value="{{ $proveedores->id }}" selected>{{ $proveedores->nombre }}</option>
                          @else
                              <option value="{{ $proveedores->id }}">{{ $proveedores->nombre }}</option>
                          @endif
                      @endif

                  @endforeach
              </select>
          </div>
      </div>
  </div>
  <div class="row">
    <label class="col-sm-2 col-form-label">Nombre</label>
    <div class="col-sm-7">
        <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
            <input class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre"
                id="nombre" type="text" placeholder="Nombre" required="true"
                @if ($create) value="{{ old('nombre') }}" @else value="{{ $familiaProducto->nombre, old('nombre') }}" @endif  aria-required="true" @if ($show) disabled @endif />
            @include('alerts.feedback', ['field' => 'nombre'])
        </div>
    </div>
  </div>
  <div class="card-footer ml-auto mr-auto">
    @if ($create)
        <button type="submit" class="btn btn-success">Crear</button>
    @endif
    @if ($edit)
        <button type="submit" class="btn btn-success">Editar</button>
    @endif
    @if ($show)
        <form action="{{ route('familia_producto.destroy', $familiaProducto->id) }}" method="post">
            @csrf
            @method('delete')
            <a rel="tooltip" class="btn btn-success" href="{{ route('familia_producto.edit', $familiaProducto->id) }}"
                data-original-title="" title=""> Editar
            </a>

            <input type="submit" class="btn btn-danger" value="Eliminar">
        </form>
    @endif
</div>
