@extends('layouts.app', ['activePage' => 'paqueteria', 'menuParent' => 'catalogos', 'titlePage' => __('paqueteria')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('paqueteria.update', $paqueteria) }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('put')

                        <div class="card ">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-parachute-box fa-2x"></i>
                                </div>
                                <h4 class="card-title">Consulta Paqueteria</h4>
                            </div>
                            @include('paqueterias.layout.form',['show' => true,'edit' => false, 'create' => false])
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
