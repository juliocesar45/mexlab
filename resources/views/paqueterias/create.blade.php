@extends('layouts.app', ['activePage' => 'paqueteria', 'menuParent' => 'catalogos', 'titlePage' => __('Paqueteria')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('paqueteria.store') }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('post')
                        <div class="card ">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-people-carry fa-2x"></i>
                                </div>
                                <h4 class="card-title">Agregar Paqueteria</h4>
                            </div>
                            @include('paqueterias.layout.form',['show' => false,'edit' => false, 'create' => true])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
