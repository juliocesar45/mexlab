<div class="card-body ">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="{{ route('entrada.index') }}"
                class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Fecha</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('fecha') ? ' has-danger' : '' }}">
                <input type="text" data-date-format="YYYY-MM-DD HH:mm:ss"
                    class="form-control datetimepicker {{ $errors->has('fecha') ? ' is-invalid' : '' }}" name="fecha"
                    id="fecha" placeholder="fecha" @if ($create) value="{{ old('fecha') }}" @else value="{{ $entrada->fecha, old('fecha') }}" @endif required="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'fecha'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Proveedor</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('proveedor_id') ? ' has-danger' : '' }}">
                <select class="select2" name="proveedor_id" aria-label="Proveedor" @if ($show) disabled @endif>
                    @foreach (App\Proveedor::all(['id', 'nombre']) as $proveedor)
                        @if ($create)
                            <option value="{{ $proveedor->id }}">{{ $proveedor->nombre }}</option>
                        @else
                            @if ($proveedor->id == $entrada->proveedor_id)
                                <option value="{{ $proveedor->id }}" selected>{{ $proveedor->nombre }}</option>
                            @else
                                <option value="{{ $proveedor->id }}">{{ $proveedor->nombre }}</option>
                            @endif
                        @endif

                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Tipo Entrada</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('tipo_entrada_id') ? ' has-danger' : '' }}">
                <select class="select2" name="tipo_entrada_id" aria-label="Tipo Entrada" @if ($show) disabled @endif>
                    @foreach (App\TipoEntrada::all(['id', 'nombre']) as $tipo_entrada)
                        @if ($create)
                            <option value="{{ $tipo_entrada->id }}">{{ $tipo_entrada->nombre }}</option>
                        @else
                            @if ($tipo_entrada->id == $entrada->tipo_entrada_id)
                                <option value="{{ $tipo_entrada->id }}" selected>{{ $tipo_entrada->nombre }}
                                </option>
                            @else
                                <option value="{{ $tipo_entrada->id }}">{{ $tipo_entrada->nombre }}</option>
                            @endif
                        @endif

                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Estado</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('estado') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" name="estado"
                    id="estado" type="text" placeholder="Estado" @if ($create) value="{{ old('estado') }}" @else value="{{ $entrada->estado, old('estado') }}" @endif required="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'estado'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Folio Compra</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('folio_compra') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('folio_compra') ? ' is-invalid' : '' }}"
                    name="folio_compra" id="folio_compra" type="text" placeholder="Folio Compra" @if ($create) value="{{ old('folio_compra') }}" @else value="{{ $entrada->folio_compra, old('folio_compra') }}" @endif required="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'folio_compra'])
            </div>
        </div>
    </div>
</div>
<div class="card-footer ml-auto mr-auto">
    @if ($create)
        <button type="submit" class="btn btn-success">Crear</button>
    @endif
    @if ($edit)
        <button type="submit" class="btn btn-success">Editar</button>
    @endif
    @if ($show)
        <form action="{{ route('entrada.destroy', $entrada->id) }}" method="post">
            @csrf
            @method('delete')
            <a rel="tooltip" class="btn btn-success" href="{{ route('entrada.edit', $entrada->id) }}"
                data-original-title="" title=""> Editar
            </a>

            <input type="submit" class="btn btn-danger" value="Eliminar">
        </form>
    @endif
</div>
