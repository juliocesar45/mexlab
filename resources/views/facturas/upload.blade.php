@extends('layouts.app', ['activePage' => 'Subir factura', 'menuParent' => 'dashboard', 'titlePage' => __('Subir
factura')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-file-invoice-dollar fa-2x"></i>
                            </div>
                            <h4 class="card-title">Subir factura</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('factura.guardar') }}" method="POST" enctype="multipart/form-data" files>
                                @csrf
                                <div class="row">
                                    <div class="col-12 d-flex justify-content-center">
                                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail img-raised">
                                                <img src="{{ asset('public/material') }}/img/factura_subir.svg" alt="...">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                                            <div>
                                                <span class="btn btn-raised btn-round btn-success btn-file">
                                                    <span class="fileinput-new">Seleccione la factura (formato html)</span>
                                                    <span class="fileinput-exists">Cambiar</span>
                                                    <input type="file" name="factura" accept=".html" id="upload_file"
                                                        onchange="loadFile(event)" />
                                                </span>
                                                <a id="remover" href="#" class="btn btn-danger btn-round fileinput-exists"
                                                    data-dismiss="fileinput">
                                                    <i class="fa fa-times"></i> Remover</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="previewBox" hidden>
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-center">
                                            <div class="iframe-container">
                                                <iframe id="preview">
                                                    <p>Tu navegador no soporta esta función.</p>
                                                </iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-5">
                                        <div class="col-6">
                                            <p>Agregar nota a la factura</p>
                                        </div>
                                        <div class="col-6">
                                            <p>Urgencia de la factura</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <textarea type="text" id="nota" name="nota" class="form-control" rows="3" placeholder="Escribe nota aquí..."></textarea>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <select class="selectpicker col-sm-12 pl-0 pr-0" name="estatus" data-style="select-with-transition" title="" data-size="100">
                                                  <option value="NORMAL" >{{ __('NORMAL') }}</option>
                                                  <option value="URGENTE" >{{ __('URGENTE') }}</option>
                                                </select>
                                              </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-center mt-4">
                                            <button type="submit" class="btn btn-success">Subir factura</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        var loadFile = function(event) {
            $('#previewBox').removeAttr('hidden')
            var output = document.getElementById('preview');
            if (event.target.files[0]) {
                output.src = URL.createObjectURL(event.target.files[0]);
                output.onload = function() {
                    URL.revokeObjectURL(output.src) // free memory
                }
                $('body').scrollTo('#previewBox',2000);
            } else {
                $('#previewBox').attr("hidden", true)
            }
        };
    </script>
@endpush
