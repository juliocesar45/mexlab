@extends('layouts.app', ['activePage' => 'factura', 'menuParent' => 'procesos', 'titlePage' => __('Entrada')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('factura.store') }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        <div class="card">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-file-invoice-dollar fa-2x"></i>
                                </div>
                                <h4 class="card-title">Agregar Factura</h4>
                            </div>
                            @include('facturas.layout.form',['show' => false,'edit' => false, 'create' => true])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
