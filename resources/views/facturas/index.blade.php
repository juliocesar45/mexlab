@extends('layouts.app', ['activePage' => 'factura', 'menuParent' => 'procesos', 'titlePage' => __('Factura')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-search-dollar fa-2x"></i>
                            </div>
                            <h4 class="card-title">Filtro de busqueda para facturas</h4>
                        </div>
                        <div class="card-body">
                            <form action=" {{ route('facturas.filtros') }}" method="POST" enctype="multipart/form-data"
                                name="filtros_form" target="_blank">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-6">
                                        <label for="fecha_inicial" class="form-label">Fecha inicial</label>
                                        <input type="text" data-date-format="DD-MM-YYYY" class="form-control datepicker" id="fecha_inicial"
                                            name="fecha_inicial" placeholder="Introduce la fecha inicial...">
                                    </div>
                                    <div class="col-6">
                                        <label for="fecha_final" class="form-label">Fecha final</label>
                                        <input type="text" data-date-format="DD-MM-YYYY" class="form-control datepicker" id="fecha_final"
                                            name="fecha_final" placeholder="Introduce la fecha final...">
                                    </div>
                                </div>
                        </div>
                        <div class="row mt-5">
                            <!--BUSCA FACTURAS-->
                            <div class="col-12 text-center mt-3">
                                <button type="button" class="btn btn-success" onclick=" table.ajax.reload();">
                                    <i class="align-middle"></i> Buscar</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--CATALOGO DE FACTURAS -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-success card-header-icon">
                        <div class="card-icon">
                            <i class="fas fa-file-invoice-dollar fa-2x"></i>
                        </div>
                        <h4 class="card-title">Facturas</h4>
                    </div>
                    <div class="card-body">
                        @can('create', App\User::class)
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{ route('factura.create') }}" class="btn btn-sm btn-success">Agregar
                                        Factura</a>
                                </div>
                            </div>
                        @endcan
                        <div class="table-responsive">
                            <table id="tabla_factura" class="table table-striped table-no-bordered table-hover"
                                style="width:100%">
                                <thead class="text-primary">
                                    <th class="desktop">
                                        Folio
                                    </th>
                                    <th class="desktop">
                                        Cliente
                                    </th>
                                    <th class="desktop">
                                        Cantidad
                                    </th>
                                    <th class="desktop">
                                        Estatus Factura
                                    </th>
                                    <th class="desktop">
                                        Fecha
                                    </th>
                                    <th class="desktop">
                                        Salida
                                    </th>
                                    <th class="text-right desktop">
                                            {{ __('Acciones') }}
                                    </th>
                                </thead>
                            </table>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    {{-- Consulta de facturas --}}
    @push('js')
    <script>
        var table = null

        $(document).ready(function() {
            search();
        });

        function search() {
            var token = "{{ csrf_token() }}";

            table = $('#tabla_factura').DataTable({
                scrollX: true,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthMenu: [
                    [10, 25, 100, -1],
                    [10, 25, 100, "Todos"]
                ],
                ajax: {
                    url: "{{ route('facturas.filtros') }}",
                    data: function(d) {
                        d._token = token;
                        d.fecha_inicial = $('#fecha_inicial').val();
                        d.fecha_final = $('#fecha_final').val();
                    }
                },
                "columns": [{
                        data: "identificador",
                        name: "identificador"
                    },
                    {
                        data: "cliente",
                        name: "cliente"
                    },
                    {
                        data: "cantidad",
                        name: "cantidad"
                    },
                    {
                        data: "estatus_factura",
                        render: function(data, type, row) {
                            if (data == 'Surtido')
                                return '<span class="badge badge-default" style="background-color:#08b31c">Surtido</span>'
                            if (data == 'Por surtir')
                                return '<span class="badge badge-default" style="background-color:#fc8605">Por Surtir</span>'
                            if (data == 'Cancelada')
                                return '<span class="badge badge-default" style="background-color:#fc1105">Cancelada</span>'
                            if (data == 'Factura entregada al cliente')
                                return '<span class="badge badge-default"style="background-color:#6b02ab">Factura entregada al cliente</span>'
                            else
                                return '<span class="badge badge-default"style="background-color:#0292ab">Embarque completo</span>'
                        }
                    },
                    {
                        data: "fecha_hora",
                        name: "fecha_hora"
                    },
                    {
                        data: "salida",
                        name: "salida"
                    },
                    {
                        data: "id",
                        render: function(data, type, row) {
                              /* Creación de la ruta show*/
                                var show = "{{ route('factura.show', ['factura' => 'show']) }}";
                                /* Asignamos el id a temp */
                                show = show.replace('show', data);

                                /* Creación de la ruta edit*/
                                var edit = "{{ route('factura.edit', ['factura' => 'edit']) }}";
                                /* Asignamos el id a temp */
                                edit = edit.replace('edit', data);


                            var  ReturnBotones = 

                                  /* Consultar */
                                '<a rel="tooltip" class="btn btn-warning btn-link" href="' + show + '" data-original-title="" title="">' + 
                                    '<i class="material-icons">visibility</i><div class="ripple-container"></div></a>';

                                ReturnBotones =  ReturnBotones +
                                
                                 /* Editar */
                                '<a rel="tooltip" class="btn btn-success btn-link" href="' + edit + '" data-original-title="" title="">' +
                                    '<i class="material-icons">edit</i><div class="ripple-container"></div></a>' +
                             
                                /* Cancelar */
                                '<button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="Swal.fire({' +
                                    "title: 'Estás seguro?',"
                                    "text: 'Se borrará el registro!',"
                                    "type: 'warning',"
                                    "showCancelButton: true,"
                                    "buttonsStyling: false,"
                                    "confirmButtonClass: 'btn btn-danger',"
                                    "cancelButtonClass: 'btn btn-success',"
                                    "confirmButtonText: 'Si, borralo!',"
                                    "cancelButtonText: 'No, quiero conservarlo!'"
                                '}).then((result) => { if (result.value) { this.parentElement.submit()}})">' + '<i class="material-icons">close</i><div class="ripple-container"></div></button>'

                                return ReturnBotones;
                            
                        }
                    }
                ]
            });

        }

        //Si no encuentra registros para el reporte con filtros
        var msg = '{{ Session::get('sin_registros') }}';
        if (msg) {
            alert(msg);
            close();
        }
    </script>
    @endpush
@endsection