<div class="card-body ">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="{{ route('factura.index') }}"
                class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
        </div>
    </div>
    
    @if ($show && $factura->estatus=='URGENTE')
    <div class="row mt-4">
        <label class="col-sm-2 col-form-label"></label>
        <div class="col-sm-7">
            <div class="alert alert-danger" role="alert" style="width: 100%;">
                Esta factura es URGENTE
            </div>
        </div>
    </div>
    @endif
    @if ($show && $factura->nota)
    <div class="row mt-4">
        <label class="col-sm-2 col-form-label"></label>
        <div class="col-sm-7">
            <div class="alert alert-warning" role="alert" style="width: 100%;">
                Esta factura tiene una nota
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <label class="col-sm-2 col-form-label">Clientes</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('cliente_id') ? ' has-danger' : '' }}">
                <select class="select2" name="cliente_id" aria-label="Cliente" @if ($show) disabled @endif>
                    @foreach (App\Cliente::all(['id', 'nombre_completo']) as $clientes)
                        @if ($create)
                            <option value="{{ $clientes->id }}">{{ $clientes->nombre_completo }}</option>
                        @else
                            @if ($clientes->id == $factura->cliente_id)
                                <option value="{{ $clientes->id }}" selected>{{ $clientes->nombre_completo }}</option>
                            @else
                                <option value="{{ $clientes->id }}">{{ $clientes->nombre_completo }}</option>
                            @endif
                        @endif
  
                    @endforeach
                </select>
            </div>
        </div>
    </div>



    <div class="row">
        <label class="col-sm-2 col-form-label">Cantidad</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('cantidad') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('cantidad') ? ' is-invalid' : '' }}" name="cantidad"
                    id="cantidad" type="text" number="true" placeholder="Cantidad" @if ($create) value="{{ old('cantidad') }}" @else value="{{ old('cantidad')??$factura->cantidad }}" @endif required="true" number="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'cantidad'])
            </div>
        </div>
    </div>




    <div class="row">
        <label class="col-sm-2 col-form-label">Estatus Factura</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('estatus_factura_id') ? ' has-danger' : '' }}">
                <select class="select2" name="estatus_factura_id" aria-label="Estatus Factura" @if ($show) disabled @endif>
                    @foreach (App\EstatusFactura::all(['id', 'nombre']) as $estatusFactura)
                        @if ($create)
                            <option value="{{ $estatusFactura->id }}">{{ $estatusFactura->nombre }}</option>
                        @else
                            @if ($estatusFactura->id == $factura->estatus_factura_id)
                                <option value="{{ $estatusFactura->id }}" selected>{{ $estatusFactura->nombre }}</option>
                            @else
                                <option value="{{ $estatusFactura->id }}">{{ $estatusFactura->nombre }}</option>
                            @endif
                        @endif

                    @endforeach
                </select>
            </div>
        </div>
    </div>


    <div class="row">
        <label class="col-sm-2 col-form-label">Identificador</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('identificador') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('identificador') ? ' is-invalid' : '' }}" name="identificador"
                    id="identificador" type="text" placeholder="Identificador" @if ($create) value="{{ old('identificador') }}" @else value="{{ old('identificador') ?? $factura->identificador }}" @endif required="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'identificador'])
            </div>
        </div>
    </div>


    <div class="row">
        <label class="col-sm-2 col-form-label">Agente</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('agente') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('agente') ? ' is-invalid' : '' }}" name="agente"
                    id="agente" type="text" placeholder="Agente" @if ($create) value="{{ old('agente') }}" @else value="{{ old('agente') ?? $factura->agente }}" @endif required="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'agente'])
            </div>
        </div>
    </div>


    <div class="row">
        <label class="col-sm-2 col-form-label">Recolector</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('recolector_id') ? ' has-danger' : '' }}">
                @if ($show) 
                    {!! Form::select('recolector_id', App\ListHelper::getRecolector(), $factura->recolector_id ?? old('recolector_id') , ['class' => 'select2', 'id' => 'recolector_id', 'placeholder' => 'Seleccione el recolector...','required' => 'true', 'disabled' => 'true']) !!}
                @elseif ($edit)
                    {!! Form::select('recolector_id', App\ListHelper::getRecolector(), $factura->recolector_id ?? old('recolector_id') , ['class' => 'select2', 'id' => 'recolector_id', 'placeholder' => 'Seleccione el recolector...','required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'recolector_id'])
                @else
                    {!! Form::select('recolector_id', App\ListHelper::getRecolector(), old('recolector_id') , ['class' => 'select2', 'id' => 'recolector_id', 'placeholder' => 'Seleccione el recolector...','required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'recolector_id'])
                @endif
            </div>
        </div>
    </div>


    <div class="row">
        <label class="col-sm-2 col-form-label">Notas Recolector</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('observaciones_recolector') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('observaciones_recolector') ? ' is-invalid' : '' }}" name="observaciones_recolector"
                    id="observaciones_recolector" type="text" placeholder="Observaciones" @if ($create) value="{{ old('observaciones_recolector') }}" @else value="{{ old('observaciones_recolector') ?? $factura->observaciones_recolector }}" @endif required="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'observaciones_recolector'])
            </div>
        </div>
    </div>


    <div class="row">
        <label class="col-sm-2 col-form-label">Salida</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('salida') ? ' has-danger' : '' }}">
                <select class="select2" name="salida_id" aria-label="Salida" @if ($show) disabled @endif>
                    @foreach (App\Salida::all(['id', 'tipo_salida_id']) as $salidas)
                        @if ($create)
                            <option value="{{ $salidas->id }}">{{ $salidas->tipo_salida->nombre }}</option>
                        @else
                            @if ($salidas->id == $factura->salida_id)
                                <option value="{{ $salidas->id }}" selected>{{ $salidas->tipo_salida->nombre }}</option>
                            @else
                                <option value="{{ $salidas->id }}">{{ $salidas->tipo_salida->nombre }}</option>
                            @endif
                        @endif

                    @endforeach
                </select>
            </div>
        </div>
    </div>
    @if ($show && $factura->nota)
    <div class="row mt-2">
        <label class="col-sm-2 col-form-label">Notas</label>
        <div class="col-sm-7">
            <textarea type="text" id="nota" name="nota" class="form-control" rows="3" placeholder="Escribe nota aquí..." readonly>{{ $factura->nota }}</textarea>
        </div>
    </div>
    @endif
</div>

<div class="card-footer ml-auto mr-auto">
    @if ($create)
        <button type="submit" class="btn btn-success">Crear</button>
    @endif
    @if ($edit)
        <button type="submit" class="btn btn-success">Editar</button>
    @endif
    @if ($show)
        <form action="{{ route('factura.destroy', $factura->id) }}" method="post">
            @csrf
            @method('delete')
            <a rel="tooltip" class="btn btn-success" href="{{ route('factura.edit', $factura->id) }}"
                data-original-title="" title=""> Editar
            </a>
            <input type="text" id="factura_id" name="factura_id" aria-label="Factura" value={{$factura->id}} hidden @if ($show) disabled @endif/>
            <input type="submit" class="btn btn-danger" value="Eliminar">

        </form>
    @endif

</div>