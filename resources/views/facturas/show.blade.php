@extends('layouts.app', ['activePage' => 'factura', 'menuParent' => 'procesos', 'titlePage' => __('Factura')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('factura.update', $factura) }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('put')
                        <div class="card">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fas fa-file-invoice-dollar fa-2x"></i>
                                </div>
                                <h4 class="card-title">Consultar Factura</h4>
                            </div>
                            @include('facturas.layout.form',['show' => true,'edit' => false, 'create' => false])
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-danger card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-file-invoice fa-2x"></i>
                            </div>
                            <h4 class="card-title">Detalles de Factura</h4>
                        </div>
                        <div class="card-body">
                            <div class="row mb-4">
                                <div class="col-md-12">
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-12">
                                    <table class="table table-hover" id="tabla_detalle_facturas" style="width: 100%;"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-camera fa-2x"></i>
                            </div>
                            <h4 class="card-title">Imagenes del embarque</h4>
                        </div>
                        <div class="card-body">
                            <div class="row mb-4">
                                <div class="col-md-6 text-center">
                                    <h3>Imagen antes del embarque</h3>
                                    <img src="{{asset('')."storage/images_base64/".$factura->foto_producto}}" width="400px" alt="">
                                </div>
                                <div class="col-md-6 text-center">
                                    <h3>Imagen despues del embarque</h3>
                                    <img src="{{asset('')."storage/images_base64/".$factura->foto_empaquetado}}" width="400px" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            @include('guias.layout.form_show',['showFactura'=>true,'showGuia'=>false,'factura'=>$factura->id])

            <!-- <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-camera fa-2x"></i>
                            </div>
                            <h4 class="card-title">Guia</h4>
                        </div>
                        
                    </div>
                </div>
            </div> -->
        </div>
    </div>
@endsection
@push('js')
<script>
    $(document).ready(function() {
    var token = "{{ csrf_token() }}";
    tabla_detalles = $('#tabla_detalle_facturas').DataTable({
            processing: true,
            scrollX: true,
            ajax:{
                url: "{{ route('facturas.detalles') }}",
                dataSrc: "",
                data: function ( d ) {
                    d._token = token;
                    d.id_factura = $('#factura_id').val();
                }
            },
            columns:[
            { title: "#", data:"num", name:"num" },
            { title: "Cantidad", data:"cantidad", name:"cantidad" },
            { title: "Descripción", data:"descripcion", name:"descripcion" },
            { title: "Qr", data:"Qr", 
                render: function(data, type, row) {
                    switch(data){
                        case 0:
                            return '<span class="badge badge-default" style="background-color:#cb3234"><i class="fas fa-exclamation-triangle fa-2x"></i></span>';
                        case 1:
                            return '<span class="badge badge-default" style="background-color:#77dd77"><i class="fas fa-archive fa-2x"></i></span>';
                        case 2:
                            return '<span class="badge badge-default" style="background-color:#ffd200"><i class="fas fa-map-marked-alt fa-2x"></i></span></span>';
                        case 3:
                            return '<span class="badge badge-default" style="background-color:#008f39"><i class="fas fa-check fa-2x"></i></span></span>';
                    }
                } 
            },
            { title: 'Lote y caducidad', data:'lote_caducidad', name:'lote_caducidad' },
            { title: "Capturado Por", data:"capturado_por", name:"capturado_por" },
            ]
        });
    });
    </script>
@endpush