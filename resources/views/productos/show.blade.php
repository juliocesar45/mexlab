@extends('layouts.app', ['activePage' => 'producto', 'menuParent' => 'precesos', 'titlePage' => __('Producto')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('producto.update', $producto) }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('put')
                        <div class="card">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fab fa-product-hunt fa-2x"></i>
                                </div>
                                <h4 class="card-title">Consultar Producto</h4>
                            </div>
                            @include('productos.layout.form',['show' => true,'edit' => false, 'create' => false])
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-danger card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-sort-amount-up-alt fa-2x"></i>
                            </div>
                            <h4 class="card-title">Lote del producto</h4>
                        </div>
                        <div class="card-body">
                            @can('create', App\User::class)
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <a href="{{ route('producto.create.lote',$producto) }}" class="btn btn-sm btn-success">Agregar un Lote del Producto</a>
                                    </div>
                                </div>
                            @endcan
                            <div class="table-responsive">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                    style="display:none;width:100%">
                                    <thead class="text-primary">
                                        <th class="desktop">Código</th>
                                        <th class="desktop">Lote</th>
                                        <th class="desktop">Producto</th>
                                        <th class="desktop">Caducidad</th>
                                        <th class="desktop">Cantidad</th>
                                        <th class="desktop">Estatus</th>
                                        <th class="desktop">Comodin</th>
                                        @can('manage-users', App\User::class)
                                            <th class="text-right desktop">
                                                {{ __('Acciones') }}
                                            </th>
                                        @endcan
                                    </thead>
                                    <tbody>
                                        @foreach ($loteProductos as $loteProducto)
                                            <tr>
                                                <td>{{ $loteProducto->codigo }}</td>
                                                <td>{{ $loteProducto->lote }}</td>
                                                <td>{{ $loteProducto->producto->nombre ?? '' }}</td>
                                                <td>{{ $loteProducto->caducidad }}</td>
                                                <td>{{ $loteProducto->cantidad }}</td>
                                                <td>{{ $loteProducto->estatus }}</td>
                                                <td>{{ $loteProducto->comodin ? 'Sí' : 'No' }}</td>
                                                <td class="td-actions text-right">
                                                    <form
                                                        action="{{ route('lote_producto.destroy', $loteProducto->id) }}"
                                                        method="post">
                                                        @csrf
                                                        @method('delete')

                                                        <!-- Button trigger modal -->
                                                        <a lass="btn btn-primary"
                                                        href="data:image/png;base64, {{base64_encode(QrCode::format('png')->size(200)->errorCorrection('H')->generate($loteProducto->id.''))}}" 
                                                        download="{{$loteProducto->lote}}"><i class="fas fa-tags"></i></a>
                                                        {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onclick="mostrar({{$loteProducto->id}})">
                                                            <i class="fas fa-tags"></i>
                                                        </button> --}}

                                                        <a rel="tooltip" class="btn btn-warning btn-link"
                                                            href="{{ route('lote_producto.show', $loteProducto->id) }}"
                                                            data-original-title="" title="">
                                                            <i class="material-icons">visibility</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                        <a rel="tooltip" class="btn btn-success btn-link"
                                                            href="{{ route('lote_producto.edit', $loteProducto->id) }}"
                                                            data-original-title="" title="">
                                                            <i class="material-icons">edit</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                        <button type="button" class="btn btn-danger btn-link"
                                                            data-original-title="" title="" onclick="Swal.fire({
                                                    title: 'Estás seguro?',
                                                    text: 'Se borrará el registro!',
                                                    type: 'warning',
                                                    showCancelButton: true,
                                                    buttonsStyling: false, 
                                                    confirmButtonClass: 'btn btn-danger',
                                                    cancelButtonClass: 'btn btn-success',
                                                    confirmButtonText: 'Si, borralo!',
                                                    cancelButtonText: 'No, quiero conservarlo!'
                                                }).then((result) => {
                                                    if (result.value) {
                                                        this.parentElement.submit()
                                                    }
                                                })">
                                                            <i class="material-icons">close</i>
                                                            <div class="ripple-container"></div>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div class="card-icon">
                                            <i class="fas fa-tags fa-2x" style="color:purple"></i>
                                        </div>
                                    <h5 class="modal-title" id="exampleModalLabel">Etiqueta</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                    <form method="post" action="{{route('producto.imprimir.etiqueta')}}" class="form-horizontal" enctype="multipart/form-data">
                                        @csrf
                                        @method('post')
                                        <div class="row mb-2">
                                            <div class="col-6">
                                                <label for="cantidad">Código</label>
                                                <input class="form-control" type="text" name="codigo" id="codigo-modal" disabled>
                                            </div>
                                            <div class="col-6">
                                                <label for="cantidad">Nombre</label>
                                                <input class="form-control" type="text" name="nombre" id="nombreEtiqueta" disabled>
                                            </div>
                                            <div class="col-6">
                                                <label for="cantidad">Lote</label>
                                                <input class="form-control" type="text" name="lote" id="loteEtiqueta" disabled>
                                            </div>
                                            <div class="col-6">
                                                <label for="cantidad">Caducidad</label>
                                                <input class="form-control" type="text" name="exp" id="Exp" disabled>
                                            </div>
                                            <div class="col-3"></div>
                                            <div class="col-6" >
                                                <label for="cantidad">Cantidad</label>
                                                <input class="form-control" type="number" min="1" pattern="^[0-9]+" digits="true" name="cantidad" id="">
                                            </div>
                                            <input type="hidden" name="id_lote" id="id_lote">
                                        </div>
                                    </div>
                                    <div class="modal-footer ml-auto mr-auto">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary">Imprimir</button>
                                    </div>
                                </form>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $(document).ready(function() {
            $('#datatables').fadeIn(1100);
            $('#datatables').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Todos"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Buscar lote_productos",
                },
                "columnDefs": [{
                    "orderable": false,
                    "targets": 7
                }, ],
            });
        });

        function mostrar(id){
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('producto.info.etiqueta') }}",
                type: 'GET',
                dataType: "JSON",
                data: {
                    "_token": token,
                     "id_lote": id
                },
                success: function(response) {
                    console.log(response)             
                    $("#codigo-modal").val(response.codigo);
                    $("#nombreEtiqueta").val(response.nombre);
                    $("#loteEtiqueta").val(response.lote);
                    $("#Exp").val(response.caducidad);
                    $("#id_lote").val(response.id);
                }
            });
        }
    </script>
@endpush
