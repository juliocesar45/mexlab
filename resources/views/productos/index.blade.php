@extends('layouts.app', ['activePage' => 'producto', 'menuParent' => 'procesos', 'titlePage' => __('Producto')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    
                    @isset($alert)
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{$alert}}</strong>
                            <br>

                            @if (count($qrCodes))
                                <button class='btn btn-sm btn-primary' onclick="clickbutton()">Descargar Qrs</button>
                                <button id="btnMostrarQrs" class='btn btn-sm btn-warning' onclick="mostrarQrs()">Mostrar Qrs</button>
                            @endif
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>


                            <div id="qrs" style="display: none">
                                @php($cont=0)
                                @foreach ($qrCodes as $qrCode)
                                        <a class="btn btn-sm btn-primary" id="enlace-{{$cont++}}" href="data:image/png;base64, {{$qrCode['img']}}" download="{{$qrCode['nombre']}}">{{$qrCode['nombre']}}</a><br>
                                @endforeach
                            </div>
                        </div>
                    @endisset

                    <div class="card">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="fab fa-product-hunt fa-2x"></i>
                            </div>
                            <h4 class="card-title">Productos</h4>
                        </div>
                        <div class="card-body">
                            @can('create', App\User::class)
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <a href="{{ route('producto.create') }}" class="btn btn-sm btn-success">Agregar
                                            Producto</a>
                                    </div>
                                </div>
                            @endcan


                            {{-- <button class='btn btn-info' onclick="clickbutton()">Descargar Qr</button> --}}
                            <div class="table-responsive">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                    style="display:none;width:100%">
                                    <thead class="text-primary">
                                        <th class="desktop">Código</th>
                                        <th class="desktop">Nombre</th>
                                        @can('manage-users', App\User::class)
                                            <th class="text-right desktop">
                                                {{ __('Acciones') }}
                                            </th>
                                        @endcan
                                    </thead>
                                    <tbody>
                                        @foreach ($productos as $producto)
                                            <tr>
                                                <td>{{ $producto->codigo }}</td>
                                                <td>{{ $producto->nombre}}</td>
                                                <td class="td-actions text-right">
                                                    <form action="{{ route('producto.destroy', $producto->id) }}"
                                                        method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <a rel="tooltip" class="btn btn-warning btn-link"
                                                            href="{{ route('producto.show', $producto->id) }}"
                                                            data-original-title="" title="">
                                                            <i class="material-icons">visibility</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                        <a rel="tooltip" class="btn btn-success btn-link"
                                                            href="{{ route('producto.edit', $producto->id) }}"
                                                            data-original-title="" title="">
                                                            <i class="material-icons">edit</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                        <button type="button" class="btn btn-danger btn-link"
                                                            data-original-title="" title="" onclick="Swal.fire({
                                        title: 'Estás seguro?',
                                        text: 'Se borrará el registro!',
                                        type: 'warning',
                                        showCancelButton: true,
                                        buttonsStyling: false, 
                                        confirmButtonClass: 'btn btn-danger',
                                        cancelButtonClass: 'btn btn-success',
                                        confirmButtonText: 'Si, borralo!',
                                        cancelButtonText: 'No, quiero conservarlo!'
                                    }).then((result) => {
                                        if (result.value) {
                                            this.parentElement.submit()
                                        }
                                    })">
                                                            <i class="material-icons">close</i>
                                                            <div class="ripple-container"></div>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $('#datatables').fadeIn(1100);
            $('#datatables').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Todos"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Buscar productos",
                },
                "columnDefs": [{
                    "orderable": false,
                    "targets": 2
                }, ],
            });
        });

        function mostrarQrs(){
            const element = document.getElementById('qrs');
            const btn = document.getElementById('btnMostrarQrs');
            console.log(element);
            const display = element.style.display;
            console.log(display);

            if(display=='none'){
                element.style.display = 'block';
                btn.innerHTML = 'Ocultar Qrs'
            }else{
                element.style.display = 'none';
                btn.innerHTML = 'Mostrar Qrs';
            }
        }
        

        function clickbutton() {
            let i=0;

            while(true){
                try{
                    document.getElementById('enlace-'+(i++)).click();
                }catch(error){
                    break;
                }
            }
        }
    </script>
@endpush
