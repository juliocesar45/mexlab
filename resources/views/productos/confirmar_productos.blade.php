@extends('layouts.app', ['activePage' => 'producto', 'menuParent' => 'procesos', 'titlePage' => __('Confirmar Producto')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('producto.confirmar') }}"
                        autocomplete="off">
                        @csrf


                        <div class="card">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="fab fa-product-hunt fa-2x"></i>
                                </div>
                                <h4 class="card-title">Entrada</h4>
                            </div>
                            @php $contador = 0; @endphp
                            @include('productos.layout.form_entrada')
                        </div>


                        @php
                            $size = count($lotes);
                        @endphp
                        @for ($i = 0; $i<$size; $i++)

                        <div class="card">
                            @if (!$i)
                                <div class="card-header card-header-success card-header-icon">
                                    <div class="card-icon">
                                        <i class="fas fa-info-circle fa-2x"></i>
                                    </div>
                                    <h4 class="card-title">Desglose de producto</h4>
                                </div>
                            @endif
                            @include('productos.layout.form_lote')
                        </div>
                        @endfor



                        <div class="row">
                            <div class="col-12 d-flex justify-content-center mt-4">
                                <button type="submit" class="btn btn-success">Finalizar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection


@push('js')
    <script>
        function ShowSelected(select,input)
        {
            const combo = document.getElementById(select);
            const selected = combo.options[combo.selectedIndex].text;
            document.getElementById(input).value = selected;
        }
    </script>
@endpush