<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Etiquetas Mexlab</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Roboto', sans-serif;
        }

        .contenedor {
            width: 100%;
            margin: auto;
            overflow: hidden;
            padding: 30px;
        }

        .page-break {
            page-break-after: always;
        }

        .page-break:last-child {
            page-break-after: auto;
        }

        @media print {

            html,
            body {
                height: 99%;
            }
        }

        .text-center {
            text-align: center;
        }

        .td-25{
            width: 25%;
        }

        .td-75{
            width: 50%;
        }

        .td-33{
            width: 33%;
        }

        table{
            margin-top: 30px;
            margin-bottom: 10px;
            width: 100%;
        }

    </style>
</head>

<body>
    @for ($i = 1; $i <= $request['cantidad']; $i++)
        <div class="contenedor">
            {{-- <p>Código: {{ $lote->codigo }}</p>

            <h1 class="text-center"> {{ $lote->nombre }}</h1>--}}

            {{-- <h3 class="text-center">Ensayo rápido para la detección multiple de drogas de abuso</h3>--}}

            <table>
               <tr>
                    {{-- <td class="td-75"><p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quisquam itaque modi, cumque
                        eum
                        voluptate iste suscipit assumenda recusandae amet quia, illum laborum adipisci non? Obcaecati
                        neque
                        tenetur,
                        alias accusantium cum doloremque, hic, architecto velit libero qui in maiores porro ducimus sint
                        accusamus
                        sed
                        id quam molestiae facere suscipit dolorum natus beatae deserunt error. Quas temporibus excepturi
                        suscipit
                        inventore eos dolore perspiciatis voluptatem pariatur, maiores ex minus sapiente labore
                        incidunt,
                        aspernatur
                        accusamus corporis reiciendis. Recusandae facere placeat voluptates tenetur? Quia neque aliquid
                        reiciendis
                        ea?
                        Minus molestias pariatur quisquam voluptatum numquam ipsa, laboriosam laudantium qui nam quos
                        sint?
                        Incidunt
                        debitis eum accusamus.
                    <p></td>--}}
                    <td class="td-25"><img class="text-center" src="data:image/png;base64, {!! $qrcode !!}"></td>
                </tr>
            </table>

            {{--<h4 class="text-center">Almacenar a temperatura de entre 4ºC y 30ºC</h4> --}}

            {{-- <table>
                <tr>
                    <td class="td-33 text-center">
                        <p>Lote: {{ $lote->lote }}</p>
                    </td>
                    <td class="td-33 text-center">
                        <p>Exp: {{ $lote->caducidad }}</p>
                    </td>
                    <td class="td-33 text-center">
                        <p>Registro Sanitario:</p>
                    </td>
                </tr>
            </table> --}}
        </div>
        <div class="page-break"></div>
    @endfor
</body>

</html>
