@extends('layouts.app', ['activePage' => 'producto', 'menuParent' => 'procesos', 'titlePage' => __('Producto')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('producto.update', $producto->id) }}"
                        autocomplete="off">
                        @csrf
                        @method('put')
                        <div class="card ">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                  <i class="fab fa-product-hunt fa-2x"></i>
                                </div>
                                <h4 class="card-title">Editar Producto</h4>
                            </div>
                            @include('productos.layout.form',['show' => false,'edit' => true, 'create' => false])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
