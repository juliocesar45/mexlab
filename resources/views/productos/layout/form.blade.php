<div class="card-body ">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="{{ route('producto.index') }}"
                class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
        </div>
    </div>

    <div class="row">
        <label class="col-sm-1 col-form-label">Código</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('codigo') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('codigo') ? ' is-invalid' : '' }}" name="codigo" id="codigo"
                    type="text" number="true" placeholder="Código" @if ($create) value="{{ old('codigo') }}" @else value="{{ $producto->codigo, old('codigo') }}" @endif required aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'codigo'])
            </div>
        </div>
        <label class="col-sm-1 col-form-label">Nombre</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" id="nombre"
                    type="text" placeholder="Nombre" @if ($create) value="{{ old('nombre') }}" @else value="{{ $producto->nombre, old('nombre') }}" @endif required aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'nombre'])
            </div>
        </div>
    </div>
    <div class="row">
        {{--
        <label class="col-sm-1 col-form-label">Tipo Caja</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('tipo_caja_id') ? ' has-danger' : '' }}">
                @if ($show)
                    {!! Form::select('tipo_caja_id', App\ListHelper::getTipoCajas(), $producto->tipo_caja_id ?? old('tipo_caja_id'), ['class' => 'select2', 'id' => 'tipo_caja_id', 'placeholder' => 'Seleccione el tipo de caja...', 'required' => 'true', 'disabled' => 'true']) !!}
                @elseif ($edit)
                    {!! Form::select('tipo_caja_id', App\ListHelper::getTipoCajas(), $producto->tipo_caja_id ?? old('tipo_caja_id'), ['class' => 'select2', 'id' => 'tipo_caja_id', 'placeholder' => 'Seleccione el tipo de caja...', 'required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'tipo_caja_id'])
                @else
                    {!! Form::select('tipo_caja_id', App\ListHelper::getTipoCajas(), old('tipo_caja_id'), ['class' => 'select2', 'id' => 'tipo_caja_id', 'placeholder' => 'Seleccione el tipo de caja...', 'required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'tipo_caja_id'])
                @endif
            </div>
        </div>
        --}}
        <label class="col-sm-1 col-form-label">Familia</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('familia_producto_id') ? ' has-danger' : '' }}">
                @if ($show)
                    {!! Form::select('familia_producto_id', App\ListHelper::getFamiliaProductos(), $producto->familia_producto_id ?? old('familia_producto_id'), ['class' => 'select2', 'id' => 'familia_producto_id', 'placeholder' => 'Seleccione la familia...', 'required' => 'true', 'disabled' => 'true']) !!}
                @elseif ($edit)
                    {!! Form::select('familia_producto_id', App\ListHelper::getFamiliaProductos(), $producto->familia_producto_id ?? old('familia_producto_id'), ['class' => 'select2', 'id' => 'familia_producto_id', 'placeholder' => 'Seleccione la familia...', 'required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'familia_producto_id'])
                @else
                    {!! Form::select('familia_producto_id', App\ListHelper::getFamiliaProductos(), old('familia_producto_id'), ['class' => 'select2', 'id' => 'familia_producto_id', 'placeholder' => 'Seleccione la familia...', 'required' => 'true']) !!}
                    @include('alerts.feedback', ['field' => 'familia_producto_id'])
                @endif
            </div>
        </div>
        {{--
    </div>
    <div class="row">
        <label class="col-sm-1 col-form-label">Alto (cm)</label>
        <div class="col-sm-2">
            <div class="form-group{{ $errors->has('alto') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('alto') ? ' is-invalid' : '' }}" name="alto" id="alto"
                    type="text" number="true" placeholder="Alto" @if ($create) value="{{ old('alto') }}" @else value="{{ $producto->alto, old('alto') }}" @endif required="true" number="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'alto'])
            </div>
        </div>
        <label class="col-sm-1 col-form-label">Ancho (cm)</label>
        <div class="col-sm-2">
            <div class="form-group{{ $errors->has('ancho') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('ancho') ? ' is-invalid' : '' }}" name="ancho" id="ancho"
                    type="text" number="true" placeholder="Ancho" @if ($create) value="{{ old('ancho') }}" @else value="{{ $producto->ancho, old('ancho') }}" @endif required="true" number="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'ancho'])
            </div>
        </div>
        <label class="col-sm-1 col-form-label">Fondo (cm)</label>
        <div class="col-sm-2">
            <div class="form-group{{ $errors->has('fondo') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('fondo') ? ' is-invalid' : '' }}" name="fondo" id="fondo"
                    type="text" number="true" placeholder="Fondo" @if ($create) value="{{ old('fondo') }}" @else value="{{ $producto->fondo, old('fondo') }}" @endif required="true" number="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'fondo'])
            </div>
        </div>
        <label class="col-sm-1 col-form-label">Volumen (cm)</label>
        <div class="col-sm-2">
            <div class="form-group{{ $errors->has('volumen') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('volumen') ? ' is-invalid' : '' }}" name="volumen"
                    id="volumen" type="text" number="true" placeholder="Volumen" @if ($create) value="{{ old('volumen') }}" @else value="{{ $producto->volumen, old('volumen') }}" @endif required="true" number="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'volumen'])
            </div>
        </div>
    </div>

    <div class="row">
            --}}
        <label class="col-sm-1 col-form-label">Cantidad</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('cantidad') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('cantidad') ? ' is-invalid' : '' }}" name="cantidad"
                    id="cantidad" type="text" number="true" placeholder="Cantidad" @if ($create) value="{{ old('cantidad') }}" @else value="{{ $producto->cantidad, old('cantidad') }}" @endif required="true" number="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'cantidad'])
            </div>
        </div>
        {{--
        <label class="col-sm-1 col-form-label">Peso (g)</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('peso') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('peso') ? ' is-invalid' : '' }}" name="peso" id="peso"
                    type="text" number="true" placeholder="Peso" @if ($create) value="{{ old('peso') }}" @else value="{{ $producto->peso, old('peso') }}" @endif required="true" number="true" aria-required="true" @if ($show)
                disabled @endif/>
                @include('alerts.feedback', ['field' => 'peso'])
            </div>
        </div>
            --}}
    </div>
</div>
<div class="card-footer ml-auto mr-auto">
    @if ($create)
        <button type="submit" class="btn btn-success">Crear</button>
    @endif
    @if ($edit)
        <button type="submit" class="btn btn-success">Editar</button>
    @endif
    @if ($show)
        <form action="{{ route('producto.destroy', $producto->id) }}" method="post">
            @csrf
            @method('delete')
            <a rel="tooltip" class="btn btn-success" href="{{ route('producto.edit', $producto->id) }}"
                data-original-title="" title=""> Editar
            </a>

            <button type="button" class="btn btn-danger" data-original-title="" title="" onclick="Swal.fire({
                                        title: 'Estás seguro?',
                                        text: 'Se borrará el registro!',
                                        type: 'warning',
                                        showCancelButton: true,
                                        buttonsStyling: false, 
                                        confirmButtonClass: 'btn btn-danger',
                                        cancelButtonClass: 'btn btn-success',
                                        confirmButtonText: 'Si, borralo!',
                                        cancelButtonText: 'No, quiero conservarlo!'
                                    }).then((result) => {
                                        if (result.value) {
                                            this.parentElement.submit()
                                        }
                                    })">Eliminar</button>
        </form>
    @endif
</div>
