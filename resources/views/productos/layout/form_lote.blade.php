<input type="hidden" name="lote_id_{{$i}}" value="{{$lotes[$i]->id}}">
<input type="hidden" name="inicial_producto_id_{{$i}}" value="{{$lotes[$i]->producto_id}}">
<div class="card-body ">
    <div class="row">
        <label class="col-sm-1 col-form-label">Producto</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('producto_id_'.$i) ? ' has-danger' : '' }}">
                {{-- <select id="producto_id_{{$i}}" class="select2" name="producto_id_{{$i}}" aria-label="Producto" onchange="validarSelect({{$lotes[$i]->producto_id}},'producto_id_{{$i}}',['div_producto_{{$i}}','div_codigo_producto_{{$i}}'])"> --}}
                {{-- <select id="producto_id_{{$i}}" class="select2" name="producto_id_{{$i}}" aria-label="Producto" onchange="getValueFromSelect(this.value,'producto_id_{{$i}}','nombre_producto_{{$i}}')"> --}}
                <select id="producto_id_{{$i}}" class="select2" name="producto_id_{{$i}}" aria-label="Producto" onchange="ShowSelected('producto_id_{{$i}}','nombre_producto_{{$i}}')">
                    @foreach (App\Producto::all(['id', 'nombre']) as $producto)
                        @if ($producto->id == $lotes[$i]->producto_id)
                            <option value="{{ $producto->id }}" selected>{{ $producto->nombre }}</option>
                        @else
                            <option value="{{ $producto->id }}">{{ $producto->nombre }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <label class="col-sm-1 col-form-label">Nombre</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('nombre_producto_'.$i) ? ' has-danger' : '' }}">
                <input data-date-format="YYYY-MM-DD"
                    class="form-control {{ $errors->has('nombre_producto_'.$i) ? ' is-invalid' : '' }}"
                    name="nombre_producto_{{$i}}" id="nombre_producto_{{$i}}" type="text" placeholder="Nombre Producto" value="{{ $productos[$i]->nombre, old('nombre_producto_'.$i) }}" aria-required="true"/>
                @include('alerts.feedback', ['field' => 'nombre_producto_'.$i])
            </div>
        </div>
        {{-- <div id="div_codigo_producto_{{$i}}" class="col-sm-6 row">
            <label class="col-sm-2 col-form-label">Código Producto</label>
            <div class="col-sm-10">
                <div class="form-group{{ $errors->has('codigo_producto_'.$i) ? ' has-danger' : '' }}">
                    <input
                    class="form-control {{ $errors->has('codigo_producto_'.$i) ? ' is-invalid' : '' }}"
                    name="codigo_producto_{{$i}}" id="codigo_producto_{{$i}}" type="text" placeholder="Nombre" value="{{ $productos[$i]->codigo, old('codigo_producto_'.$i) }}" required aria-required="true"/>
                    @include('alerts.feedback', ['field' => 'codigo_producto_'.$i])
                </div>
            </div>
        </div> --}}
    </div>

    {{-- <div id="div_producto_{{$i}}" class="row">
        <label class="col-sm-1 col-form-label">Nombre</label>
        <div class="col-sm-11">
            <div class="form-group{{ $errors->has('nombre_producto_'.$i) ? ' has-danger' : '' }}">
                <input data-date-format="YYYY-MM-DD"
                    class="form-control {{ $errors->has('nombre_producto_'.$i) ? ' is-invalid' : '' }}"
                    name="nombre_producto_{{$i}}" id="nombre_producto_{{$i}}" type="text" placeholder="Nombre Producto" value="{{ $productos[$i]->nombre, old('nombre_producto_'.$i) }}" aria-required="true"/>
                @include('alerts.feedback', ['field' => 'nombre_producto_'.$i])
            </div>
        </div>
    </div> --}}
    <div class="row">
        {{-- <label class="col-sm-1 col-form-label">Nombre Lote</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('nombre_lote_'.$i) ? ' has-danger' : '' }}">
                <input
                    class="form-control {{ $errors->has('nombre_lote_'.$i) ? ' is-invalid' : '' }}"
                    name="nombre_lote_{{$i}}" id="nombre_lote_{{$i}}" type="text" placeholder="Nombre Lote" value="{{ $lotes[$i]->nombre, old('nombre_lote_'.$i) }}" required aria-required="true"/>
                @include('alerts.feedback', ['field' => 'nombre_lote_'.$i])
            </div>
        </div> --}}

        {{-- <div id="div_codigo_producto_{{$i}}" class="col-sm-6 row">
            <label class="col-sm-2 col-form-label">Código Producto</label>
            <div class="col-sm-10">
                <div class="form-group{{ $errors->has('codigo_producto_'.$i) ? ' has-danger' : '' }}">
                    <input
                    class="form-control {{ $errors->has('codigo_producto_'.$i) ? ' is-invalid' : '' }}"
                    name="codigo_producto_{{$i}}" id="codigo_producto_{{$i}}" type="text" placeholder="Nombre" value="{{ $productos[$i]->codigo, old('codigo_producto_'.$i) }}" required aria-required="true"/>
                    @include('alerts.feedback', ['field' => 'codigo_producto_'.$i])
                </div>
            </div>
        </div> --}}
        
        <label class="col-sm-1 col-form-label">Lote</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('lote_'.$i) ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('lote_'.$i) ? ' is-invalid' : '' }}" name="lote_{{$i}}" id="lote_{{$i}}"
                    type="text" placeholder="Lote" value="{{ $lotes[$i]->lote, old('lote_'.$i) }}" required aria-required="true"/>
                @include('alerts.feedback', ['field' => 'lote_'.$i])
            </div>
        </div>

        <label class="col-sm-1 col-form-label">Código Lote</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('codigo_lote_'.$i) ? ' has-danger' : '' }}">
                <input data-date-format="YYYY-MM-DD"
                    class="form-control {{ $errors->has('codigo_lote_'.$i) ? ' is-invalid' : '' }}"
                    name="codigo_lote_{{$i}}" id="codigo_lote_{{$i}}" type="text" placeholder="Código" value="{{ $lotes[$i]->codigo, old('codigo_lote_'.$i) }}" digits="true" aria-required="true" required />
                @include('alerts.feedback', ['field' => 'codigo_lote_'.$i])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-1 col-form-label">Caducidad</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('caducidad_'.$i) ? ' has-danger' : '' }}">
                <input data-date-format="YYYY-MM-DD"
                    class="form-control datepicker {{ $errors->has('caducidad_'.$i) ? ' is-invalid' : '' }}"
                    name="caducidad_{{$i}}" id="caducidad_{{$i}}" type="text" placeholder="Caducidad_{{$i}}" value="{{ $lotes[$i]->caducidad, old('caducidad_'.$i) }}" required aria-required="true"/>
                @include('alerts.feedback', ['field' => 'caducidad_'.$i])
            </div>
        </div>
        <label class="col-sm-1 col-form-label">Cantidad</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('cantidad_'.$i) ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('cantidad_'.$i) ? ' is-invalid' : '' }}" 
                    name="cantidad_{{$i}}" id="cantidad_{{$i}}" type="text" placeholder="Cantidad_{{$i}}" value="{{ $lotes[$i]->cantidad, old('cantidad_'.$i) }}" required digits="true" aria-required="true"/>
                @include('alerts.feedback', ['field' => 'cantidad_'.$i])
            </div>
        </div>
    </div>
    {{-- <div class="row">
        <label class="col-sm-1 col-form-label">Código Lote</label>
        <div class="col-sm-5">
            <div class="form-group{{ $errors->has('codigo_lote_'.$i) ? ' has-danger' : '' }}">
                <input data-date-format="YYYY-MM-DD"
                    class="form-control {{ $errors->has('codigo_lote_'.$i) ? ' is-invalid' : '' }}"
                    name="codigo_lote_{{$i}}" id="codigo_lote_{{$i}}" type="text" placeholder="Código" value="{{ $lotes[$i]->codigo, old('codigo_lote_'.$i) }}" digits="true" aria-required="true" required />
                @include('alerts.feedback', ['field' => 'codigo_lote_'.$i])
            </div>
        </div>
    </div> --}}
    <div class="row">
        <label class="col-sm-1 col-form-label">Descripción</label>
        <div class="col-sm-11">
            <div class="form-group{{ $errors->has('descripcion_'.$i) ? ' has-danger' : '' }}">
                <input data-date-format="YYYY-MM-DD"
                    class="form-control {{ $errors->has('descripcion_'.$i) ? ' is-invalid' : '' }}"
                    name="descripcion_{{$i}}" id="descripcion_{{$i}}" type="text" placeholder="Descripción" value="{{ $lotes[$i]->descripcion, old('descripcion_'.$i) }}" aria-required="true"/>
                @include('alerts.feedback', ['field' => 'descripcion_'.$i])
            </div>
        </div>
    </div>
</div>