<input type="hidden" name="entrada_id" value="{{$entrada->id}}">
<input type="hidden" id="inicial_proveedor_id" name="inicial_proveedor_id" value="{{$entrada->proveedor_id}}">
<input type="hidden" id="inicial_tipo_entrada_id" name="inicial_tipo_entrada_id" value="{{$entrada->tipo_entrada_id}}">

<div class="card-body ">
    <div class="row">
        <label class="col-sm-2 col-form-label">Fecha</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('fecha') ? ' has-danger' : '' }}">
                <input type="text" data-date-format="YYYY-MM-DD HH:mm:ss"
                    class="form-control datetimepicker {{ $errors->has('fecha') ? ' is-invalid' : '' }}" name="fecha"
                    id="fecha" placeholder="fecha" value="{{ $entrada->fecha, old('fecha') }}" required="true" aria-required="true"/>
                @include('alerts.feedback', ['field' => 'fecha'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Proveedor</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('proveedor_id') ? ' has-danger' : '' }}">
                <select id="proveedor_id" class="select2" name="proveedor_id" aria-label="Proveedor"  onchange="ShowSelected('proveedor_id','nombre_proveedor')">
                    @foreach (App\Proveedor::all(['id', 'nombre']) as $model)
                        @if ($model->id == $entrada->proveedor_id)
                            <option value="{{ $model->id }}" selected>{{ $model->nombre }}</option>
                        @else
                            <option value="{{ $model->id }}">{{ $model->nombre }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div id="div_proveedor" class="row">
        <label class="col-sm-2 col-form-label">Nombre Proveedor</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('nombre_proveedor') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('nombre_proveedor') ? ' is-invalid' : '' }}" name="nombre_proveedor"
                    id="nombre_proveedor" type="text" placeholder="Nombre Proveedor" value="{{ $proveedor->nombre,old('nombre_proveedor') }}" required="true" aria-required="true" />
                @include('alerts.feedback', ['field' => 'nombre_proveedor'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Tipo Entrada</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('tipo_entrada_id') ? ' has-danger' : '' }}">
                <select id="tipo_entrada_id" class="select2" name="tipo_entrada_id" aria-label="Tipo Entrada" onchange="ShowSelected('tipo_entrada_id','nombre_tipo_entrada')">
                    @foreach (App\TipoEntrada::all(['id', 'nombre']) as $tipo_entrada)
                            @if ($tipo_entrada->id == $entrada->tipo_entrada_id)
                                <option value="{{ $tipo_entrada->id }}" selected>{{ $tipo_entrada->nombre }}
                                </option>
                            @else
                                <option value="{{ $tipo_entrada->id }}">{{ $tipo_entrada->nombre }}</option>
                            @endif
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div id="div_tipo_entrada" class="row">
        <label class="col-sm-2 col-form-label">Tipo Entrada</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('nombre_tipo_entrada') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('nombre_tipo_entrada') ? ' is-invalid' : '' }}" name="nombre_tipo_entrada" 
                    id="nombre_tipo_entrada" type="text" placeholder="Nombre Proveedor" value="{{ $tipoEntrada->nombre,old('nombre_tipo_entrada') }}" required="true" aria-required="true" />
                @include('alerts.feedback', ['field' => 'nombre_tipo_entrada'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Estado</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('estado') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" name="estado"
                    id="estado" type="text" placeholder="Estado" value="{{ $entrada->estado, old('estado') }}" required="true" aria-required="true"/>
                @include('alerts.feedback', ['field' => 'estado'])
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Folio Compra</label>
        <div class="col-sm-7">
            <div class="form-group{{ $errors->has('folio_compra') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('folio_compra') ? ' is-invalid' : '' }}"
                    name="folio_compra" id="folio_compra" type="text" placeholder="Folio Compra" value="{{ $entrada->folio_compra, old('folio_compra') }}" required="true" aria-required="true"/>
                @include('alerts.feedback', ['field' => 'folio_compra'])
            </div>
        </div>
    </div>
</div>
