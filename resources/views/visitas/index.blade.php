@extends('layouts.app', ['activePage' => 'visita', 'menuParent' => 'consultas', 'titlePage' => __('Consulta de
productos')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="fas fa-search fa-2x"></i>
                            </div>
                            <h4 class="card-title">Consulta de productos</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="col-form-label">Almacen</label>
                                    <div class="form-group{{ $errors->has('almacen_id') ? ' has-danger' : '' }}">
                                        {!! Form::select('almacenes', App\ListHelper::getAlmacenes(), null, ['class' => 'select2', 'id' => 'almacenes', 'placeholder' => 'Seleccione el almacen...', 'onchange' => 'cargar("almacenes")']) !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label class="col-form-label">Rack</label>
                                    <div class="form-group ">
                                        {!! Form::select('racks', App\ListHelper::getRacks(), null, ['class' => 'select2', 'id' => 'racks', 'placeholder' => 'Seleccione el rack...', 'onchange' => 'cargar("racks")']) !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label class="col-form-label">Ubicacion</label>
                                    <div class="form-group">
                                        {!! Form::select('ubicaciones', App\ListHelper::getUbicaciones(), null, ['class' => 'select2', 'id' => 'ubicaciones', 'placeholder' => 'Seleccione la ubicacion...', 'onchange' => 'cargar("ubicaciones")']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="col-form-label">Lote Producto</label>
                                    <div class="">
                                        <div class="form-group">
                                            {!! Form::select('lote_productos', App\ListHelper::getLoteProductos(), null, ['class' => 'select2', 'id' => 'lote_productos', 'placeholder' => 'Seleccione el lote...', 'onchange' => 'cargar("lote_productos")']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-sm">
                                    <label class="col-form-label">Tipo Entrada</label>
                                    <div class="">
                                        <div class="form-group">
                                            {!! Form::select('tipo_entradas', App\ListHelper::getTipoEntradas(), null, ['class' => 'select2', 'id' => 'tipo_entradas', 'placeholder' => 'Seleccione el tipo...', 'onchange' => "cargar('tipo_entradas')"]) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <label class="col-form-label">Entrada</label>
                                    <div class="">
                                        <div class="form-group">
                                            {!! Form::select('entradas', App\ListHelper::getEntradas(), null, ['class' => 'select2', 'id' => 'entradas', 'placeholder' => 'Seleccione la entrada...', 'onchange' => "cargar('entradas')"]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm">
                                    <label class="col-form-label">Proveedor</label>
                                    <div class="">
                                        <div class="form-group">
                                            {!! Form::select('proveedores', App\ListHelper::getProveedores(), null, ['class' => 'select2', 'id' => 'proveedores', 'placeholder' => 'Seleccione el proveedor...', 'onchange' => "cargar('proveedores')"]) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <label class="col-form-label">Familia Producto</label>
                                    <div class="">
                                        <div class="form-group{{ $errors->has('rack_id') ? ' has-danger' : '' }}">
                                            {!! Form::select('familia_productos', App\ListHelper::getFamiliaProductos(), null, ['class' => 'select2', 'id' => 'familia_productos', 'placeholder' => 'Seleccione la familia...']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-sm">
                                    <label class="col-form-label">Tipo Caja</label>
                                    <div class="">
                                        <div class="form-group">
                                            {!! Form::select('tipo_cajas', App\ListHelper::getTipoCajas(), null, ['class' => 'select2', 'id' => 'tipo_cajas', 'placeholder' => 'Seleccione el tipo...']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row d-flex justify-content-center">
                                <button type="button" class="btn btn-success" onclick=" table.ajax.reload();"><i
                                        class="align-middle" data-feather="search"></i>Buscar Productos</button>

                                <button type="button" class="btn btn-warning" onclick=" table.ajax.reload();"><i
                                        class="align-middle" data-feather="search"></i>Limpiar filtros</button>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header card-header-danger card-header-icon">
                            <div class="card-icon">
                                <i class="fab fa-product-hunt fa-2x"></i>
                            </div>
                            <h4 class="card-title">Productos consultados</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="tabla" class="table table-striped table-no-bordered table-hover"
                                    style="width:100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        let table = null;

        $(document).ready(function() {
            search();
        });

        function search() {
            table = $('#tabla').DataTable({
                processing: true,
                scrollX: true,
                dom: 'Blfrtip',
                ajax: {
                    url: "{{ route('productos') }}",
                    dataSrc: "",
                    data: function(d) {
                        d.tipo_caja = $('#tipo_cajas').val();
                        d.lote = $('#lote_productos').val();
                        d.ubicacion = $('#ubicaciones').val();
                        d.rack = $('#racks').val();
                        d.almacen = $('#almacenes').val();
                        d.familia_producto = $('#familia_productos').val();
                        d.proveedor = $('#proveedores').val();
                        d.entrada = $('#entradas').val();
                        d.tipo_entrada = $('#tipo_entradas').val();
                    }
                },

                columns: [{
                        title: "Código",
                        data: "codigo",
                        name: "codigo"
                    },
                    {
                        title: "Tipo Caja",
                        data: "tipo_caja",
                        name: "tipo_caja"
                    },
                    {
                        title: "Familia Producto",
                        data: "familia",
                        name: "familia"
                    },
                    {
                        title: "Proveedor",
                        data: "proveedor",
                        name: "proveedor"
                    },
                    {
                        title: "Entrada",
                        data: "entradas",
                        name: "entradas"
                    },
                    {
                        title: "Tipo Entrada",
                        data: "tipo_entradas",
                        name: "tipo_entradas"
                    },
                    {
                        title: "Lote",
                        data: "lotes",
                        name: "lotes"
                    },
                    {
                        title: "Ubicacion",
                        data: "ubicaciones",
                        name: "ubicaciones"
                    },
                    {
                        title: "Rack",
                        data: "racks",
                        name: "racks"
                    },
                    {
                        title: "Almacen",
                        data: "almacenes",
                        name: "almacenes"
                    },
                ],
                buttons: [{
                        extend: "excelHtml5",
                        className: "btn btn-success",
                        text: "Exportar a excel <i class='far fa-file-excel'></i>",
                        title: "PPRODUCTOS CONSULTADOS",
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn btn-danger ml-2 mr-5",
                        text: "Exportar a pdf <i class='far fa-file-pdf'></i>",
                        title: "PPRODUCTOS CONSULTADOS",
                        footer: true,
                    }
                ],
                select: true
            });
        }

        function cargar(select) {

            let valor = document.querySelector('#' + select).value;

            switch (select) {
                case 'tipo_entradas':
                    cargarSelect('tipo_entradas', 'entradas', 1, valor);
                    cargarSelect('entradas', 'proveedores', 1, valor);
                    cargarSelect('proveedores', 'familia_productos', 1, valor);
                    break;
                case 'entradas':
                    cargarSelect('entradas', 'proveedores', 2, valor);
                    cargarSelect('proveedores', 'familia_productos', 2, valor);
                    break;
                case 'proveedores':
                    cargarSelect('proveedores', 'familia_productos', 3, valor);
                    break;

                case 'almacenes':
                    cargarSelect('almacenes', 'racks', 1, valor);
                    cargarSelect('racks', 'ubicaciones', 1, valor);
                    cargarSelect('ubicaciones', 'lote_productos', 1, valor);
                    break;
                case 'racks':
                    cargarSelect('racks', 'ubicaciones', 2, valor);
                    cargarSelect('ubicaciones', 'lote_productos', 2, valor);
                    break;
                case 'ubicaciones':
                    cargarSelect('ubicaciones', 'lote_productos', 3, valor);
                    break;
            }
        }

        function cargarSelect(select1, select2, opc, valor) {
            let id = document.querySelector('#' + select1).value;

            let url = "{{ route('entradas', ['opc', 'temp']) }}";
            url = url.replace('entradas', select2);
            url = url.replace('opc', opc);
            url = url.replace('temp', valor);

            $.ajax({
                url,
                type: 'GET',
                dataType: "JSON",
                success: function(data) {
                    $("#" + select2).empty();
                    $("#" + select2).append('<option value="">Selecciona una opcion</option>');
                    $.each(data, function(id, dato) {
                        $("#" + select2).append('<option value="' + dato.clave + '">' + dato.valor +
                            '</option>');
                    });
                }
            });
        }
    </script>
@endpush
