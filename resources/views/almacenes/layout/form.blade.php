    <div class="card-body ">
        <div class="row">
            <div class="col-md-12 text-right">
                <a href="{{ route('almacen.index') }}"
                    class="btn btn-sm btn-success">{{ __('Regresar a la lista') }}</a>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Nombre</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre"
                        id="nombre" type="text" placeholder="Nombre" @if ($create) value="{{ old('nombre') }}" @else value="{{ $almacen->nombre, old('nombre') }}" @endif @if ($show) disabled @endif required="true"
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'nombre'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Encargado</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('encargado') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('encargado') ? ' is-invalid' : '' }}" name="encargado"
                        id="encargado" type="text" placeholder="Encargado" @if ($create) value="{{ old('encargado') }}" @else value="{{ $almacen->encargado, old('encargado') }}" @endif @if ($show) disabled @endif
                    required="true" aria-required="true" />
                    @include('alerts.feedback', ['field' => 'encargado'])
                </div>
            </div>
            <label class="col-sm-2 col-form-label">Teléfono</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('telefono') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }}" name="telefono"
                        id="telefono" type="tel" placeholder="Teléfono" @if ($create) value="{{ old('telefono') }}" @else value="{{ $almacen->telefono, old('telefono') }}" @endif @if ($show) disabled @endif
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'telefono'])
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label">Dirección</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('direccion') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion"
                        id="direccion" type="text" placeholder="Dirección" @if ($create) value="{{ old('direccion') }}" @else value="{{ $almacen->direccion, old('direccion') }}" @endif @if ($show) disabled @endif
                    required="true" aria-required="true" />
                    @include('alerts.feedback', ['field' => 'direccion'])
                </div>
            </div>
            <label class="col-sm-2 col-form-label">País</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('pais') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('pais') ? ' is-invalid' : '' }}" name="pais" id="pais"
                        type="text" placeholder="País" @if ($create) value="{{ old('pais') }}" @else value="{{ $almacen->pais, old('pais') }}" @endif @if ($show) disabled @endif
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'pais'])
                </div>
            </div>
            <label class="col-sm-2 col-form-label">Estado</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('estado') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" name="estado"
                        id="estado" type="text" placeholder="Estado" @if ($create) value="{{ old('estado') }}" @else value="{{ $almacen->estado, old('estado') }}" @endif @if ($show) disabled @endif
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'estado'])
                </div>
            </div>
            <label class="col-sm-2 col-form-label">Ciudad</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('ciudad') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('ciudad') ? ' is-invalid' : '' }}" name="ciudad"
                        id="ciudad" type="text" placeholder="Ciudad" @if ($create) value="{{ old('ciudad') }}" @else value="{{ $almacen->ciudad, old('ciudad') }}" @endif @if ($show) disabled @endif
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'ciudad'])
                </div>
            </div>
            <label class="col-sm-2 col-form-label">División</label>
            <div class="col-sm-4">
                <div class="form-group{{ $errors->has('division') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('ciudad') ? ' is-invalid' : '' }}" name="division"
                        id="division" type="text" digits="true" placeholder="División" @if ($create) value="{{ old('division') }}" @else value="{{ $almacen->division, old('division') }}" @endif @if ($show) disabled @endif
                    aria-required="true" />
                    @include('alerts.feedback', ['field' => 'division'])
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer ml-auto mr-auto">
        @if ($create)
            <button type="submit" class="btn btn-success">Crear</button>
        @endif
        @if ($edit)
            <button type="submit" class="btn btn-success">Editar</button>
        @endif
        @if ($show)
            <form action="{{ route('almacen.destroy', $almacen->id) }}" method="post">
                @csrf
                @method('delete')
                <a rel="tooltip" class="btn btn-success" href="{{ route('rack.edit', $almacen->id) }}"
                    data-original-title="" title=""> Editar
                </a>

                <input type="submit" class="btn btn-danger" value="Eliminar" />
            </form>
        @endif
    </div>
