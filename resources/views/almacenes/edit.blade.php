@extends('layouts.app', ['activePage' => 'almacen', 'menuParent' => 'catalogos', 'titlePage' => __('Almacen')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ route('almacen.update', $almacen) }}"
                        autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('put')

                        <div class="card ">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                  <i class="fas fa-warehouse fa-2x"></i>
                                </div>
                                <h4 class="card-title">Editar Almacén</h4>
                            </div>
                            @include('almacenes.layout.form',['show' => false,'edit' => true, 'create' => false])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
