<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;
use App\Salida;

Route::get('/', function () {
  return redirect('login');
});

Auth::routes();

/* Rutas estaticas */
Route::get('home', 'HomeController@index')->name('home');
Route::get('dashboard', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('role', 'RoleController', ['except' => ['show', 'destroy']]);
    Route::resource('user', 'UserController', ['except' => ['show']]);
    
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

    Route::resource('almacen','AlmacenController');
    Route::resource('proveedor','ProveedorController');
    Route::resource('mensajero','MensajeroController');
    Route::resource('rack','RackController');
    Route::resource('ubicacion','UbicacionController');
    Route::resource('entrada','EntradaController');
    Route::resource('cliente','ClienteController');
    Route::resource('tipocaja','TipoCajaController');
    Route::resource('paqueteria','PaqueteriaController');
    Route::resource('detalle_entrada','DetalleEntradaController');
    Route::resource('materiales','MaterialController');
    Route::resource('producto','ProductoController');
    Route::resource('lote_producto','LoteProductoController');
    Route::resource('recolector','RecolectorController');

    Route::resource('estatus_factura','EstatusFacturaController');
    Route::resource('tipo_entrada','TipoEntradaController');
    Route::resource('conductor','ConductorController');    
    Route::resource('tipo_salida','TipoSalidaController');
    Route::resource('familia_producto','FamiliaProductoController');
    Route::resource('tipo_envio','TipoEnvioController');
    Route::resource('salida','SalidaController');    
    Route::resource('factura','FacturaController');   
    Route::resource('detalle_factura','DetalleFacturaController');

    Route::get('guia/{guia}/{factura}/edit','GuiaController@edit')->name('guia.editar');

    Route::resource('guia','GuiaController');
    Route::resource('ruta_embarque','RutaEmbarqueController');
    Route::post('/producto/imprimir/etiqueta',[App\Http\Controllers\ProductoController::class, 'getImprimirEtiquetas'])->name('producto.imprimir.etiqueta');
    Route::get('/producto/info/etiqueta',[App\Http\Controllers\ProductoController::class, 'getInfoEtiquetas'])->name('producto.info.etiqueta');
    Route::post('/rack/imprimir/etiqueta',[App\Http\Controllers\RackController::class, 'getImprimirEtiquetas'])->name('rack.imprimir.etiqueta');
    Route::get('/rack/info/etiqueta',[App\Http\Controllers\RackController::class, 'getInfoEtiquetas'])->name('rack.info.etiqueta');
    Route::get('/facturas/notificacion', [App\Http\Controllers\FacturaController::class, 'getFacturasPorSurtir'])->name('facturas.notificacion');
    Route::get('/facturas/notificacion', [App\Http\Controllers\FacturaController::class, 'getFacturasPorSurtir'])->name('facturas.notificacion');
    Route::get('/facturas/detalles', [App\Http\Controllers\FacturaController::class, 'getDetalle'])->name('facturas.detalles');
    Route::get('/facturas/crear_detalle/{id_factura}', [App\Http\Controllers\FacturaController::class, 'create_detalle'])->name('facturas.detalle_create');
    Route::get('/facturas/filtros', [App\Http\Controllers\FacturaController::class, 'filtros'])->name('facturas.filtros');
    Route::get('/facturas/consulta/filtros', [App\Http\Controllers\FacturaController::class, 'consultaFacturas'])->name('facturas.consulta');
    Route::get('/factura/{factura}/show', [App\Http\Controllers\FacturaController::class, 'showFactura'])->name('facturas.show');
    Route::get('/entrada/crear/detalle/{entrada}', [App\Http\Controllers\DetalleEntradaController::class, 'createDetalle'])->name('entrada.create.detalle');
    Route::get('/producto/crear/lote/{producto}', [App\Http\Controllers\LoteProductoController::class, 'createLote'])->name('producto.create.lote');
    Route::get('/home/consulta', [App\Http\Controllers\HomeController::class, 'consulta'])->name('consulta');
    Route::get('/home/consulta_factura', [App\Http\Controllers\HomeController::class, 'consulta_factura'])->name('consulta_factura');
    Route::get('/home/consulta_pie', [App\Http\Controllers\HomeController::class, 'consulta_pie'])->name('consulta_pie');
    Route::post('/salida/estatus/mensajero',[App\Http\Controllers\SalidaController::class, 'getEstatusMensajero'])->name('salida.estatus.mensajero');
    Route::post('/salida/asignacion/mensajero',[App\Http\Controllers\SalidaController::class, 'getAsignacionMensajero'])->name('salida.asignacion.mensajero');
    Route::post('/guia/imprimir/etiqueta',[App\Http\Controllers\GuiaController::class, 'getImprimirEtiquetas'])->name('guia.imprimir.etiqueta');
    Route::get('/guia/info/etiqueta',[App\Http\Controllers\GuiaController::class, 'getInfoEtiquetas'])->name('guia.info.etiqueta');

    Route::get('/visita', 'VisitaController@visita')->name('visita');
    Route::get('/tipocajas', 'VisitaController@tipoCajas')->name('tipocajas');
    Route::get('/tipo_entradas/{entrada}', 'VisitaController@tipoEntradas')->name('tipoentradas');
    Route::get('/detalle_entradas', 'VisitaController@detalleEntradas')->name('detalle_entradas');

    Route::get('/entradas/{tipo}', 'VisitaController@entradas')->name('entradas2');
    Route::get('/proveedores/{entrada}', 'VisitaController@proveedores')->name('proveedores');
    Route::get('/familia_productos/{producto}', 'VisitaController@familiaProductos')->name('familia_productos');
    Route::get('/racks/{almacen}', 'VisitaController@racks')->name('racks');
    Route::get('/ubicaciones/{rack}', 'VisitaController@ubicaciones')->name('ubicaciones');

    Route::get('/facturas/subir', function () {return view('facturas.upload');})->name('facturas.upload');
    Route::post('/facturas/guardar', 'FacturaController@subirFactura')->name('factura.guardar');
    Route::get('/productos/subir', function () {return view('productos.upload');})->name('productos.upload');
    Route::post('/productos/guardar', 'ProductoController@subirProducto')->name('producto.guardar');
    Route::post('/productos/confirmar-producto', 'ProductoController@confirmarProducto')->name('producto.confirmar');
    //Primer Camino
    Route::get('/entradas/{opc}/{tipoE}','VisitaController@entradas')->name('entradas');
    Route::get('/proveedores/{opc}/{entrada}','VisitaController@proveedores')->name('proveedores');
    Route::get('/familia_productos/{opc}/{proveedor}','VisitaController@familiaProductos')->name('familia_productos');

    Route::get('/racks/{opc}/{dato}','VisitaController@racks')->name('racks');
    Route::get('/ubicaciones/{opc}/{dato}','VisitaController@ubicaciones')->name('ubicaciones');
    Route::get('/lote_productos/{opc}/{dato}','VisitaController@loteProductos')->name('lote_productos');

    Route::get('/productos','VisitaController@productos')->name('productos');
    Route::get('/salidas/mensajero', [App\Http\Controllers\SalidaController::class, 'mensajero'])->name('salidas.upload');
    Route::get('/salidas/amensajero', [App\Http\Controllers\SalidaController::class, 'amensajero'])->name('salidas.asignacion');
});


//Rutas Android
Route::post('/setToken','RecolectorController@setToken')->name('setToken');
Route::post('/arreglo','FacturaController@password');
Route::post('/por-surtir','FacturaController@getPorSurtir');
Route::post('/todas','FacturaController@getTodas');
Route::post('/get-factura','FacturaController@getFactura');
Route::post('/actualizar-factura','FacturaController@actualizarFactura');
Route::post('/recolectores','RecolectorController@getRecolectores')->name('recolectores');
Route::post('/ver-ubicaciones','UbicacionController@getUbicaciones');
Route::post('/ver-productos','ProductoController@getProductos');
Route::post('/subir-fotos','FacturaController@subirFotos');
Route::post('/actualizar-lote','UbicacionController@actualizarLote');
Route::post('/ver-detalles','DetalleFacturaController@getDetalles');

Route::post('/get-estatus-factura','EstatusFacturaController@getAllEstatus');
Route::post('/get-producto','ProductoController@getProducto');
Route::post('/get-ubicacion','UbicacionController@getUbicacion');
Route::post('/get-almacenes', function () {return App\ListHelper::getAllAlmacenes();});
Route::post('/get-via-embarques', function () {return App\ListHelper::getAllPaqueterias();});
Route::post('/actualizar-estatus-factura','FacturaController@actualizarEstatus');
// Route::get('/get-tipos-envio','EstatusFacturaController@getAllEstatus');


Route::post('/modificar-lotes','LoteProductoController@modificarLotes');
Route::post('/asignar-ubicacion','LoteUbicacionController@asignarUbicacionToProduct');
Route::post('/lotesByUbicacion','LoteProductoController@lotesByUbicacion');
