<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoteProducto extends Model
{
    use HasFactory;
    protected $table = 'lote_producto';
    protected $fillable = [
        'nombre',
        'lote',
        'caducidad',
        'cantidad',
        'estatus',
        'comodin',
        'cantidad_actual',
        'capturado_por',
        'actualizado_por',
        'producto_id',
        'codigo',
        'codigo2',
        'qr_data',
        'descripcion',
    ];

    public function producto(){
        return $this->belongsTo('App\Producto');
    }
}
