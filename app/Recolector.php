<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recolector extends Model
{
    use HasFactory;
    protected $table = 'c_recolectore';
    protected $fillable = [
        'nombre',
        'pass',
        'almacen_id',
        'secuencia',
        'estatus',
        'capturado_por',
        'actualizado_por',
    ];

    public function almacen(){
        return $this->belongsTo('App\Almacen');
    }
}
