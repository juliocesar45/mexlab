<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Salida extends Model
{
    use HasFactory;
    protected $table = 'salida';
    protected $fillable = [
        'fecha',
        'tipo_salida_id',
        'cliente_id',
        'factura_id',
        'guia_id',
        'resultado_envio',
        'notas',
        'mensajero_id',
        'ruta_embarque_id',
        'estatus',
        'capturado_por',
        'actualizado_por',
    ];

    public function tipo_salida(){
        return $this->belongsTo('App\TipoSalida');
    }

    public function cliente(){
        return $this->belongsTo('App\Cliente');
    }

    public function factura(){
        return $this->belongsTo('App\Factura');
    }

    public function guia(){
        return $this->belongsTo('App\Guia');
    }

    public function ruta_embarque(){
        return $this->belongsTo('App\RutaEmbarque');
    }

    public function mensajero(){
        return $this->belongsTo('App/Mensajero');
    }
}
