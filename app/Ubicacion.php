<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ubicacion extends Model
{
    use HasFactory;
    protected $table = 'c_ubicacione';
    protected $fillable = [
        'lote_producto_id',
        'rack_id',
        'slot',
        'x',
        'y',
        'seccion',
        'alto',
        'ancho',
        'identificador',
        'cantidad',
        'qr_data',
        'capturado_por',
        'actualizado_por',
    ];

    public function rack(){
        return $this->belongsTo('App\Rack');
    }

    public function loteProducto(){
        return $this->belongsTo('App\LoteProducto');
    }
}
