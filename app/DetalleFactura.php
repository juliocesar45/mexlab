<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleFactura extends Model
{
    use HasFactory;
    protected $table = 'detalle_factura';
    protected $fillable = [
        'factura_id',
        'producto_id',
        'cantidad',
        'notas',
        'descripcion',
        'lote_caducidad',
        'lote_producto_id',
        'unidad',
        'codigo',
        'codigo2',
        'estatus',
        'recolector_id',
        'capturado_por',
        'actualizado_por',
        'ubicacion',
        'producto'
    ];

    public function factura(){
        return $this->belongsTo('App\Factura');
    }

    public function lote_producto(){
        return $this->belongsTo('App\LoteProducto');
    }
}
