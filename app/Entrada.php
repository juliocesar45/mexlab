<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entrada extends Model
{
    use HasFactory;
    protected $table = 'entrada';
    protected $fillable = [
        'fecha',
        'proveedor_id',
        'tipo_entrada_id',
        'estado',
        'folio_compra',
        'capturado_por',
        'actualizado_por',
    ];

    public function detalle_entradas(){
        return $this->hasMany('App\DetalleEntrada');
    }

    public function tipo_entrada(){
        return $this->belongsTo('App\TipoEntrada');
    }

    public function proveedor(){
        return $this->belongsTo('App\Proveedor');
    }
}
