<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoEntrada extends Model
{
    use HasFactory;
    protected $table = 'c_tipo_entrada';
    protected $fillable = [
        'nombre',
        'capturado_por',
        'actualizado_por',
    ];

    public function entradas()
    {
        return $this->hasMany('App\Entrada');
    }
}
