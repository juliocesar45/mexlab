<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;
    protected $table = 'producto';
    protected $fillable = [
        'nombre',
        'material_id',
        'tipo_caja_id',
        'familia_producto_id',
        'alto',
        'ancho',
        'fondo',
        'volumen',
        'cantidad',
        'peso',
        'codigo',
        'qr_data',
        'capturado_por',
        'actualizado_por',
    ];

    public function familiaProducto(){
        return $this->belongsTo('App\FamiliaProducto');
    }

    public function tipoCaja(){
        return $this->belongsTo('App\TipoCaja');
    }

    public function material(){
        return $this->belongsTo('App\Material');
    }

    // public function loteProductos()
    // {
    //     return $this->hasMany('App\LoteProducto');
    // }
}
