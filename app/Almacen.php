<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Almacen extends Model
{
    use HasFactory;
    protected $table = 'c_almacenes';
    protected $fillable = [
        'nombre',
        'direccion',
        'encargado',
        'telefono',
        'ciudad',
        'pais',
        'estado',
        'capturado_por',
        'actualizado_por',
    ];

    public function racks()
    {
        return $this->hasMany('App\Rack');
    }

}
