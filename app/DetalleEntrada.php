<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleEntrada extends Model
{
    use HasFactory;
    protected $table = 'detalle_entrada';
    protected $fillable = [
        'entrada_id',
        'cantidad',
        'cantidad_mal_estado',
        'capturado_por',
        'actualizado_por',
    ];

    public function entrada(){
        return $this->belongsTo('App\Entrada');
    }

    public function proveedor(){
        return $this->belongsTo('App\Proveedor');
    }
}
