<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoteUbicacion extends Model
{
    use HasFactory;
    protected $table = 'lote_ubicacion';
    protected $fillable = [
        'c_ubicacione_id',
        'lote_producto_id',
        'cantidad'
    ];
}
