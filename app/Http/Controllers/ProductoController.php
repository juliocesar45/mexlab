<?php

namespace App\Http\Controllers;

use App\Entrada;
use App\Factura;
use App\LoteProducto;
use App\Producto;
use App\Proveedor;
use App\TipoEntrada;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode as FacadesQrCode;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($qrCodes = [])
    {
        
        if(!count($qrCodes))
            return view('productos.index', ['productos' => Producto::all(),'qrCodes'=>$qrCodes]);
        return view('productos.index', ['productos' => Producto::all(),'qrCodes'=>$qrCodes,'alert'=>"Factura Subida Correctamente"])->withStatus('Registro creado correctamente.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('productos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'codigo' => ['required', 'numeric', 'unique:producto,codigo'],
            'familia_producto_id' => 'required|numeric',
            'cantidad' => 'required|numeric',
            'nombre' => 'required',
        ]);

        $qr_data = $request->codigo;
        $request->request->add(['qr_data' => $qr_data]);
        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        Producto::create($request->all());
        return redirect()->route('producto.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $producto)
    {
        $loteProductos = LoteProducto::where('producto_id', $producto->id)->get();
        return view('productos.show', compact('producto', 'loteProductos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        return view('productos.edit', compact('producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producto $producto)
    {
        $validacion = $request->codigo === $producto->codigo ? 'required' : ['required', 'numeric', 'unique:producto,codigo'];

        $request->validate([
            'codigo' => $validacion,
            'tipo_caja_id' => 'required|numeric',
            'familia_producto_id' => 'required|numeric',
            'alto' => 'required|numeric',
            'ancho' => 'required|numeric',
            'fondo' => 'required|numeric',
            'volumen' => 'required|numeric',
            'cantidad' => 'required|numeric',
            'peso' => 'required|numeric',
            'nombre' => 'required',
        ]);

        $qr_data = $request->codigo;
        $request->request->add(['qr_data' => $qr_data]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $producto->update($request->all());
        return redirect()->route('producto.show', $producto)->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {
        $producto->delete();
        $loteProductos = LoteProducto::where('producto_id', $producto->id)->get();
        foreach ($loteProductos as $loteProducto) {
            $loteProducto->delete();
        }
        return redirect()->route('producto.index')->withStatus(__('El registro fue borrado.'));
    }

    public function getProducto(Request $request){

        if ($request[0] != "4dm1n")
            return [];

        $query = 'select lp.id,lp.lote, lp.codigo,lp.nombre
                    from lote_producto lp
                    where lp.id = ' . $request[1];

        $productos = DB::select($query);
                    

        return $productos;
    }

    public function getInfoEtiquetas(Request $request)
    {
        return LoteProducto::find($request->id_lote);
    }

    public function getImprimirEtiquetas(Request $request)
    {
        $lote =  LoteProducto::find($request->id_lote);
        $request = $request->all();

        $qr_data = DB::select('SELECT lp.id, pr.nombre, lp.codigo, lp.codigo2, lp.lote, "producto" AS tipo, lp.caducidad
                                FROM lote_producto lp 
                                JOIN producto pr
                                on lp.producto_id = pr.id
                                WHERE lp.id =' . $lote->id);

        $qrcode = base64_encode(FacadesQrCode::format('png')->size(200)->errorCorrection('H')->generate($qr_data[0]->id.''));
        $pdf = PDF::loadView('productos.etiquetas', compact('request', 'lote', 'qrcode'));

        // download PDF file with download method
        return $pdf->download('etiquetas.pdf');
    }



    public function generarQR($idLote)
    {
        //$qrcode = base64_encode(FacadesQrCode::format('png')->size(200)->errorCorrection('H')->generate(json_encode($arreglo[])));
        $qrcode = base64_encode(FacadesQrCode::format('png')->size(200)->errorCorrection('H')->generate($idLote.''));
        return $qrcode;
    }

    public function subirProducto(Request $request)
    {
        
        // $json = '{"lotes":[{"nombre":"FOSFATASA ALCALINA CINETICA 60 ml","lote":"034301","cantidad":40,"codigo":"8001400","caducidad":"2023-06-30","descripcion":"FOSFATASA ALCALINA CINETICA 1x50ml+1x10ml ","producto_id":13,"capturado_por":"Esteban Sevilla","actualizado_por":"Esteban Sevilla","estatus":"ACTIVO","updated_at":"2022-01-25T20:26:36.000000Z","created_at":"2022-01-25T20:26:36.000000Z","id":452},{"nombre":"REACTIVO LIZANTE PARA HEMATOLOGIA CON 10","lote":"1367","cantidad":12,"codigo":"7001502","caducidad":"2022-12-31","descripcion":"REACTIVO LIZANTE PARA HEMATOLOGIA CON 1000 ML. ","producto_id":14,"capturado_por":"Esteban Sevilla","actualizado_por":"Esteban Sevilla","estatus":"ACTIVO","updated_at":"2022-01-25T20:26:37.000000Z","created_at":"2022-01-25T20:26:37.000000Z","id":453}],"productos":[{"id":13,"created_at":"2021-12-11T09:57:51.000000Z","updated_at":"2021-12-11T09:57:51.000000Z","tipo_caja_id":null,"familia_producto_id":null,"alto":0,"ancho":0,"fondo":0,"volumen":0,"cantidad":0,"codigo":"8001400","peso":0,"qr_data":"","capturado_por":"","actualizado_por":"","nombre":"FOSFATASA ALCALINA CINETICA 60 ml"},{"id":14,"created_at":"2021-12-11T09:57:51.000000Z","updated_at":"2021-12-11T09:57:51.000000Z","tipo_caja_id":null,"familia_producto_id":null,"alto":0,"ancho":0,"fondo":0,"volumen":0,"cantidad":0,"codigo":"7001502","peso":0,"qr_data":"","capturado_por":"","actualizado_por":"","nombre":"REACTIVO LIZANTE PARA HEMATOLOGIA CON 10"}],"proveedor":[{"id":2,"nombre":"Cardia Diagnostica Bio, S.A DE C.V","capturado_por":"Esteban Sevilla","actualizado_por":"Esteban Sevilla","created_at":"2021-12-11T09:57:51.000000Z","updated_at":"2021-12-11T09:57:51.000000Z"}],"tipo_entrada":{"id":1,"nombre":"Factura de compra","capturado_por":"","actualizado_por":"","created_at":null,"updated_at":null},"entrada":{"fecha":"2022-01-25T20:26:37.292844Z","tipo_entrada_id":1,"estado":"En buen estado.","folio_compra":"678","proveedor_id":2,"capturado_por":"Esteban Sevilla","actualizado_por":"Esteban Sevilla","updated_at":"2022-01-25T20:26:37.000000Z","created_at":"2022-01-25T20:26:37.000000Z","id":15}}';
        // $arreglo = json_decode($json);
        // return view('productos.confirmar_productos',[
        //     'lotes'=>$arreglo->lotes,
        //     'productos'=>$arreglo->productos,
        //     'proveedor'=>$arreglo->proveedor[0],
        //     'entrada'=>$arreglo->entrada,
        //     'tipoEntrada'=>$arreglo->tipo_entrada,
        // ]);

        if ($request->hasFile('producto')) {
            $producto = File::get($request->file('producto'));

            $replace = ['<span style="white-space:pre;">','<span>','</span><br>','</span>'];
            $producto = str_replace($replace, "", $producto);

            #Obtenemos folio de compra
            $folio = explode('>',explode('</div',explode('Fecha',$producto)[1])[1])[2];
            #Obtenemos proveedor de la compra
            $proveedor = explode('>', explode('</div', explode('Comprado', $producto)[1])[2])[2];
            $proveedor = str_replace('&nbsp;', ' ', $proveedor);

            $productos = explode('/Pedimento</div>', $producto);


            if (count($productos) > 2) {
                $texto_junto = '';
                $validacion = 0;
                foreach ($productos as $row) {
                    if ($validacion > 0)
                        $texto_junto .= $row . ' ';
                    $validacion++;
                }
                
                /* Removemos el codigo basura */
                $codigo_html = array(
                    'font6" style="top:901px;left:514px;height:15px;width:201px;',
                    '<div class="font7" style="top:900px;left:719px;height:15px;width:48px;border-width:0px;border-color:RGB(0,0,0);color:RGB(0,0,0);background-color:transparent;">1&nbsp;/&nbsp;2</div>',
                    '<div class="basenotprint" style="border-width:0px;border-top-width:1px;top:1054px;left:0px;height:1px;width:814px;"></div>',
                    '</a></div><a name="XFRXPAGE_1">', '</a><div class="page" style="width:816;height:1056;page-break-before:always;"><a name="XFRXPAGE_1"></a><a name="XFRXPAGE_2">',
                    '<div class="', 'font2" style="', 'base" style="', 'border-color:RGB(0,0,0);', 'text-align: right;', 'color:RGB(0,0,0);',
                    'background-color:transparent;"', 'border-width:0px;', 'border-left-width:1px;', 'text-align: center;', '</div', 'font3" style="',
                    'border-style:solid;', 'border-color:RGB(128,128,128);', 'background-color:RGB(128,128,128);', 'border-top-width:1px;', 'font7"', 'font6', 'font1', 'font5', 'Importe',
                    'Subtotal', 'TOTAL', 'font4', 'I.V.A.', 'Pagina:', 'font8"', 'Elaborado', 'con', 'top:1017px;left:718px;height:24px;width:31px;',
                    'page', '</a', 'Contrarecibo de Compra', 'EMITIDA', 'Tel:', 'Fax:'
                );
                $producto = str_replace($codigo_html, '', $texto_junto);
                /* Barremos página por página para quitar la paginación */
                $paginas = explode('<img', $producto);
                /* Vaciamos el texto anterior para volverlo a cargar */
                $producto = '';
                /* Removemos contenido sobre paginacion */
                foreach ($paginas as $pagina) {
                    if (count(explode('top:443px;left:141px;height:28px;width:82px;>', $pagina)) > 1)
                        $pagina = explode('top:443px;left:141px;height:28px;width:82px;>', $pagina)[1];
                    $producto .= explode('style="top:998px;left:545px;height:16px;width:156px;>>', $pagina)[0];
                }
            } else {
                /* Removemos el codigo basura */
                $codigo_html = array(
                    'font6" style="top:901px;left:514px;height:15px;width:201px;',
                    '<div class="font7" style="top:900px;left:719px;height:15px;width:48px;border-width:0px;border-color:RGB(0,0,0);color:RGB(0,0,0);background-color:transparent;">1&nbsp;/&nbsp;2</div>',
                    '<div class="basenotprint" style="border-width:0px;border-top-width:1px;top:1054px;left:0px;height:1px;width:814px;"></div>',
                    '</a></div><a name="XFRXPAGE_1">', '</a><div class="page" style="width:816;height:1056;page-break-before:always;"><a name="XFRXPAGE_1"></a><a name="XFRXPAGE_2">',
                    '<div class="', 'font2" style="', 'base" style="', 'border-color:RGB(0,0,0);', 'text-align: right;', 'color:RGB(0,0,0);',
                    'background-color:transparent;"', 'border-width:0px;', 'border-left-width:1px;', 'text-align: center;', '</div', 'font3" style="',
                    'border-style:solid;', 'border-color:RGB(128,128,128);', 'background-color:RGB(128,128,128);', 'border-top-width:1px;', 'font7"', 'font6', 'font1', 'font5', 'Importe',
                    'Subtotal', 'TOTAL', 'font4', 'I.V.A.'
                );
                $producto = str_replace($codigo_html, '', $productos[1]);
            }
            //Removemos espacios y saltos de linea
            $producto = trim(preg_replace('/\s\s+/', '', $producto));
            //Quitemos los &nbsp; por espacios
            $producto = str_replace('&nbsp;', ' ', $producto);
            //Quitamos los &#8209; por guion
            $producto = str_replace('&#8209;', '-', $producto);
            $productos = explode("PESOS", $producto);
            $productos = explode('style', $productos[0]);
            $info = array();
            $productos_extraidos = array();
            foreach ($productos as $row) {
                $info = explode(">", $row);

                foreach ($info as $line) {

                    if (
                        $line != "" && substr($line, 0, 4) != '="to' && substr($line, 0, 4) != 'top:'
                        && $line != '" ' && $line != " " && substr($line, 0, 4) != '<img' &&
                        substr($line, 0, 2) != '<a' && substr($line, 0, 1) != '=' && !str_contains($line, '"')
                        && substr($line, 0, 2) != '  ' && !str_contains($line, 'Contrarecibo')
                        && !str_contains($line, 'Folio')  && !str_contains($line, 'Fecha')
                        && !str_contains($line, 'Entregar') && !str_contains($line, 'ALMACEN')
                    )
                        // dd(substr($line, 0, 4));
                        array_push($productos_extraidos, $line);
                }
            }
            $bandera_corte = false; #Bandera para remover la última información no necesaria.
            $productos_recortados = array();
            #Validación para quitar los últomos registros
            foreach ($productos_extraidos as $line) {
                #Validamos que el registro despues de un registro con / tenga una coma
                if ($bandera_corte) {
                    $bandera_corte = false;
                    if (str_contains($line, ',')) {
                        break;
                    }
                }
                #Validamos que la linea tenga un /
                if ($this->validateDate($line, 'd/m/Y')) {
                    $bandera_corte = true;
                }
                array_push($productos_recortados, $line);
            }

            $lote = "";
            $clave = "";
            $cantidad = 0;
            $nombre = "";
            $contador = 0;
            $caducidad = "";
            $descripcion = "";

            //Se guardaran todos los lotes de producto
            $lotes = [];
            $productos = [];


            foreach ($productos_recortados as $line) {
                /* Obtenemos la cantidad de los productos */
                if ($contador == 0)
                    $cantidad = intval($line);
                /* Obtenemos el nombre del producto */
                if ($contador == 2)
                    $nombre = $line;
                /* Obtenemos el lote */
                if ($contador == 4)
                    $lote = $line;
                /* Obtenemos la clave */
                if ($contador == 5)
                    $clave = $line;
                /* Descripcion y Caducidad */
                if ($contador >= 6) {
                    if ($this->validateDate($line, 'd/m/Y')) {
                        $caducidad = $line;
                        $contador = -1;
                        $fecha = explode('/', $caducidad);
                        $producto = Producto::where('codigo', $clave)->where('nombre', $nombre)->get()->first();
                        if ($producto) {
                            $lotes[] = LoteProducto::create([
                                'nombre' => $nombre,
                                'lote' => $lote,
                                'cantidad' => $cantidad,
                                'codigo' => $clave,
                                'caducidad' => $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0],
                                'descripcion' => $descripcion,
                                'producto_id' => $producto->id,
                                'capturado_por' => Auth::user()->name,
                                'actualizado_por' => Auth::user()->name,
                                'estatus' => 'ACTIVO'
                            ]);
                        } else {
                            $producto_nuevo = Producto::create([
                                'nombre' => $nombre,
                                'codigo' => $clave,
                            ]);

                            $lotes[] = LoteProducto::create([
                                'nombre' => $nombre,
                                'lote' => $lote,
                                'cantidad' => $cantidad,
                                'codigo' => $clave,
                                'caducidad' => $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0],
                                'descripcion' => $descripcion,
                                'producto_id' => $producto_nuevo->id,
                                'capturado_por' => Auth::user()->name,
                                'actualizado_por' => Auth::user()->name,
                                'estatus' => 'ACTIVO'
                            ]);

                            $producto = $producto_nuevo;
                        }

                        $productos[] = $producto;

                        $arreglo = [
                            'id'=>end($lotes)->id,
                            'nombre'=>$producto->nombre, 
                            'codigo'=>end($lotes)->codigo,
                            'codigo2'=>end($lotes)->codigo2,
                            'lote'=>end($lotes)->lote,
                            'tipo'=>'producto',
                            'caducidad'=>end($lotes)->caducidad
                        ];

                        $descripcion = '';
                    } else {
                        $descripcion .= $line . ' ';
                    }
                }

                $contador++;
            }

            #Búscar registro del proveedor
            $proveedor_obj = Proveedor::where('nombre', $proveedor)->get();
            $tipo_entrada;
            $entrada;

            if (count($proveedor_obj) > 0) {
                #Crear registro
                $tipo_entrada = TipoEntrada::where('nombre','Factura de compra')->get()[0];
                $entrada = Entrada::create([
                    'fecha' => Carbon::now(),
                    'tipo_entrada_id' => $tipo_entrada->id,
                    'estado' => 'En buen estado.',
                    'folio_compra' => $folio,
                    'proveedor_id' => $proveedor_obj[0]->id,
                    'capturado_por' => Auth::user()->name,
                    'actualizado_por' => Auth::user()->name,
                ]);
               
            } else {
                $proveedor_obj = Proveedor::create([
                    'nombre' => $proveedor,
                    'capturado_por' => Auth::user()->name,
                    'actualizado_por' => Auth::user()->name,

                ]);

                $entrada = Entrada::create([
                    'fecha' => Carbon::now(),
                    'tipo_entrada_id' => 1,
                    'estado' => 'En buen estado.',
                    'folio_compra' => $folio,
                    'proveedor_id' => $proveedor_obj->id,
                    'capturado_por' => Auth::user()->name,
                    'actualizado_por' => Auth::user()->name,
                ]);
            
            }

            return view('productos.confirmar_productos',[
                'lotes'=>$lotes,
                'productos'=>$productos,
                'proveedor'=>$proveedor_obj[0],
                'entrada'=>$entrada,
                'tipoEntrada'=>$tipo_entrada,
            ]);
        } else {
            return redirect()->route('productos.upload')->withStatus(__('Algo salio mal.'));
        }
    }

    private function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function getProductos(Request $request)
    {
        if ($request[0] != "4dm1n")
            return [];

        $query = 'select lp.id, lp.lote, lp.cantidad, lp.nombre, lp.codigo
                    from lote_producto lp
                    where lp.id = ' . $request[1] . ' order by caducidad';

        $productos = DB::select($query);

        return $productos;
    }

    public function confirmarProducto(Request $request){
        $proveedorID = $request->inicial_proveedor_id;
        $tipoEntradaID = $request->inicial_tipo_entrada_id;
        $entradaID = $request->entrada_id;
        $producto = [];
        $lote = [];
        $QRs = [];
        $i = 0;

        $proveedor = [
            'nombre' => $request->nombre_proveedor,
        ];

        $tipoEntrada = [
            'nombre' => $request->nombre_tipo_entrada,
        ];

        $entrada = [
            'fecha' => $request->fecha,
            'proveedor_id' => $request->proveedor_id,
            'tipo_entrada_id' => $request->tipo_entrada_id,
            'estado' => $request->estado,
            'folio_compra' => $request->folio_compra,
        ];


        $modelProveedor = Proveedor::where('id',$proveedorID);
        if($proveedorID == $entrada['proveedor_id']){
            $modelProveedor->update($proveedor);
        }
        

        $modelEntrada = TipoEntrada::where('id',$tipoEntradaID);
        if($tipoEntradaID == $entrada['tipo_entrada_id']){
            if($tipoEntradaID != 1)
                $modelEntrada->update($tipoEntrada);
        }
        
        Entrada::where('id',$entradaID)->update($entrada);


        while(true){
            if($request['producto_id_'.$i]){
                $productoID = $request['inicial_producto_id_'.$i]; //es el id del producto con el que fue creado
                $productoIDNuevo = $request['producto_id_'.$i]; //es el nuevo id de producto, sirve en caso que se haya modificado
                $producto['codigo'] = $request['codigo_lote_'.$i];
                $producto['nombre'] = $request['nombre_producto_'.$i];

                $loteID = $request['lote_id_'.$i];
                $lote['producto_id'] = $productoID;
                $lote['nombre'] = $request['nombre_producto_'.$i];
                $lote['lote'] = $request['lote_'.$i];
                $lote['caducidad'] = $request['caducidad_'.$i];
                $lote['cantidad'] = $request['cantidad_'.$i];
                $lote['codigo'] = $request['codigo_lote_'.$i];
                $lote['descripcion'] = $request['descripcion_'.$i];

                $modelProducto = Producto::where('id',$productoID);
                if($productoID == $productoIDNuevo){
                    $modelProducto->update($producto);
                }

                LoteProducto::where('id',$loteID)->update($lote);
                $QRs[] = ['nombre'=>$lote['lote'].' - '.$lote['nombre'], 'img' => $this->generarQR($loteID)];
                $i++;
            } else {
                break;
            }
        }

        return $this->index($QRs);
    }
}
