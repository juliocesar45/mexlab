<?php

namespace App\Http\Controllers;

use App\Almacen;
use App\Cliente;
use App\DetalleFactura;
use App\EstatusFactura;
use App\Factura;
use App\FamiliaProducto;
use App\Guia;
use App\Recolector;
use App\LoteProducto;
use App\Paqueteria;
use App\Producto;
use App\Salida;
use App\TipoEnvio;
use App\TipoSalida;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use function PHPSTORM_META\type;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;
use SimpleXMLElement;
use App\Helpers\FacturaXML;

class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('facturas.index', ['facturas' => Factura::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('facturas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cliente_id' => 'required',
            'cantidad' => 'required',
            'estatus_factura_id' => 'required',
            'identificador' => 'required',
        ]);



        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);


        $recolector = DB::select('select count(*) pendiente,cr.token, cr.id, nombre
                                    from c_recolectore cr
                                    left join factura fac
                                    on fac.recolector_id = cr.id
                                    where estatus=\'ACTIVO\' AND estatus_factura_id=10
                                    group by cr.id
                                    order by pendiente
                                    limit 1');

        $request->request->add(['recolector_id' => $recolector[0]->id]);


        $factura = Factura::create($request->all());

        $detalle = "Por surtir factura $factura->identificador";
        $link = "https://www.google.com.mx/";
        $token = $recolector[0]->token;

        // $this->sendNotify('MEXLAB', $detalle, $link, $token);
        return redirect()->route('factura.show', $factura)->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function show(Factura $factura)
    {
        return view('facturas.show', ['factura'=>$factura,'guia'=>$factura->guia]);
    }


    public function showFactura($factura){
        $factura = Factura::where('identificador',$factura)->firstOrFail();
        return view('facturas.show', ['factura'=>$factura,'guia'=>$factura->guia]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function edit(Factura $factura)
    {
        return view('facturas.edit', compact('factura'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Factura $factura)
    {
        $request->validate([
            'cliente_id' => 'required',
            'cantidad' => 'required',
            'estatus_factura_id' => 'required',
            'identificador' => 'required',
        ]);

        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $factura->update($request->all());
        return redirect()->route('factura.show', $factura)->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function destroy(Factura $factura)
    {
        $factura->update(['estatus_factura_id' => 12]);
        return redirect()->route('factura.index');
    }

    public function create_detalle($id_factura)
    {
        return view('detalle_facturas.create', ['id_factura' => $id_factura]);
    }

    public function getFacturasPorSurtir(Request $request)
    {
        $facturas = Factura::where('estatus_factura_id', 10)->get();
        $total_facturas = array();
        if ($facturas) {
            foreach ($facturas as $factura) {
                array_push($total_facturas, array(
                    'id' => $factura->id,
                    'identificador' => $factura->identificador,
                ));
            }
            return json_encode($total_facturas);
        }
    }

    /**
     * Ingresar facutura del html del otro sistema aquí.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function subirFactura(Request $request)
    {
        //Comprobamos que exista el archivo factura
        if (!$request->hasFile('factura')) return redirect()->route('facturas.upload')->withStatus(__('Suba una factura válida'));
        
        //Cargamos el texto html de la factura
        $factura_string = File::get($request->file('factura'));
        
        //Carmos nuestra instancia de Factura
        $factura = new \App\Helpers\Factura($factura_string);

        //Buscamos el modelo de Familia Producto `SIN FAMILIA`
        $familia_producto = FamiliaProducto::where('nombre', 'SIN FAMILIA')->get()[0];

        //Validamos si la factura ya se había subido
        if(Factura::where('identificador',$factura->getFolio())->first()!=null) 
            return redirect()->route('facturas.upload')->withStatus(__('La Factura('.$factura->getFolio().') ya se había subido'));

        //Buscamos si el cliente ya existe
        $cliente = Cliente::where('rfc', $factura->getRFC())->first();

        //Si el cliente no existe lo creamos
        if($cliente==null) $cliente = Cliente::create([
            'nombre_completo' => $factura->getNombreContactoPara(),
            'rfc' => $factura->getRFC(),
            'domicilio' => $factura->getDomicilio(),
            'domicilio_fiscal' => $factura->getDomicilio(),
            'telefonos' => $factura->getTelefonos(),
            'capturado_por' => Auth::user()->name,
            'actualizado_por' => Auth::user()->name,
        ]);

        //Buscamos al recolector con menos facturas asignadas para asignarle una nueva
        $recolector = Recolector::where('estatus', 'ACTIVO')->orderby('secuencia', 'ASC')->limit(1)->first();

        //Consultamos el modelo del estatus de la factura `Por surtir`
        $estatus_factura = EstatusFactura::where('nombre', 'Por surtir')->first();

        //Buscamos si el almacen ya existe
        $almacen = Almacen::where('nombre', $factura->getAlmacen())->first();

        //Si no existe el almacen, lo creamos
        if($almacen==null)$almacen = Almacen::create(['nombre' => $almacen]);

        //Validamos que tenga paqueteria; Buscamos si la paqueteria ya existe.
        $paqueteria = $factura->getPaqueteria() ? Paqueteria::where('nombre', $factura->getPaqueteria())->first() : false;

        //Si no existe la paqueteria, creamos el modelo
        if ($paqueteria===null) $paqueteria = Paqueteria::create([
            'nombre' => $factura->getPaqueteria(),
            'capturado_por' => Auth::user()->name,
            'actualizado_por' => Auth::user()->name,
        ]);

        //Validamos que exista el tipo de envio; Buscamos si el tipo de envio ya existe.
        $tipo_envio = $factura->getTipoEnvio() ? TipoEnvio::where('nombre', $factura->getTipoEnvio())->first() : false;

        //Si no existe la tipo de envio, creamos el modelo
        if($tipo_envio===null) $tipo_envio = TipoEnvio::create([
            'nombre' => $factura->getTipoEnvio(),
            'capturado_por' => Auth::user()->name,
            'actualizado_por' => Auth::user()->name,
        ]);

        //Creamos el modelo de la factura
        $factura_model = Factura::create([
            'cliente_id' => $cliente->id,
            'cantidad' => count($factura->getConceptos()),
            'estatus_factura_id' => $estatus_factura->id,
            'identificador' => $factura->getFolio(),
            'fecha_embarque' => now(),
            'recolector_id' => $recolector->id,
            'capturado_por' => Auth::user()->name,
            'actualizado_por' => Auth::user()->name,
            'agente' => $factura->getAgente(),
            'nota' => $request->nota,
            'estatus' => $request->estatus,
            'almacen_id' => $almacen->id,
            'fecha_emision' => $factura->getFechaHoraEmision()
        ]);

        //Recorremos los productos de la factura
        foreach ($factura->getConceptos() as $concepto){
            //Buscamos si el producto ya existe
            $producto = Producto::where('nombre', $concepto->getDescripcion())->first();

            //Si el producto no existe lo creamos
            if($producto===null) $producto = Producto::create([
                'codigo' => $concepto->getCodigo(),
                'familia_producto_id' => $familia_producto->id,
                'alto' => 0,
                'ancho' => 0,
                'fondo' => 0,
                'volumen' => 0,
                'cantidad' => $concepto->getUnidades(),
                'peso' => 0,
                'capturado_por' => Auth::user()->name,
                'actualizado_por' => Auth::user()->name,
                'nombre' => $concepto->getDescripcion(),
            ]);

            //Buscamos si el lote ya existe
            $lote_producto = LoteProducto::where('lote', $concepto->getLote())->first();

            //Si el lote no existe lo creamos
            if($lote_producto===null){
                if($concepto->getLote()!==null) $lote_producto = LoteProducto::create([
                    'nombre' => $concepto->getDescripcion(),
                    'lote' => $concepto->getLote(),
                    'cantidad' =>0,
                    'codigo' => $concepto->getCodigo(),
                    'caducidad' => $concepto->getFechaCaducidad(),
                    'descripcion' => $concepto->getDescripcion(),
                    'producto_id' => $producto->id,
                    'capturado_por' => Auth::user()->name,
                    'actualizado_por' => Auth::user()->name,
                    'estatus' => 'ACTIVO'
                ]);
            } else {//Si ya existe el stock actualizamos la cantidad del lote
                $lote_producto->update(['cantidad' => ($lote_producto->cantidad - $concepto->getUnidades())]);
            }

            //Creamos el detalle de la factura para cada producto pedido
            DetalleFactura::create([
                'factura_id' => $factura_model->id,
                'cantidad' => $concepto->getUnidades(),
                'descripcion' => $concepto->getDescripcion(),
                'lote_caducidad' => $concepto->getLoteCaducidad(),
                'lote_producto_id' => $lote_producto ? $lote_producto->id : null,
                'producto_id' => $lote_producto ? $lote_producto->id : null,
                'unidad' => $concepto->getTipoUnidad(),
                'codigo' => $concepto->getCodigo(),
                'codigo2' => $concepto->getCodigo(),
                'estatus' => 1,
                'recolector_id' => $recolector->id,
                'capturado_por' => Auth::user()->name,
                'actualizado_por' => Auth::user()->name,
            ]);
        }

        //Creamos la guía de envio para la factura
        Guia::create([
            "cliente_id" => $cliente->id,
            'direccion' => $factura->getDomicilio(),
            'contacto' => $factura->getNombreContacto(),
            'receptor' => $factura->getNombreContactoPara(),
            'tipo_envio_id' => $tipo_envio ? $tipo_envio->id : null,
            'c_paqueteria_id' => $paqueteria ? $paqueteria->id : null,
            'capturado_por' => Auth::user()->name,
            'actualizado_por' => Auth::user()->name,
            'factura_id' => $factura_model->id,
        ]);


        /* Actualizamos el campo secuencia de el recolector */
        #Seteamos todas las secuencias en 0 de los recolectores activos
        foreach (Recolector::where('estatus', 'ACTIVO')->get() as $recolector) {
            $recolector->update(['secuencia' => 0]);
        }

        #Se buscan los recolectores con facturas asignadas el dí­a actual
        $recolectores = Factura::select('recolector_id')->groupby('recolector_id')->whereDate('created_at', '=', Carbon::today()->toDateString())->get();
        foreach ($recolectores as $recolector) {
            $recolector->recolector->update(['secuencia' => count(Factura::where('recolector_id', $recolector->recolector_id)->whereDate('created_at', '=', Carbon::today()->toDateString())->get())]);
        }

        //Enviamos la notificacion al recolector
        $this->sendNotify('MEXLAB', $factura->getFolio(), $recolector->token);

        //Mandamos al show de la factura
        return redirect()->route('factura.show', $factura_model)->withStatus(__('Registro creado correctamente.'));
    }

    private function getTexto($factura, $cadena, $coincidencia = 1)
    {
        $length = strlen($cadena);
        $inicio = 0;

        for ($i = 0; $i < $coincidencia; $i++) {
            $inicio = strpos($factura, $cadena, $inicio) + $length;

            if ($inicio === $length)
                return null;
        }

        $fin = strpos($factura, '</div>', $inicio);
        $subString = substr($factura, $inicio, $fin - $inicio);
        $strtr = strtr($subString, array_flip(get_html_translation_table(HTML_ENTITIES, ENT_QUOTES)));

        return trim($strtr, chr(0xC2) . chr(0xA0));
    }

    public function getDetalle(Request $request)
    {
        $factura_id = $request->id_factura;
        $detalle_factura = DetalleFactura::where('factura_id', $factura_id)->get();
        $total_detalle = array();
        $consecutivo = 0;
        if ($detalle_factura) {
            foreach ($detalle_factura as $detalle) {
                $consecutivo++;

                $qr = 0;
                if ($detalle->producto) {
                    if ($detalle->ubicacion)
                        $qr = 3;
                    else
                        $qr = 1;
                } else {
                    if ($detalle->ubicacion)
                        $qr = 2;
                }

                array_push($total_detalle, array(
                    'Qr' => $qr,
                    'num' => $consecutivo,
                    'cantidad' => $detalle->cantidad,
                    'descripcion' => $detalle->descripcion,
                    'lote_caducidad' => $detalle->lote_caducidad,
                    'capturado_por' => $detalle->capturado_por,
                ));
            }
            return json_encode($total_detalle);
        }
    }

    public function getFactura(Request $request)
    {
        if ($request[0] != '4dm1n')
            return [];

        $factura = Factura::where('identificador', $request[1])->get();

        $cliente = $factura[0]->cliente;
        $almacen = $factura[0]->almacen;
        $guia = $factura[0]->guia;
        $paqueteria = $guia ? $guia->c_paqueteria : null;
        $tipoEnvio = $guia ? $guia->tipo_envio : null;
        $contacto = $guia ? $guia->contacto ? $guia->contacto : null : null;
        $receptor = $guia ? $guia->receptor ? $guia->receptor : null : null;

        $facturaRecolector = [
            "id" => $factura[0]->id,
            "cliente" => $cliente ? $cliente->nombre_completo : 'N/A',
            "domicilio_fiscal" => $cliente ? $cliente->domicilio_fiscal : 'N/A',
            "almacen" => $almacen ? $almacen->nombre : 'N/A',
            "via_embarque" => $paqueteria ? $paqueteria->nombre : 'N/A',
            "tipo_envio" => $tipoEnvio ? $tipoEnvio->nombre : 'N/A',
            "fecha_emision" => $factura[0]->fecha_emision ? $factura[0]->fecha_emision : 'N/A',
            "contacto" => $contacto ? $contacto : 'N/A',
            "receptor" => $receptor ? $receptor : 'N/A',
            "cantidad" => $factura[0]->cantidad ? $factura[0]->cantidad : 'N/A',
            "identificador" => $factura[0]->identificador ? $factura[0]->identificador : 'N/A',
            "agente" => $factura[0]->agente ? $factura[0]->agente : 'N/A',
            "prioridad" => $factura[0]->estatus ? $factura[0]->estatus : 'N/A',
            "estatus" => $factura[0]->estatus_factura->nombre ? $factura[0]->estatus_factura->nombre : 'N/A',
            "observaciones_recolector" => $factura[0]->observaciones_recolector ? $factura[0]->observaciones_recolector : ''
        ];

        return [$facturaRecolector];
    }

    public function getFacturaPorIdentificador($identificador)
    {

        $factura = Factura::where('identificador', $identificador)->first();

        $cliente = $factura->cliente;
        $almacen = $factura->almacen;
        $guia = $factura->guia;
        $paqueteria = $guia ? $guia->c_paqueteria : null;
        $tipoEnvio = $guia ? $guia->tipo_envio : null;
        $contacto = $guia ? $guia->contacto ? $guia->contacto : null : null;
        $receptor = $guia ? $guia->receptor ? $guia->receptor : null : null;

        $factura = [
            "id" => $factura->id,
            "cliente" => $cliente ? $cliente->nombre_completo : 'N/A',
            "domicilio_fiscal" => $cliente ? $cliente->domicilio_fiscal : 'N/A',
            "almacen" => $almacen ? $almacen->nombre : 'N/A',
            "via_embarque" => $paqueteria ? $paqueteria->nombre : 'N/A',
            "tipo_envio" => $tipoEnvio ? $tipoEnvio->nombre : 'N/A',
            "fecha_emision" => $factura->fecha_emision ? $factura->fecha_emision : 'N/A',
            "contacto" => $contacto ? $contacto : 'N/A',
            "receptor" => $receptor ? $receptor : 'N/A',
            "cantidad" => $factura->cantidad ? $factura->cantidad : 'N/A',
            "identificador" => $factura->identificador ? $factura->identificador : 'N/A',
            "agente" => $factura->agente ? $factura->agente : 'N/A',
            "prioridad" => $factura->estatus ? $factura->estatus : 'N/A',
            "estatus" => $factura->estatus_factura->nombre ? $factura->estatus_factura->nombre : 'N/A',
        ];

        return $factura;
    }

    public function saveImage($image, $encabezado)
    {
        $image = $image;  // your base64 encoded
        $image = str_replace('data:image/jpeg;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = $encabezado . time() . '.jpeg';
        \File::put(storage_path() . '/images_base64/' . $imageName, base64_decode($image));
        // \File::put(storage_path(). '/app/public/img/' . $imageName, base64_decode($image));
        return $imageName;
    }

    public function actualizarFactura(Request $request)
    {
        if ($request[0] != '4dm1n')
            return [];

        $factura = Factura::find($request[1]);
        $factura->update(["estatus_factura_id" => 2, "tiempo" => $request[2],'observaciones_recolector'=>$request[3]]);

        $detalles = $request->all();
        $size = sizeof($request->all());
        for ($i = 4; $i < $size; $i++) {
            $detalle = $detalles[$i];
            $id = $detalle["id"];
            unset($detalle['id']);
            $resp = DB::table('detalle_factura')
                ->where('id', $id)
                ->update($detalle);
        }

        return ["EXITO"];
    }



    public function subirFotos(Request $request)
    {
        if ($request[0] != '4dm1n')
            return [];

        $fotoProducto = $this->saveImage($request[2], 'antes_');
        $fotoEmpaquetado = $this->saveImage($request[3], 'despues_');

        $factura = Factura::find($request[1]);
        $factura->update(["foto_producto" => $fotoProducto, "foto_empaquetado" => $fotoEmpaquetado]);

        return ["EXITO"];

        $cliente = Cliente::find($factura->cliente_id);
        $guia = Guia::where('factura_id', $factura->id)->get();

        Salida::create([
            'fecha' => Carbon::now(),
            'tipo_salida_id' => 2,
            'cliente_id' => $cliente->id,
            'factura_id' => $factura->id,
            'guia_id' => $guia[0]->id,
            'estatus_factura_id' => 19,
            'capturado_por' => Auth::user()->name,
            'actualizado_por' => Auth::user()->name,
        ]);
    }


    //FACTURAS POR SURTIR
    public function getPorSurtir(Request $request)
    {
        if ($request->password != "4dm1n")
            return [];
        return Factura::select("identificador")->where('recolector_id', $request->recolector)->where('estatus_factura_id', 10)->get();
    }

    //TODAS LAS FACTURAS
    public function getTodas(Request $request)
    {
        // return $request;
        // $request = ["4dm1n","1",null,"16/01/2022","18/01/2022",0];
        // ["4dm1n","1",null,"10/01/2022",0]
        //$request = ["4dm1n",2,'1','11/01/2022','13/01/2022',1];
        // $request = ["4dm1n",2,'1','11/01/2022','13/01/2022',1];

        if ($request[0] != "4dm1n")
            return [];

        date_default_timezone_set('UTC');
        $campos = ["f.identificador", "f.estatus_factura_id", "f.cantidad", "f.created_at", "f.estatus"];
        $stringCampos = implode(',',$campos);
        $arguments = [];
        // $arguments['recolector_id']=$request[1];

        $query = 'SELECT '.$stringCampos.' FROM factura f
                    INNER JOIN guia g
                    ON g.factura_id=f.id
                    WHERE recolector_id = '.$request[1];;

        // $query = 'SELECT '.$stringCampos.' FROM factura WHERE recolector_id = '.$request[1];

        if($request[2]){
            $query .= " AND f.identificador LIKE '%".$request[2]."%'";
        }  

        if($request[3]){
            $arrayFecha = explode('/',$request[3]);
            $stringFecha = $arrayFecha[2].'-'.$arrayFecha[1].'-'.$arrayFecha[0];
            $query .= " AND f.created_at >= '".$stringFecha." 00:00:00'";
        }

        if($request[4]){
            $arrayFecha = explode('/',$request[4]);
            $stringFecha = $arrayFecha[2].'-'.$arrayFecha[1].'-'.$arrayFecha[0];
            $query .= " AND f.created_at <= '".$stringFecha." 23:59:59'";
        } else {
            $query .= " AND f.created_at <= now()";
        }

        if($request[5]){
            $query .= ' AND f.estatus_factura_id = '.$request[5];
        }

        if($request[6]){
            $query .= ' AND f.almacen_id = '.$request[6];
        }

        if($request[7]){
            $query .= ' AND g.c_paqueteria_id = '.$request[7];
        }

        $query .= ' ORDER BY created_at DESC';

        return DB::select($query);
    }

    public function sendNotify($titulo, $identificador, $token)
    {
        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = 'AAAAWaAVzfM:APA91bGhKFzlkDmXpOdCYaB3UUhvAVtYVD4WJDwHGUvKp_v4TMOl9WsBHeC-HWqUFg-XTaaVvEygWUPY_LBbbWqMD86qy3wyDwxZatC3uV0TfHZvLgs14nN593FoLoiYeS_ZvTv8z7nJ';

        $data = [
            'titulo' => $titulo,
            'detalle' => 'FACTURA: ' . $identificador,
            'factura' => $this->getFacturaPorIdentificador($identificador)
        ];


        $arrayToSend = array('to' => $token, 'data' => $data, 'priority' => 'high');
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
    }


    public function password(Request $request)
    {
        return $request->password;
    }

    /* Consulta de facturas */
    public function filtros(Request $request)
    {
        $fecha_inicial = '';
        $fecha_final = '';

        /* Validamos que tenga fechas */
        if ($request->fecha_inicial && $request->fecha_final) {

            /* LE DAMOS FORMATO A LAS FECHAS */
            $fecha_inicial = Carbon::parse($request->fecha_inicial)->format("Y/m/d");
            $fecha_final = Carbon::parse($request->fecha_final)->format("Y/m/d");

            /* CON FILTROS */
            return DataTables::of(DB::select("SELECT cc.nombre_completo AS cliente, cef.nombre AS estatus_factura, f.identificador, cantidad,
            DATE_FORMAT(f.created_at, '%d/%m/%Y') AS fecha_hora, '' AS salida, f.id
            FROM factura f 
            JOIN c_cliente cc ON f.cliente_id=cc.id 
            JOIN c_estatus_facturas cef ON f.estatus_factura_id=cef.id 
            WHERE f.created_at BETWEEN '$fecha_inicial 00:00' AND '$fecha_final 23:59'
            ORDER BY f.created_at DESC "))

                ->addIndexColumn()
                ->make(true);
        } else {
            /* SIN FILTROS */
            return DataTables::of(DB::select("SELECT cc.nombre_completo AS cliente, cef.nombre AS estatus_factura, f.identificador, cantidad,
                                            DATE_FORMAT(f.created_at, '%d/%m/%Y') AS fecha_hora, '' AS salida, f.id
                                            FROM factura f 
                                            JOIN c_cliente cc ON f.cliente_id=cc.id 
                                            JOIN c_estatus_facturas cef ON f.estatus_factura_id=cef.id 
                                            WHERE DATE(f.created_at) 
                                            ORDER BY f.created_at DESC "))

                ->addIndexColumn()
                ->make(true);
        }
    }


    public function actualizarEstatus(Request $request){
        if ($request[0] != '4dm1n')
            return [];

        $factura = Factura::find($request[1])->update(['estatus_factura_id'=>$request[2]]);

        return [$factura];

    }
}
