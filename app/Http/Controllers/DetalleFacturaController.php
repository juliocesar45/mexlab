<?php

namespace App\Http\Controllers;

use App\DetalleFactura;
use App\Factura;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DetalleFacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('detalle_facturas.index',['detalleFacturas'=>DetalleFactura::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('detalle_facturas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'factura_id'=>'required',
            //'producto_id'=>'required',
            'cantidad'=>'required',
            'notas'=>'required'
        ]);

        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);

        DetalleFactura::create($request->all());
        return redirect()->route('facturas.detalle_create',$request->factura_id)->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DetalleFactura  $detalleFactura
     * @return \Illuminate\Http\Response
     */
    public function show(DetalleFactura $detalleFactura)
    {
        return view('detalle_facturas.edit', compact('detalleFactura'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DetalleFactura  $detalleFactura
     * @return \Illuminate\Http\Response
     */
    public function edit(DetalleFactura $detalleFactura)
    {
        return view('detalle_facturas.edit', compact('detalleFactura'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DetalleFactura  $detalleFactura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetalleFactura $detalleFactura)
    {
        $request->validate([
            'factura_id'=>'required',
            //'producto_id'=>'required',
            'cantidad'=>'required',
            'notas'=>'required'
        ]);

        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        DetalleFactura::create($request->all());
        return redirect()->route('detalle_factura.index')->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DetalleFactura  $detalleFactura
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetalleFactura $detalleFactura)
    {
        $detalleFactura->delete();
        return redirect()->route('detalle_factura.index')->withStatus(__('El registro fue borrado.'));
    }

    public function getDetalles(Request $request){
        if ($request[0] != '4dm1n')
            return [];

        $resp = DB::table('detalle_factura')
            ->select(['id','cantidad','descripcion','lote_caducidad','unidad','codigo','codigo2'])
            ->where('factura_id',$request[1])
            ->get();

        return $resp;
    }
}
