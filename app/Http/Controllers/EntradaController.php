<?php

namespace App\Http\Controllers;

use App\DetalleEntrada;
use App\Entrada;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EntradaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('entradas.index',['entradas'=>Entrada::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('entradas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fecha' => 'required',
            'proveedor_id' => 'required',
            'tipo_entrada_id' => 'required',
            'estado' => 'required',
            'folio_compra' => 'required',
        ]);
        
        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        Entrada::create($request->all());
        return redirect()->route('entrada.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entrada  $entrada
     * @return \Illuminate\Http\Response
     */
    public function show(Entrada $entrada)
    {
        $entrada->fecha = str_replace(' ', 'T', $entrada->fecha);
        $detalleEntradas = DetalleEntrada::where('entrada_id',$entrada->id)->get();

        return view('entradas.show',compact('entrada','detalleEntradas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entrada  $entrada
     * @return \Illuminate\Http\Response
     */
    public function edit(Entrada $entrada)
    {
        $entrada->fecha = str_replace(' ', 'T', $entrada->fecha);

        return view('entradas.edit',compact('entrada'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entrada  $entrada
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entrada $entrada)
    {
        $request->validate([
            'fecha' => 'required',
            'proveedor_id' => 'required',
            'tipo_entrada_id' => 'required',
            'estado' => 'required',
            'folio_compra' => 'required',
        ]);

        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $entrada->update($request->all());
        return redirect()->route('entrada.show',$entrada)->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entrada  $entrada
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entrada $entrada)
    {
        $entrada->detalle_entradas()->delete();
        $entrada->delete();
        return redirect()->route('entrada.index')->withStatus(__('El registro fue borrado.'));
    }
}
