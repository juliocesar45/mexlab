<?php

namespace App\Http\Controllers;

use App\Factura;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('home');
    }

    public function consulta(Request $request)
     {   
         $emitidas = Factura::select('id')->whereDate('created_at',Carbon::today())->get();
         $factura = Factura::select('id')->whereDate('created_at',Carbon::today())->where('estatus_factura_id',11)->get();
         $cancelada = Factura::select('id')->whereDate('updated_at',Carbon::today())->where('estatus_factura_id',12)->get();;
         /* Output a la interfaz */
         $datos = array(
             "facturas_emitidas" => count($emitidas),
             "facturas_surtidas" => count($factura),
             "facturas_canceladas" => count($cancelada),
             "fecha" =>  Carbon::parse(Carbon::now()->setTimezone('America/Mexico_City'))->format('d-m-Y H:i:s')
             
         );
         return json_encode($datos);
     }

     public function consulta_factura(Request $request)
     {
        return DataTables::of(DB::select("SELECT cc.nombre_completo AS cliente, cef.nombre AS estatus_factura, f.identificador, 
                                        DATE_FORMAT(f.created_at, '%d/%m/%Y %H:%i %p') AS fecha_hora, tiempo, f.agente, ca.nombre AS almacen
                                        FROM factura f 
                                        JOIN c_cliente cc ON f.cliente_id=cc.id 
                                        JOIN c_estatus_facturas cef ON f.estatus_factura_id=cef.id 
                                        JOIN c_almacenes ca ON ca.id = f.almacen_id 
                                        WHERE DATE(f.created_at) = DATE(NOW()) 
                                        ORDER BY f.created_at DESC "))
        ->addIndexColumn()
        ->make(true);
     }

     public function consulta_pie(Request $request)
     {
        $facturas = DB::select("SELECT cr.nombre, COUNT(1) AS facturas FROM factura f
                                JOIN c_recolectore cr ON cr.id=f.recolector_id
                                WHERE DATE(f.created_at) = DATE(NOW()) AND f.estatus_factura_id = 11
                                GROUP BY cr.nombre;");

    $nombres = array();
    $cantidad_factura = array();
    foreach ($facturas as $factura) {
        array_push($nombres,$factura->nombre);
        array_push($cantidad_factura, $factura->facturas);
    } 

    $datos = array(
        "recolector" => $nombres,
        "facturas_surtidas" => $cantidad_factura
    );    
    return ($datos);
    }
     
}

