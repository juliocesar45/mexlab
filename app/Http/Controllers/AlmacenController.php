<?php

namespace App\Http\Controllers;

use App\Factura;
use App\Almacen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DateTime;

class AlmacenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $almacenes = Almacen::all();
        return view('almacenes.index',compact(["almacenes","user"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('almacenes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'direccion' => 'required',
            'encargado' => 'required'
        ]);

        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);

        Almacen::create($request->all());
        return redirect()->route('almacen.index')->withStatus(__('Almacen creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function show(Almacen $almacen)
    {
        return view('almacenes.show',compact('almacen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function edit(Almacen $almacen)
    {
        return view('almacenes.edit',compact("almacen"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Almacen $almacen)
    {
        $request->validate([
            'nombre' => 'required',
            'direccion' => 'required',
            'encargado' => 'required'
        ]);


        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $almacen->update($request->all());
        return redirect()->route('almacen.index')->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Almacen $almacen)
    {
        $almacen->racks()->delete();
        $almacen->delete();
        return redirect()->route('almacen.index')->withStatus(__('El registro fue borrado.'));
    }


    public function aux($id){
        // return view('almacenes.aux');
        return Factura::find($id)->delete();
        // return Factura::find(79)->delete();
    }
}
