<?php

namespace App\Http\Controllers;

use App\DetalleEntrada;
use App\Entrada;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DetalleEntradaController extends Controller
{
    /**
     * Display a listing of the resource. jeje
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('detalle_entradas.index',['detalleEntradas'=>DetalleEntrada::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('detalle_entradas.create');
    }

    /**
     * Creación del detalle en la misma pantalla de la entrada
     */
    public function createDetalle(Entrada $entrada)
    {
        return view('detalle_entradas.create',compact('entrada'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'entrada_id' => 'required',
            'cantidad' => 'required',
            'cantidad_mal_estado' => 'required',
        ]);

        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $detalleEntrada = DetalleEntrada::create($request->all());
        return redirect()->route('entrada.show',$detalleEntrada->entrada_id)->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DetalleEntrada  $detalleEntrada
     * @return \Illuminate\Http\Response
     */
    public function show(DetalleEntrada $detalleEntrada)
    {
        return view('detalle_entradas.show',compact('detalleEntrada'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DetalleEntrada  $detalleEntrada
     * @return \Illuminate\Http\Response
     */
    public function edit(DetalleEntrada $detalleEntrada)
    {
        return view('detalle_entradas.edit',compact('detalleEntrada'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DetalleEntrada  $detalleEntrada
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetalleEntrada $detalleEntrada)
    {
        $request->validate([
            'cantidad' => 'required',
            'cantidad_mal_estado' => 'required',
        ]);

        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $detalleEntrada->update($request->all());
        return redirect()->route('entrada.show',$detalleEntrada->entrada_id)->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DetalleEntrada  $detalleEntrada
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetalleEntrada $detalleEntrada)
    {
        $detalleEntrada->delete();
        return redirect()->route('entrada.show',$detalleEntrada->entrada_id)->withStatus(__('El registro fue eliminado.'));
    }
}
