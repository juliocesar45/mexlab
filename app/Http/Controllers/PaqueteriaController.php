<?php

namespace App\Http\Controllers;

use App\Paqueteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaqueteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $paqueterias = Paqueteria::all();
        return view('paqueterias.index',compact(["paqueterias","user"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paqueterias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
        ]);

        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);

        Paqueteria::create($request->all());
        return redirect()->route('paqueteria.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paqueteria  $paqueteria
     * @return \Illuminate\Http\Response
     */
    // public function show(Paqueteria $paqueteria)
    public function show($id)
    {
        $paqueteria = Paqueteria::findOrFail($id);
        
        return view('paqueterias.show',compact('paqueteria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paqueteria  $paqueteria
     * @return \Illuminate\Http\Response
     */
    // public function edit(Paqueteria $paqueteria)
    public function edit($id)
    {
        $paqueteria = Paqueteria::findOrFail($id);
        return view('paqueterias.edit',compact("paqueteria"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paqueteria  $paqueteria
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, Paqueteria $paqueteria)
    public function update(Request $request, $id)
    {
        $paqueteria = Paqueteria::findOrFail($id);
        $request->validate([
            'nombre' => 'required',
        ]);


        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $paqueteria->update($request->all());
        return redirect()->route('paqueteria.index')->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paqueteria  $paqueteria
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Paqueteria $paqueteria)
    public function destroy($id)
    {
        $paqueteria = Paqueteria::findOrFail($id);
        $paqueteria->delete();
        return redirect()->route('paqueteria.index')->withStatus(__('El registro fue borrado.'));
    }
}
