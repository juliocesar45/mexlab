<?php

namespace App\Http\Controllers;

use App\TipoEnvio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TipoEnvioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $tipoEnvio = TipoEnvio::all();
        return view('tipo_envios.index', compact(["tipoEnvio","user"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipo_envios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre'=>'required'
        ]);

        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);

        TipoEnvio::create($request->all());
        return redirect()->route('tipo_envio.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoEnvio  $tipoEnvio
     * @return \Illuminate\Http\Response
     */
    public function show(TipoEnvio $tipoEnvio)
    {
        return view('tipo_envios.show', compact('tipoEnvio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoEnvio  $tipoEnvio
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoEnvio $tipoEnvio)
    {
        return view('tipo_envios.edit', compact('tipoEnvio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoEnvio  $tipoEnvio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoEnvio $tipoEnvio)
    {
        $request->validate([
            'nombre'=>'required'
        ]);

        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $tipoEnvio->update($request->all());
        return redirect()->route('tipo_envio.index')->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoEnvio  $tipoEnvio
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoEnvio $tipoEnvio)
    {
        $tipoEnvio->delete();
        return redirect()->route('tipo_envio.index')->withStatus(__('El registro fue borrado.'));
    }

}
