<?php

namespace App\Http\Controllers;

use App\FamiliaProducto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FamiliaProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $familiaProducto = FamiliaProducto::all();
        return view('familia_productos.index', compact(["familiaProducto","user"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('familia_productos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre'=>'required',
            'proveedor_id'=>'required'
        ]);

        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);

       # dd($request);

        FamiliaProducto::create($request->all());
        return redirect()->route('familia_producto.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FamiliaProducto  $familiaProducto
     * @return \Illuminate\Http\Response
     */
    public function show(FamiliaProducto $familiaProducto)
    {
        return view('familia_productos.show', compact('familiaProducto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FamiliaProducto  $familiaProducto
     * @return \Illuminate\Http\Response
     */
    public function edit(FamiliaProducto $familiaProducto)
    {
        return view('familia_productos.edit', compact('familiaProducto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FamiliaProducto  $familiaProducto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FamiliaProducto $familiaProducto)
    {
        $request->validate([
            'nombre'=>'required'
        ]);

        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $familiaProducto->update($request->all());
        return redirect()->route('familia_producto.index')->withStatus(__('El registro fue actualizado.'));;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FamiliaProducto  $familiaProducto
     * @return \Illuminate\Http\Response
     */
    public function destroy(FamiliaProducto $familiaProducto)
    {
        $familiaProducto->delete();
        return redirect()->route('familia_producto.index')->withStatus(__('El registro fue borrado.'));
    }
}
