<?php

namespace App\Http\Controllers;

use App\Rack;
use App\Almacen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode as FacadesQrCode;
use Illuminate\Support\Facades\Auth;

class RackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($qrCodes = [])
    {
        if(!count($qrCodes))
            return view('racks.index', ['racks' => Rack::all(),'qrCodes'=>$qrCodes]);
        return view('racks.index', ['racks' => Rack::all(),'qrCodes'=>$qrCodes,'alert'=>"QRs generados con exito"])->withStatus('Registro creado correctamente.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('racks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        Rack::create($request->all());
        return redirect()->route('rack.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rack  $rack
     * @return \Illuminate\Http\Response
     */
    public function show(Rack $rack)
    {
        return view('racks.show',compact('rack'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rack  $rack
     * @return \Illuminate\Http\Response
     */
    public function edit(Rack $rack)
    {
        return view('racks.edit',compact('rack'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rack  $rack
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rack $rack)
    {
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $rack->update($request->all());
        return redirect()->route('rack.index')->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rack  $rack
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rack $rack)
    {
        $rack->ubicaciones()->delete();
        $rack->delete();
        return redirect()->route('rack.index')->withStatus(__('El registro fue borrado.'));
    }
    public function getInfoEtiquetas(Request $request)
    {
        return Rack::find($request->id_rack);
    }

    public function getImprimirEtiquetas(Request $request)
    {
        $rack =  Rack::find($request->id_rack);
        $request = $request->all();

        $ubicaciones = DB::select("SELECT cu.id, identificador, x, y, cr.pasillo
                                FROM c_ubicacione cu
                                INNER JOIN c_rack cr
                                ON cu.rack_id = cr.id
                                where cr.id =$rack->id");

        foreach ($ubicaciones as $ubicacion) {
            $QRs[] = ['nombre'=>'Pasillo-'.$ubicacion->pasillo.' fila-'.$ubicacion->identificador.' Anaquel-'.$ubicacion->x.' Estante-'.$ubicacion->y, 'img' => $this->generarQR($ubicacion->id)];
        }

        return $this->index($QRs);
    }


    public function generarQR($idUbicacion)
    {
        $qrcode = base64_encode(FacadesQrCode::format('png')->size(200)->errorCorrection('H')->generate($idUbicacion.''));
        return $qrcode;
    }

}
