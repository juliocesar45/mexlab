<?php

namespace App\Http\Controllers;

use App\Ubicacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class UbicacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ubicaciones.index',['ubicaciones'=>Ubicacion::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ubicaciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->seccion=$request->seccion?true:false;

        $request->validate([
            'rack_id'=>'required',
        ]);



        $request->request->add(['qr_data' => 32]);
        $request->request->add(['seccion' => $request->seccion?true:false]);
        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        Ubicacion::create($request->all());

        return redirect()->route('ubicacion.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ubicacion  $ubicacion
     * @return \Illuminate\Http\Response
     */
    public function show(Ubicacion $ubicacion)
    {
        return view('ubicaciones.show',compact('ubicacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ubicacion  $ubicacion
     * @return \Illuminate\Http\Response
     */
    public function edit(Ubicacion $ubicacion)
    {
        return view('ubicaciones.edit',compact('ubicacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ubicacion  $ubicacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ubicacion $ubicacion)
    {
        $request->validate([
            'rack_id'=>'required',
        ]);

        $request->request->add(['qr_data' => 32]);
        $request->request->add(['seccion' => $request->seccion?true:false]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $ubicacion->update($request->all());
        return redirect()->route('ubicacion.index')->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ubicacion  $ubicacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ubicacion $ubicacion)
    {
        $ubicacion->delete();
        return redirect()->route('ubicacion.index')->withStatus(__('El registro fue borrado.'));
    }


    public function getUbicaciones(Request $request){
        if($request[0]!="4dm1n")
            return [];

        $query = 'SELECT ca.nombre,cr.pasillo,cu.identificador,cu.x,cu.y FROM c_ubicacione cu
                    INNER JOIN c_rack cr
                    ON cr.id = cu.rack_id
                    INNER JOIN c_almacenes ca
                    ON ca.id = cr.c_almacene_id
                    INNER JOIN lote_ubicacion lu
                    ON lu.c_ubicacione_id = cu.id
                    INNER JOIN lote_producto lp
                    ON lu.lote_producto_id = lp.id
                    where lp.lote = \''.$request[1].'\'';

        $ubicaciones = DB::select($query);

        return $ubicaciones;
    }

    public function actualizarLote(Request $request){
        if($request["password"]!="4dm1n")
            return [];
        
        $resp = DB::table('c_ubicacione')
            ->where('id',$request["ubicacion"])
            ->update(['lote_producto_id'=>$request["producto"]]);

        return [true];
    }

    public function getUbicacion(Request $request){
        if($request[0]!="4dm1n")
            return [];

            
        $query = 'SELECT ca.nombre,cr.pasillo,cu.identificador,cu.x,cu.y FROM c_ubicacione cu
                    INNER JOIN c_rack cr
                    ON cr.id = cu.rack_id
                    INNER JOIN c_almacenes ca
                    ON ca.id = cr.c_almacene_id
                    where cu.id = '.$request[1];

        $ubicaciones = DB::select($query);

        return $ubicaciones;
    }
}
