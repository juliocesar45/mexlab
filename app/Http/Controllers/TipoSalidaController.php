<?php

namespace App\Http\Controllers;

use App\TipoSalida;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TipoSalidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $tipoSalida = TipoSalida::all();
        return view('tipo_salidas.index', compact(["tipoSalida","user"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipo_salidas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre'=>'required'
        ]);


        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);

        TipoSalida::create($request->all());
        return redirect()->route('tipo_salida.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoSalida  $tipoSalida
     * @return \Illuminate\Http\Response
     */
    public function show(TipoSalida $tipoSalida)
    {
        return view('tipo_salidas.show', compact('tipoSalida'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoSalida  $tipoSalida
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoSalida $tipoSalida)
    {
        return view('tipo_salidas.edit', compact('tipoSalida'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoSalida  $tipoSalida
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoSalida $tipoSalida)
    {
        $request->validate([
            'nombre'=>'required'
        ]);

        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $tipoSalida->update($request->all());
        return redirect()->route('tipo_salida.index')->withStatus(__('El registro fue actualizado.'));;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoSalida  $tipoSalida
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoSalida $tipoSalida)
    {
        $tipoSalida->delete();
        return redirect()->route('tipo_salida.index')->withStatus(__('El registro fue borrado.'));
    }
}
