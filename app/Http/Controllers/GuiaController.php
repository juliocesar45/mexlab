<?php

namespace App\Http\Controllers;

use App\Guia;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GuiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('guias.index', ['guias'=>Guia::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('guias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'guia'=>'required',
        'c_paqueteria_id'=>'required',
        'cliente_id'=>'required',
        'direccion'=>'required',
        'fecha_recoleccion'=>'required',
        
        ]);

        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);

        Guia::create($request->all());
        return redirect()->route('guia.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Guia  $guia
     * @return \Illuminate\Http\Response
     */
    public function show(Guia $guium)
    {
        $guia = $guium;
        return view('guias.show',compact('guia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Guia  $guia
     * @return \Illuminate\Http\Response
     */
    public function edit($guia,$factura=null)
    // public function edit(Guia $guium,$factura=null)
    {
        $guia = Guia::find($guia);
        $variables = [];

        if($factura)
            $variables['factura'] = $factura;
        
        $variables['guia'] = $guia;

        return view('guias.edit',$variables);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Guia  $guia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Guia $guium)
    {
        $request->validate([
            'guia'=>'required',
            'c_paqueteria_id'=>'required',
            'cliente_id'=>'required',
            'direccion'=>'required',
            'fecha_recoleccion'=>'required',
            
        ]);

    
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $guium->update($request->all());


        if (isset($request->factura))
            return redirect()->route('factura.show',$request->factura)->withStatus(__('Se editó exitosamente la guía.'));


        return redirect()->route('guia.index')->withStatus(__('Registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Guia  $guia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Guia $guium)
    {
        $guium->delete();
        return redirect()->route('guia.index')->withStatus(__('Registro eliminado correctamente.'));
    }

    public function getInfoEtiquetas(Request $request)
    {
        return Guia::find($request->id_guia);
    }

    public function getImprimirEtiquetas(Request $request)
    {
        // return $request->cantidad;
        $guia =  Guia::find($request->id_guia);
        
        $cantidad = $request->cantidad;
        
        
        // return view('guias.etiquetasg',compact('cantidad','guia'));
        
        $pdf = PDF::loadView('guias.etiquetasg', compact('cantidad','guia'));
        
        // download PDF file with download method
        return $pdf->download('guia.pdf');

        // // download PDF file with download method
        // return $pdf->download('etiquetas.pdf');
    }
}
