<?php

namespace App\Http\Controllers;

use App\ListHelper;
use App\LoteProducto;
use Illuminate\Support\Facades\DB;
use App\Producto;
use App\LoteUbicacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoteProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('lote_productos.index',['loteProductos'=>LoteProducto::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lote_productos.create');
    }

    /**
     * Creación del lote de la pantalla en la misma pantalla de la entrada
     */
    public function createLote(Producto $producto)
    {
        return view('lote_productos.create',compact('producto'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'producto_id' =>'required',
            'lote' => 'required',
            'caducidad' =>'required',
            'cantidad' =>'required|numeric',
            'codigo' => 'required|numeric',
            'estatus' =>'required',
        ]);
        $qr_data = $request->codigo;
        $request->request->add(['qr_data' => $qr_data]);
        $request->request->add(['comodin' => $request->comodin?true:false]);
        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $loteProducto = LoteProducto::create($request->all());
        return redirect()->route('producto.show',$loteProducto->producto_id)->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoteProducto  $loteProducto
     * @return \Illuminate\Http\Response
     */
    public function show(LoteProducto $loteProducto)
    {
        $variables=[
            'loteProducto'=>$loteProducto,
            'ubicaciones'=>$this->ubicaciones($loteProducto->id),
        ];

        return view('lote_productos.show',$variables);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoteProducto  $loteProducto
     * @return \Illuminate\Http\Response
     */
    public function edit(LoteProducto $loteProducto)
    {
        $variables=[
            'loteProducto'=>$loteProducto,
            'ubicaciones'=>$this->ubicaciones($loteProducto->id),
        ];
        return view('lote_productos.edit',$variables);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoteProducto  $loteProducto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoteProducto $loteProducto)
    {
        $request->validate([
            'caducidad' =>'required',
            'cantidad' =>'required|numeric',
            'estatus' =>'required',
            'producto_id' =>'required',
            'codigo' => 'required|numeric',
        ]);

        $request->request->add(['comodin' => $request->comodin?true:false]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $loteProducto->update($request->all());
        return redirect()->route('producto.show',$loteProducto->producto_id)->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoteProducto  $loteProducto
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoteProducto $loteProducto)
    {
        $loteProducto->delete();
        return redirect()->route('producto.show',$loteProducto->producto_id)->withStatus(__('El registro fue eliminado.'));
    }

    public function aux(){
        return ListHelper::getListaUbicaciones();
    }

    private function ubicaciones($id){
        $ubicaciones = DB::table('lote_ubicacion')
            ->join('lote_producto', 'lote_producto.id', '=', 'lote_ubicacion.lote_producto_id')
            ->join('c_ubicacione', 'c_ubicacione.id', '=', 'lote_ubicacion.c_ubicacione_id')
            ->select('lote_ubicacion.id','lote_ubicacion.cantidad', 'c_ubicacione.id AS c_ubicacione_id')
            ->where('lote_ubicacion.lote_producto_id',$id)
            ->get();

        return $ubicaciones;
    }

    public function lotesByUbicacion(Request $request){
        $lotes = DB::table('lote_ubicacion')
            ->join('lote_producto', 'lote_producto.id', '=', 'lote_ubicacion.lote_producto_id')
            ->join('c_ubicacione', 'c_ubicacione.id', '=', 'lote_ubicacion.c_ubicacione_id')
            ->select('lote_ubicacion.id','lote_ubicacion.cantidad', 'lote_producto.nombre')
            ->where('c_ubicacione.id',$request->id)
            ->get();

        return $lotes;
    }


    public function modificarLotes(Request $request){
        $lotesJson = json_decode($request->lotes);

        $length = count($lotesJson);


        for($i=0; $i<$length; $i++){
            $json = json_decode($lotesJson[$i]);
            $lote = LoteUbicacion::find($json->id);


            if($json->isDeleted==1){
                $lote->delete();
            }
            $lote->update(['cantidad'=>$json->cantidad]);
        }

        return true;
    }
}
