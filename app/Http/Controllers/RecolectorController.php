<?php

namespace App\Http\Controllers;

use App\Recolector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RecolectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('recolectores.index',['recolectores'=>Recolector::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recolectores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'estatus' => 'required',    
            'pass' => 'required',    
        ]);

        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);

        Recolector::create($request->all());

        return redirect()->route('recolector.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recolector  $recolector
     * @return \Illuminate\Http\Response
     */
    public function show(Recolector $recolector)
    {
        return view('recolectores.show',compact('recolector'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recolector  $recolector
     * @return \Illuminate\Http\Response
     */
    public function edit(Recolector $recolector)
    {
        return view('recolectores.edit',compact('recolector'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recolector  $recolector
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recolector $recolector)
    {
        $request->validate([
            'nombre' => 'required',
            'estatus' => 'required',  
            'pass' => 'required',    
        ]);


        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $recolector->update($request->all());
        
        return redirect()->route('recolector.index')->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recolector  $recolector
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recolector $recolector)
    {
        $recolector->delete();
        return redirect()->route('recolector.index')->withStatus(__('El registro fue borrado.'));
    }

    public function getRecolectores(Request $request){
        if($request->password!="4dm1n")
            return [];
        return Recolector::all(['nombre']);
    }

    public function setToken(Request $request){
        if($request->password!="4dm1n")
            return [];

        $query = 'Select id from c_recolectore WHERE nombre = \''.$request->recolector.'\' and pass = \''.$request->pass.'\'';
        $recolector = DB::select($query);
        
        if(count($recolector)){
            $query = 'UPDATE c_recolectore SET token = \''.$request->token.'\' WHERE id = \''.$recolector[0]->id.'\'';
            DB::update($query);
        }
        
        return $recolector;

    }
}