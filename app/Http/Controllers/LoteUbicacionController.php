<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LoteUbicacion;

class LoteUbicacionController extends Controller
{
    public function asignarUbicacionToProduct(Request $request){
        if($request["password"]!="4dm1n")
            return [];

        $resp = LoteUbicacion::create([
            'c_ubicacione_id' => $request["ubicacion"],
            'lote_producto_id' => $request["producto"],
            'cantidad' => $request["cantidad"]
        ]);

        return [true];
    }
}
