<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proveedor;
use App\Almacen;
use App\Material;
use App\Rack;
use App\TipoCaja;
use App\TipoEntrada;
use App\Entrada;
use App\DetalleEntrada;
use App\FamiliaProducto;
use App\ListHelper;
use Illuminate\Support\Facades\DB;

class VisitaController extends Controller
{
    public function visita(){
        return view('visitas.index');
    }

    public function entradas($opc,$tipoEntrada){
        $query = "SELECT 
                    ent.id clave, ent.folio_compra valor
                    FROM entrada ent
                    INNER JOIN c_tipo_entrada cte
                    ON ent.tipo_entrada_id = cte.id
                    WHERE cte.id = $tipoEntrada";

        return DB::select($query);
    }

    public function proveedores($opc,$entrada){
        switch($opc){
            case 1:
                $query = "SELECT 
                    distinct(cp.id) clave, cp.nombre valor
                    FROM c_proveedor cp
                    INNER JOIN entrada ent
                    ON cp.id = ent.proveedor_id
                    INNER JOIN c_tipo_entrada cte
                    ON ent.tipo_entrada_id = cte.id
                    where cte.id = $entrada";
                break;
            case 2:
                $query = "SELECT 
                    distinct(cp.id) clave, cp.nombre valor
                    FROM c_proveedor cp
                    INNER JOIN entrada ent
                    ON cp.id = ent.proveedor_id
                    where ent.id = $entrada";
                break;
        }

        return DB::select($query);
    }

    public function familiaProductos($opc,$proveedor){
        switch($opc){
            case 1:
                $query = "SELECT 
                    distinct(cfp.id) clave, cfp.nombre valor
                    FROM c_familia_producto cfp
                    INNER JOIN c_proveedor cp
                    ON cfp.proveedor_id = cp.id
                    INNER JOIN entrada ent
                    ON cp.id = ent.proveedor_id
                    INNER JOIN c_tipo_entrada cte
                    ON ent.tipo_entrada_id = cte.id
                    WHERE cte.id = $proveedor";
                break;
            case 2:
                $query = "SELECT 
                    distinct(cfp.id) clave, cfp.nombre valor
                    FROM c_familia_producto cfp
                    INNER JOIN c_proveedor cp
                    ON cfp.proveedor_id = cp.id
                    INNER JOIN entrada ent
                    ON cp.id = ent.proveedor_id
                    WHERE ent.id = $proveedor";
                break;
            case 3:
                $query = "SELECT 
                    distinct(cfp.id) clave, cfp.nombre valor
                    FROM c_familia_producto cfp
                    INNER JOIN c_proveedor cp
                    ON cfp.proveedor_id = cp.id
                    WHERE cp.id = $proveedor";
                break;
        }

        

        return DB::select($query);
    }

    public function racks($opc,$almacen){
        $query = "SELECT distinct(cr.id) clave, cr.pasillo valor
                FROM c_almacenes ca
                INNER JOIN c_rack cr
                ON cr.almacen_id = ca.id
                WHERE ca.id = $almacen";
        return DB::select($query);
    }

    public function ubicaciones($opc,$dato){
        switch($opc){
            case 1:
                $query = "SELECT distinct(cu.id) clave, cu.identificador valor
                FROM c_almacenes ca
                INNER JOIN c_rack cr
                ON cr.almacen_id = ca.id
                INNER JOIN c_ubicacione cu
                ON cu.rack_id = cr.id
                WHERE ca.id = $dato";
                break;
            case 2:
                $query = "SELECT distinct(cu.id) clave, cu.identificador valor
                FROM c_rack cr
                INNER JOIN c_ubicacione cu
                ON cu.rack_id = cr.id
                WHERE cr.id = $dato";
        }
        
        return DB::select($query);
    }

    public function loteProductos($opc,$dato){
        switch($opc){
            case 1:
                $query = "SELECT distinct(lp.id) clave, lp.lote valor
                FROM c_almacenes ca
                INNER JOIN c_rack cr
                ON cr.almacen_id = ca.id
                INNER JOIN c_ubicacione cu
                ON cu.rack_id = cr.id
                INNER JOIN lote_producto lp
                ON lp.id = cu.lote_id
                WHERE ca.id = $dato";
                break;
            case 2:
                $query = "SELECT distinct(lp.id) clave, lp.lote valor
                FROM c_rack cr
                INNER JOIN c_ubicacione cu
                ON cu.rack_id = cr.id
                INNER JOIN lote_producto lp
                ON lp.id = cu.lote_id
                WHERE cr.id = $dato";
                break;
            case 3:
                $query = "SELECT distinct(lp.id) clave, lp.lote valor
                FROM c_ubicacione cu
                INNER JOIN lote_producto lp
                ON lp.id = cu.lote_id
                WHERE cu.id = $dato";
                break;
        }
        
        return DB::select($query);
    }

    public function productos(Request $request){
        $query = $this->getQuery();
        
        if(isset($request->tipo_caja)){
            $query .= "WHERE tipo_caja_id = $request->tipo_caja ";
        } 

        if(isset($request->familia_producto)){
            $query .= "WHERE familia_producto_id = $request->familia_producto ";
        } else 
        if(isset($request->proveedor)){
            $query .= "WHERE cp.id = $request->proveedor ";
        } else
        if(isset($request->entrada)){
            $query .= "WHERE ent.id = $request->entrada ";
        } else
        if(isset($request->tipo_entrada)){
            $query .= "WHERE cte.id = $request->tipo_entrada ";
        } 

        if(isset($request->lote)){
            $query .= "WHERE lp.id = $request->lote ";
        }else
        if(isset($request->ubicacion)){
            $query .= "WHERE cu.id = $request->ubicacion ";
        }else
        if(isset($request->rack)){
            $query .= "WHERE cr.id = $request->rack ";
        }else
        if(isset($request->almacen)){
            $query .= "WHERE ca.id = $request->almacen ";
        }

        

        $query = str_replace("WHERE", "AND", $query);
        $query = str_replace("almacen_id AND", "almacen_id WHERE", $query);
        $query .= "GROUP BY codigo";

        return DB::select($query);
    }

    public function getQuery(){
        return "SELECT 
                pro.codigo, 
                ctp.nombre tipo_caja,
                cfp.nombre familia,
                cp.nombre proveedor,
                group_concat(distinct(ent.id) separator ', ') entradas,
                group_concat(distinct(cte.nombre) separator ', ') tipo_entradas,
                group_concat(distinct(lp.lote) separator ', ') lotes,
                group_concat(distinct(cu.identificador) separator ', ') ubicaciones,
                group_concat(distinct(cr.pasillo) separator ', ') racks, 
                group_concat(distinct(ca.nombre) separator ', ') almacenes
                FROM producto pro
                INNER JOIN c_tipo_caja ctp
                ON ctp.id = pro.tipo_caja_id
                INNER JOIN c_familia_producto cfp
                ON cfp.id = pro.familia_producto_id
                INNER JOIN c_proveedor cp
                ON cp.id = cfp.proveedor_id
                INNER JOIN entrada ent
                ON ent.proveedor_id = cp.id
                INNER JOIN c_tipo_entrada cte
                ON cte.id = ent.tipo_entrada_id
                INNER JOIN lote_producto lp
                ON pro.id = lp.producto_id
                INNER JOIN c_ubicacione cu
                ON cu.lote_producto_id = lp.id
                INNER JOIN c_rack cr
                ON cr.id = cu.rack_id
                INNER JOIN c_almacenes ca
                ON ca.id = cr.almacen_id ";
    }
}
