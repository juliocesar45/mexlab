<?php

namespace App\Http\Controllers;

use App\Mensajero;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MensajeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $mensajeros = Mensajero::all();
        return view('mensajeros.index',compact(["mensajeros","user"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mensajeros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'telefono' => 'max:14',
            'user_id'=> 'required'
        ]);

        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);

        Mensajero::create($request->all());
        return redirect()->route('mensajero.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mensajero  $mensajero
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mensajero = Mensajero::findOrFail($id);
        return view('mensajeros.show',compact('mensajero'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mensajero  $mensajero
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mensajero = Mensajero::findOrFail($id);
        return view('mensajeros.edit',compact("mensajero"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mensajero  $mensajero
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mensajero = Mensajero::findOrFail($id);

        $request->validate([
            'nombre' => 'required',
            'telefono' => 'max:14',
            'user_id' => 'required',
        ]);

        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $mensajero->update($request->all());
        return redirect()->route('mensajero.index')->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mensajero  $mensajero
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mensajero = Mensajero::findOrFail($id);
        $mensajero->delete();
        return redirect()->route('mensajero.index')->withStatus(__('El registro fue borrado.'));
    }
}
