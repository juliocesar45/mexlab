<?php

namespace App\Http\Controllers;

use App\Factura;
use App\Mensajero;
use App\Salida;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SalidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('salidas.index', ['salidas' => Salida::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('salidas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fecha' => 'required',
            'tipo_salida_id' => 'required',
            'cliente_id' => 'required',
            'factura_id' => 'required',
            'guia_id' => 'required',
            'ruta_embarque_id'
        ]);

        $request->request->add(['resultado_envio' => 'SALIENDO DEL ALMACEN']);
        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);

        Salida::create($request->all());
        return redirect()->route('salida.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Salida  $salida
     * @return \Illuminate\Http\Response
     */
    public function show(Salida $salida)
    {
        return view('salidas.show', compact('salida'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Salida  $salida
     * @return \Illuminate\Http\Response
     */
    public function edit(Salida $salida)
    {
        return view('salidas.edit', compact('salida'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Salida  $salida
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Salida $salida)
    {
        $request->validate([
            'fecha' => 'required',
            'tipo_salida_id' => 'required',
            'cliente_id' => 'required',
            'factura_id' => 'required',
            'guia_id' => 'required',
            'ruta_embarque_id'
        ]);

        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $salida->update($request->all());
        return redirect()->route('salida.index')->withStatus(__('El registro fue actualizado.'));;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Salida  $salida
     * @return \Illuminate\Http\Response
     */
    public function destroy(Salida $salida)
    {
        $salida->delete();
        return redirect()->route('salida.index')->withStatus(__('El registro fue borrado.'));
    }

    public function mensajero(Request $request)
    {
        $mensajero = Mensajero::where('user_id', Auth::user()->id)->limit(1)->get();
        if(count($mensajero) > 0){
            return view('salidas.upload', ['salidas' => Salida::where('mensajero_id', $mensajero[0]->id)->get()]);
        }else{
            return view('salidas.upload', ['salidas' => Salida::where('mensajero_id', 1)->get()]);
        }
    }

    public function getEstatusMensajero(Request $request)
    {
        $salida = Salida::find($request->id_salida);
        $factura = Factura::find($salida->factura_id);
        $salida->update(['estatus' => $request->estatus, 'notas' => $request->nota]);

        if ($request->estatus == 'ENTREGADO') {
            $factura->update(['estatus_factura_id' => 17]);
        } elseif ($request->estatus == 'NO ENTREGADO') {
            $factura->update(['estatus_factura_id' => 19]);
        } else {
            $factura->update(['estatus_factura_id' => 12]);
        }

        return view('salidas.upload', ['salidas' => Salida::all()]);
    }

    public function amensajero(Request $request)
    {
        return view('salidas.asignacion', ['salidas' => Salida::join('factura', 'factura_id', '=', 'factura.id')->where('estatus_factura_id', 14)->get()]);
    }

    public function getAsignacionMensajero(Request $request)
    {
        $salida = Salida::find($request->id_salida);

        return view('salidas.asignacion', ['salidas' => Salida::all()]);
    }
}
