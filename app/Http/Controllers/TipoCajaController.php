<?php

namespace App\Http\Controllers;

use App\TipoCaja;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TipoCajaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $tipocajas = TipoCaja::all();
        return view('tipocajas.index',compact(["tipocajas","user"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipocajas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
        ]);

        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);

        TipoCaja::create($request->all());
        return redirect()->route('tipocaja.index')->withStatus(__('Registro creado correctamente'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoCaja  $tipoCaja
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tipoCaja = TipoCaja::findOrFail($id);
        return view('tipocajas.show',compact('tipoCaja'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoCaja  $tipoCaja
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoCaja = TipoCaja::findOrFail($id);
        return view('tipocajas.edit',compact("tipoCaja"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoCaja  $tipoCaja
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipoCaja = TipoCaja::findOrFail($id);
        $request->validate([
            'nombre' => 'required',
        ]);


        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $tipoCaja->update($request->all());
        return redirect()->route('tipocaja.index')->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoCaja  $tipoCaja
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipoCaja = TipoCaja::findOrFail($id);
        $tipoCaja->delete();
        return redirect()->route('tipocaja.index')->withStatus(__('El registro fue borrado.'));
    }
}
