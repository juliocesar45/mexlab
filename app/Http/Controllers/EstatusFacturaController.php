<?php

namespace App\Http\Controllers;

use App\EstatusFactura;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ListHelper;


class EstatusFacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $estatusfacturas = EstatusFactura::all();
        return view('estatus_facturas.index', compact(["estatusfacturas","user"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estatus_facturas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre'=>'required'
        ]);


        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        
        EstatusFactura::create($request->all());
        return redirect()->route('estatus_factura.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EstatusFactura  $estatusFactura
     * @return \Illuminate\Http\Response
     */
    public function show(EstatusFactura $estatusFactura)
    {
        return view('estatus_facturas.show', compact('estatusFactura'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EstatusFactura  $estatusFactura
     * @return \Illuminate\Http\Response
     */
    public function edit(EstatusFactura $estatusFactura)
    {
        return view('estatus_facturas.edit', compact('estatusFactura'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EstatusFactura  $estatusFactura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstatusFactura $estatusFactura)
    {
        $request->validate([
            'nombre'=>'required'
        ]);

        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $estatusFactura->update($request->all());
        return redirect()->route('estatus_factura.index')->withStatus(__('El registro fue actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstatusFactura  $estatusFactura
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstatusFactura $estatusFactura)
    {
        $estatusFactura->delete();
        return redirect()->route('estatus_factura.index')->withStatus(__('El registro fue borrado.'));
    }

    public function getAllEstatus(){
        return EstatusFactura::all(['id','nombre']);
    }
}
