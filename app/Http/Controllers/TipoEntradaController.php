<?php

namespace App\Http\Controllers;

use App\TipoEntrada;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TipoEntradaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $tipoEntradas = TipoEntrada::all();
        return view('tipo_entradas.index', compact(["tipoEntradas","user"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipo_entradas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre'=>'required'
        ]);

        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);

        TipoEntrada::create($request->all());
        return redirect()->route('tipo_entrada.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoEntrada  $tipoEntrada
     * @return \Illuminate\Http\Response
     */
    public function show(TipoEntrada $tipoEntrada)
    {
        return view('tipo_entradas.show', compact('tipoEntrada'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoEntrada  $tipoEntrada
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoEntrada $tipoEntrada)
    {
        return view('tipo_entradas.edit', compact('tipoEntrada'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoEntrada  $tipoEntrada
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoEntrada $tipoEntrada)
    {
        $request->validate([
            'nombre'=>'required'
        ]);

        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $tipoEntrada->update($request->all());
        return redirect()->route('tipo_entrada.index')->withStatus(__('El registro fue actualizado.'));;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoEntrada  $tipoEntrada
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoEntrada $tipoEntrada)
    {
        $tipoEntrada->entradas()->delete();
        $tipoEntrada->delete();
        return redirect()->route('tipo_entrada.index')->withStatus(__('El registro fue borrado.'));
    }
}
