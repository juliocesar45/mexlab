<?php

namespace App\Http\Controllers;

use App\RutaEmbarque;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RutaEmbarqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ruta_embarques.index',['rutaEmbarques'=>RutaEmbarque::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ruta_embarques.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'paqueteria_id'=>'required',
            'guia'=>'required',
            'entregar_a'=>'required',
            'direccion_factura'=>'required',
            'direccion'=>'required',
            'conductor_id'=>'required',
            'mensajero_id'=>'required',
            'tipo_envio_id'=>'required',
            'ruta'=>'required',
            'tiempo_estimado'=>'required',
            'fecha_entrega_estimada'=>'required',
            'fecha_entrega'=>'required',
            'estatus'=>'required',
        ]);
        // dd($request->all());
        //$request->request->add(['resultado_envio' => 'SALIENDO DEL ALMACEN']);
        $request->request->add(['capturado_por' => Auth::user()->name]);
        $request->request->add(['actualizado_por' => Auth::user()->name]);
        

        RutaEmbarque::create($request->all());
        return redirect()->route('ruta_embarque.index')->withStatus(__('Registro creado correctamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RutaEmbarque  $rutaEmbarque
     * @return \Illuminate\Http\Response
     */
    public function show(RutaEmbarque $rutaEmbarque)
    {
        return view('ruta_embarques.show', compact('rutaEmbarque'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RutaEmbarque  $rutaEmbarque
     * @return \Illuminate\Http\Response
     */
    public function edit(RutaEmbarque $rutaEmbarque)
    {
        return view('ruta_embarques.edit', compact('rutaEmbarque'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RutaEmbarque  $rutaEmbarque
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RutaEmbarque $rutaEmbarque)
    {
        $request->validate([
            'paqueteria_id'=>'required',
            'guia'=>'required',
            'entregar_a'=>'required',
            'direccion_factura'=>'required',
            'direccion'=>'required',
            'conductor_id'=>'required',
            'mensajero_id'=>'required',
            'tipo_envio_id'=>'required',
            'ruta'=>'required',
            'tiempo_estimado'=>'required',
            'fecha_entrega_estimada'=>'required',
            'fecha_entrega'=>'required',
            'estatus'=>'required',
        ]);

        $request->request->add(['actualizado_por' => Auth::user()->name]);
        $rutaEmbarque->update($request->all());
        return redirect()->route('ruta_embarque.index')->withStatus(__('El registro fue actualizado.'));;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RutaEmbarque  $rutaEmbarque
     * @return \Illuminate\Http\Response
     */
    public function destroy(RutaEmbarque $rutaEmbarque)
    {
        $rutaEmbarque->delete();
        return redirect()->route('ruta_embarque.index')->withStatus(__('El registro fue borrado.'));
    }
}
