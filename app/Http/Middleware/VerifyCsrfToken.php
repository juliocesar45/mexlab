<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/ver-detalles/{factura}',
        '/arreglo',
        '/setToken',
        '/por-surtir',
        '/todas',
        '/get-factura',
        '/ver-detalles',
        '/actualizar-factura',
        '/recolectores',
        '/ver-ubicaciones',
        '/ver-productos',
        '/subir-fotos',
        '/actualizar-lote',
        '/get-estatus-factura',
        '/actualizar-estatus-factura',
        '/get-producto',
        '/get-almacenes',
        '/get-via-embarques',
        '/get-ubicacion',
        '/asignar-ubicacion',
        '/lotesByUbicacion',
        '/modificar-lotes'
    ];
}
