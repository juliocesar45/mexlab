<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstatusFactura extends Model
{
    use HasFactory;
    protected $table = 'c_estatus_facturas';
    protected $fillable = [
        'nombre',
        'capturado_por',
        'actualizado_por',
    ];
}
