<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guia extends Model
{
    use HasFactory;
    protected $table = 'guia';
    protected $fillable = [
        'guia',
        'c_paqueteria_id',
        'cliente_id',
        'direccion',
        'fecha_recoleccion',
        'salida_id',
        'capturado_por',
        'actualizado_por',
        'factura_id',
        'contacto',
        'receptor',
        'tipo_envio_id'
    ];

    public function c_paqueteria(){
        return $this->belongsTo('App\Paqueteria');
    }

    public function cliente(){
        return $this->belongsTo('App\Cliente');
    }

    public function salida(){
        return $this->belongsTo('App\Salida');
    }

    public function tipo_envio(){
        return $this->belongsTo('App\TipoEnvio');
    }
}
