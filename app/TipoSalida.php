<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoSalida extends Model
{
    use HasFactory;
    protected $table = 'c_tipo_salida';
    protected $fillable = [
        'nombre',
        'capturado_por',
        'actualizado_por',
    ];
}
