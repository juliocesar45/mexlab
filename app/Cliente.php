<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;
    protected $table = 'c_cliente';
    protected $fillable = [
        'nombres',
        'ap_paterno',
        'ap_materno',
        'nombre_completo',
        'rfc',
        'domicilio_fiscal',
        'domicilio',
        'colonia',
        'codigo_postal',
        'ciudad',
        'estado',
        'pais',
        'telefonos',
        'capturado_por',
        'actualizado_por',
    ];
}
