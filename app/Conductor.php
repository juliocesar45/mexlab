<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conductor extends Model
{
    use HasFactory;
    protected $table = 'c_conductor';
    protected $fillable = [
        'nombre',
        'capturado_por',
        'actualizado_por',
        'telefono',
    ];
}
