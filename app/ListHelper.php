<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\TipoEntrada;
use App\Entrada;
use App\DetalleEntrada;
use App\Proveedor;
use App\FamiliaProducto;
use App\TipoCaja;
use App\Rack;
use App\Almacen;
use App\Ubicacion;
use App\LoteProducto;
use App\Conductor;
use App\Mensajero;
use App\TipoEnvio;
use App\EstatusFactura;
use App\Factura;

class ListHelper extends Model
{
    use HasFactory;


    static function getRacks()
    {
        return Rack::orderby('pasillo')->pluck('pasillo', 'id');
    }


    static function getAlmacenes()
    {
        return Almacen::orderby('nombre')->pluck('nombre', 'id');
    }


    static function getAllAlmacenes()
    {
        return DB::select('SELECT id,nombre FROM c_almacenes  ORDER BY nombre');
        // return Almacen::orderby('nombre')->pluck('nombre', 'id');
    }


    static function getUbicaciones()
    {
        return Ubicacion::orderby('identificador')->pluck('identificador', 'id');
    }

    static function getLoteProductos()
    {
        return LoteProducto::orderby('lote')->pluck('lote', 'id');
    }

    static function getTipoCajas()
    {
        return TipoCaja::orderby('nombre')->pluck('nombre', 'id');
    }

    static function getTipoEntradas()
    {
        return TipoEntrada::orderby('nombre')->pluck('nombre', 'id');
    }

    static function getEntradas()
    {
        return Entrada::orderby('id')->pluck('id', 'id');
    }

    static function getDetalleEntradas()
    {
        return DetalleEntrada::orderby('nombre')->pluck('nombre', 'id');
    }

    static function getProveedores()
    {
        return Proveedor::orderby('nombre')->pluck('nombre', 'id');
    }

    static function getFamiliaProductos()
    {
        return FamiliaProducto::orderby('nombre')->pluck('nombre', 'id');
    }

    static function getEstatus()
    {
        return [
            'ACTIVO' => 'ACTIVO',
            'INACTIVO' => 'INACTIVO',
            'CANCELADO' => 'CANCELADO',
        ];
    }

    static function getRecolector()
    {
        return Recolector::orderby('nombre')->pluck('nombre', 'id');
    }

    static function getSalidas()
    {
        return Salida::orderby('id', 'DESC')->pluck('id', 'id');
    }

    static function getProductos()
    {
        return Producto::orderby('id', 'DESC')->pluck('nombre', 'id');
    }

    static function getClientes()
    {
        return Cliente::orderby('nombre_completo')->pluck('nombre_completo','id');
    }

    static function getPaqueteria()
    {
        return Paqueteria::orderby('nombre')->pluck('nombre','id');
    }

    static function getAllPaqueterias()
    {
        return DB::select('SELECT id,nombre FROM c_paqueteria ORDER BY nombre');
    }

    static function getConductor()
    {
        return Conductor::orderby('nombre')->pluck('nombre','id');
    }

    static function getMensajero()
    {
        return Mensajero::orderby('nombre')->pluck('nombre','id');
    }

    static function getTipoEnvio()
    {
        return TipoEnvio::orderby('nombre')->distinct('nombre')->pluck('nombre','id');
    }

    static function getEstatusRuta()
    {
        return [
            'SALIO DEL ALMACÉN' => 'SALIO DEL ALMACÉN',
            'LLEGO A SU DESTINO' => 'LLEGO A SU DESTINO',
        ];
    }

    static function getGuia()
    {
        return Guia::orderby('nombre')->pluck('nombre','id');
    }

    static function getRutaEmbarque()
    {
        return RutaEmbarque::distinct('nombre')->orderby('nombre')->pluck('nombre','id');
    }

    static function getEstatusFactura(){
        return EstatusFactura::orderby('nombre')->pluck('nombre','id');
    }
    
    static function getFacturas(){
        return Factura::get();
    }

    static function getListaUbicaciones(){
        return DB::table('ubicaciones')
            ->select('ubicaciones.*')
            ->orderby('nombre')
            ->pluck('nombre','id');
        // return EstatusFactura::orderby('nombre')->pluck('nombre','id');
    }
}
