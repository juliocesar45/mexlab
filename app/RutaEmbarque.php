<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RutaEmbarque extends Model
{
    use HasFactory;
    protected $table = 'ruta_embarque';
    protected $fillable = [
        'paqueteria_id',
        'guia',
        'entregar_a',
        'direccion_factura',
        'direccion',
        'conductor_id',
        'mensajero_id',
        'tipo_envio_id',
        'ruta',
        'tiempo_estimado',
        'fecha_entrega_estimada',
        'fecha_entrega',
        'estatus',
        'capturado_por',
        'actualizado_por',
    ];

    public function paqueteria(){
        return $this->belongsTo('App\Paqueteria');
    }

    public function conductor(){
        return $this->belongsTo('App\Conductor');
    }

    public function mensajero(){
        return $this->belongsTo('App\Mensajero');
    }

    public function tipo_envio(){
        return $this->belongsTo('App\TipoEnvio');
    }
}
