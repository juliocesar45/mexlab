<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paqueteria extends Model
{
    use HasFactory;
    protected $table = 'c_paqueteria';
    protected $fillable = [
        'nombre',
        'capturado_por',
        'actualizado_por',
    ];
}
