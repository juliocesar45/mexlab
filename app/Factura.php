<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    use HasFactory;
    protected $table = 'factura';

    protected $fillable = [
        'cliente_id',
        'cantidad',
        'estatus_factura_id',
        'identificador',
        'fecha_embarque',
        'salida_id',
        'recolector_id',
        'nota',
        'capturado_por',
        'actualizado_por',
        'agente',
        'estatus',
        'tiempo',
        'foto_producto',
        'foto_empaquetado',
        'almacen_id',
        'fecha_emision',
        'observaciones_recolector'
    ];

    

    public function cliente(){
        return $this->belongsTo('App\Cliente');
    }
    
    public function estatus_factura(){
        return $this->belongsTo('App\EstatusFactura');
    }

    public function salida(){
        return $this->belongsTo('App\Salida');
    }

    public function recolector(){
        return $this->belongsTo('App\Recolector');
    }


    public function almacen(){
        return $this->belongsTo('App\Almacen');
    }


    public function guia(){
        return $this->hasOne('App\Guia');
    }
}
