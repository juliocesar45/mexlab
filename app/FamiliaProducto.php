<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamiliaProducto extends Model
{
    use HasFactory;
    protected $table = 'c_familia_producto';
    protected $fillable = [
        'nombre',
        'proveedor_id',
        'capturado_por',
        'actualizado_por',
    ];

    public function proveedor(){
        return $this->belongsTo('App\Proveedor');
    }
}
