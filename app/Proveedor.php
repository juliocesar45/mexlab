<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    use HasFactory;
    protected $table = 'c_proveedor';
    protected $fillable = [
        'nombre',
        'capturado_por',
        'actualizado_por',
    ];

    public function entradas()
    {
        return $this->hasMany('App\Entrada');
    }
}
