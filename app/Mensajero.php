<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mensajero extends Model
{
    use HasFactory;
    protected $table = 'c_mensajero';
    protected $fillable = [
        'nombre',
        'telefono',
        'user_id',
        'capturado_por',
        'actualizado_por',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
