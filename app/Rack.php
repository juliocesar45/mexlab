<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rack extends Model
{
    use HasFactory;
    protected $table = 'c_rack';
    protected $fillable = [
        'c_almacene_id',
        'pasillo',
        'fila1',
        'alto_fila1',
        'ancho_fila1',
        'fila2',
        'alto_fila2',
        'ancho_fila2',
        'capturado_por',
        'actualizado_por',
    ];

    public function almacen(){
        return $this->belongsTo('App\Almacen','c_almacene_id');
    }
    
    public function ubicaciones()
    {
        return $this->hasMany('App\Ubicacion');
    }
}
