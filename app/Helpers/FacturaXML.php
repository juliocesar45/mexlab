<?php
namespace App\Helpers;

use App\Helpers\XMLString;
use App\Helpers\ConceptoXML;

class FacturaXML extends XMLString
{
    /**
     * Cantidad de conceptos que tiene la factura
     * 
     * @return int
     */
    public function getCountConceptos(){
        //`<cfdi:Concepto ` cuantas veces se repite ese substring en la factura
        return substr_count($this->xml_string,'<cfdi:Concepto ');
    }

    /**
     * 
     */
    public function getConceptoAt($num_concepto){
        $start = 0;

        if($num_concepto>=$this->getCountConceptos()) return false;

        $start=strpos($this->xml_string,'<cfdi:Concepto ',$start);

        for($i=0;$i<$num_concepto;$i++)
            $start=strpos($this->xml_string,'<cfdi:Concepto ',$start+1);

        if($start===false) return false;
        
        $end = strpos($this->xml_string,'">',$start)+2;

        return new ConceptoXML(substr($this->xml_string,$start,$end-$start));
    }
}
