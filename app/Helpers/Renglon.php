<?php

namespace App\Helpers;

class Renglon {
    private $left;
    private $height;
    private $width;
    private $top;
    private $value;

    private $is_title;

    function __construct($renglon_string) {
        $this->left=$this->getPixels('left',$renglon_string);
        $this->height=$this->getPixels('height',$renglon_string);
        $this->width=$this->getPixels('width',$renglon_string);
        $this->top=$this->getPixels('top',$renglon_string);
        $this->value= $this->procesarCarecteresEspecialesHtml($this->getValueDiv($renglon_string));
        $this->is_title = strpos($renglon_string,'font-weight:bold;') ? true : false;
    }

    public function getLeft(){return $this->left;}
    public function getTop(){return $this->top;}
    public function getHeight(){return $this->height;}
    public function getWidth(){return $this->width;}
    public function getValue(){return $this->value;}
    public function isTitle(){return $this->is_title;}

    public function getPixels($position,$div){
        return $this->getStringValue($position.':','px',$div);
    }

    private function getValueDiv($div){
        $value = $this->getStringValue('<span style="white-space:pre;">','</span>',$div);

        if($value===false)
            $value = $this->getStringValue('<span style=\'white-space:pre;\'>','</span>',$div);

        if($value===false || $value==='')
            $value = $this->getStringValue('<span>','</span>',$div);

        return $value===false || $value==='' ? false : $value;
    }

    private function getStringValue($find_string,$delimiter,$div){
        $position_pos = strpos($div,$find_string);

        if($position_pos===false) return false;

        $position_start = $position_pos+strlen($find_string);
        $position_end = strpos($div,$delimiter,$position_start);
        $value = substr($div,$position_start,$position_end-$position_start);

        return $value;
    }


    function procesarCarecteresEspecialesHtml($value){
        $final_value = str_replace('&#47;','/',$value);
        return $final_value;
    }


}
