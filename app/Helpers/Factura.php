<?php

namespace App\Helpers;

class Campo {
    private $encabezado;
    public $dato;

    function __construct($encabezado) {
        $this->encabezado=$encabezado;
    }

    public function getEncabezado(){
        return $this->encabezado;
    }
}

class Factura {
    private $lefts;
    private $tops;
    private $campo_almacen;
    private $campo_folio;
    private $campo_fecha_hora_emision;
    private $campo_contacto;
    private $campo_contacto_para;
    private $rfc;
    private $agente;
    private $campo_condicion_envio;
    private $campo_envio;
    private $telefonos;
    private $campo_domicilio;
    private $conceptos;

    function __construct($factura_string) {
        $this->lefts = [];
        $this->tops = [];

        $this->setArrays($factura_string);

        $this->setData();
        $this->setConceptos();
    }

    public function getPaqueteria(){
        return $this->campo_envio->dato??'No se especifica';
    }
    public function getTipoEnvio(){
        return $this->campo_condicion_envio->dato??'No se especifica';
    }

    public function getFechaHoraEmision(){
        return $this->campo_fecha_hora_emision->dato;
    }

    public function getAgente(){
        return $this->agente;
    }

    public function getConceptos(){
        return $this->conceptos->dato;
    }
    public function getAlmacen(){
        return $this->campo_almacen->dato;
    }

    public function getTelefonos(){
        return $this->telefonos;
    }

    public function getDomicilio(){
        return $this->campo_domicilio->dato;
    }

    /**
     * Devuelve el folio de la factura
     * @return string Folio
     */
    public function getFolio(){
        return $this->campo_folio->dato;
    }

    /**
     * Devuelve el RFC de la factura
     * @return string RFC
     */
    public function getRFC(){
        return $this->rfc;
    }

    /**
     * Devuelve el Nombre del contacto del factura
     * @return string Nombre del contacto
     */
    public function getNombreContacto(){
        return $this->campo_contacto->dato;
    }

    public function getNombreContactoPara(){
        return $this->campo_contacto_para->dato;
    }

    public function setArrays($factura_string){
        $div_apertura = 0;

        while(true){

            $div_apertura = strpos($factura_string,'<div class="font',$div_apertura); 

            if(!$div_apertura) break;

            $div_cierre = strpos($factura_string,'</span></div>',$div_apertura)+13;

            $div = substr($factura_string,$div_apertura,$div_cierre-$div_apertura);

            $renglon = new \App\Helpers\Renglon($div);

            if(strpos($renglon->getValue(),"ALMAC")!==false) $this->campo_almacen = new Campo($renglon);
            else
            if($renglon->getValue()=='FACTURA') $this->campo_folio = new Campo($renglon);
            else
            if(strpos($renglon->getValue(),"FECHA Y HORA DE EMISI")!==false)$this->campo_fecha_hora_emision = new Campo($renglon);
            else
            if($renglon->getValue()=='CONTACTO:') $this->campo_contacto = new Campo($renglon);
            else
            if($renglon->getValue()=='PARA') $this->campo_contacto_para = new Campo($renglon);
            else
            if(strpos($renglon->getValue(),"RFC: ")!==false) $this->rfc = substr($renglon->getValue(),5);
            else
            if(strpos($renglon->getValue(),"AGENTE: ")!==false) $this->agente = substr($renglon->getValue(),8);
            else
            if(strpos($renglon->getValue(),"FONOS: ")!==false) $this->telefonos = trim(substr($renglon->getValue(),11),' ,');
            else
            if(strpos($renglon->getValue(),"Condiciones de Envio:")!==false) $this->campo_condicion_envio = new Campo($renglon);
            else
            if(strpos($renglon->getValue(),"Via de Embarque:")!==false) $this->campo_envio = new Campo($renglon);
            else
            if(strpos($renglon->getValue(),"DOMICILIO FISCAL:")!==false) $this->campo_domicilio = new Campo($renglon);
            else
            if(strpos($renglon->getValue(),"CANTIDAD")!==false) $this->conceptos = new Campo($renglon);
            
            $this->lefts[$renglon->getLeft()??'N/A'][] = $renglon;
            $this->tops[$renglon->getTop()??'N/A'][] = $renglon;

            $div_apertura++;
        }


        foreach ($this->lefts as $key => $value) {
            usort($value,function($a, $b) {
                return $a->getTop() - $b->getTop();
            });

            $this->lefts[$key] = $value;
        }

        foreach ($this->tops as $key => $value) {
            usort($value,function($a, $b) {
                return $a->getLeft() - $b->getLeft();
            });

            $this->tops[$key] = $value;
        }
    }

    public function getPixels($position,$div){
        $position_pos = strpos($div,$position.':');

        if($position_pos===false) return false;

        $position_start = $position_pos+strlen($position)+1;
        $position_end = strpos($div,'px',$position_start);
        $px = substr($div,$position_start,$position_end-$position_start);

        return $px;
    }

    public function getLefts(){
        return $this->lefts;
    }

    public function setData(){
        if($this->campo_almacen!=false)
            $this->campo_almacen->dato = $this->getValue($this->campo_almacen->getEncabezado(),true);

        if($this->campo_folio!=false)
            $this->campo_folio->dato = $this->getValue($this->campo_folio->getEncabezado(),true);

        if($this->campo_fecha_hora_emision!=false){
            $value_row = str_replace('&#47;','/',$this->getCompleteRowValue($this->campo_fecha_hora_emision->getEncabezado()));
            $values = explode(' ',$value_row);
            $fecha = $values[0];
            $hora = $values[1];
            $arregloFecha = explode('/', $fecha);
            $fechaHoraEmision = $arregloFecha[2] . '-' . $arregloFecha[1] . '-' . $arregloFecha[0] . ' ' . $hora;
            
            $this->campo_fecha_hora_emision->dato = $fechaHoraEmision;
        }

        if($this->campo_contacto!=false)
            $this->campo_contacto->dato = $this->getValue($this->campo_contacto->getEncabezado());

        if($this->campo_contacto_para!=false)
            $this->campo_contacto_para->dato = $this->getValue($this->campo_contacto_para->getEncabezado(),false,'RFC:');

        if($this->campo_domicilio!=false)
            $this->campo_domicilio->dato = $this->getValue($this->campo_domicilio->getEncabezado(),false,'TEL');

        if($this->campo_condicion_envio!=false)
            $this->campo_condicion_envio->dato = $this->getRightValue($this->campo_condicion_envio->getEncabezado());

        if($this->campo_envio!=false)
            $this->campo_envio->dato = $this->getRightValue($this->campo_envio->getEncabezado());
    }


    /**
     * @param \App\Helpers\Renglon $renglon
     */
    public function getValue($renglon,$is_title=false,$delimiter=false){
        $bandera_inicio = false;
        $value = false;

        foreach($this->lefts[$renglon->getLeft()] as $left){
            if(!$bandera_inicio){
                if($renglon==$left){
                    $bandera_inicio = true;
                    $value = '';
                }
            } else {
                if($delimiter!==false){
                    if(strpos($left->getValue(),$delimiter)!==false){
                        break;
                    }
                }

                if($left->isTitle()&&$is_title===false){
                    break;
                } else {
                    $value .= $left->getValue().' ';
                }
            }
        }

        return $value===''?false:trim($value);
    }


    /**
     * @param \App\Helpers\Renglon $renglon
     */
    public function getRightValue($renglon,$is_title=false,$delimiter=false){
        $bandera_inicio = false;
        $value = false;

        foreach($this->tops[$renglon->getTop()] as $top){
            if(!$bandera_inicio){
                if($renglon==$top){
                    $bandera_inicio = true;
                    $value = '';
                }
            } else {
                if($delimiter!==false){
                    if(strpos($top->getValue(),$delimiter)!==false){
                        break;
                    }
                }

                if($top->isTitle()&&$is_title===false){
                    break;
                } else {
                    $value .= $top->getValue().' ';
                }
            }
        }

        return $value===''?false:trim($value);
    }

    /**
     * @param \App\Helpers\Renglon $renglon
     */
    public function getCompleteRowValue($renglon,$is_title=false,$delimiter=false){
        $pos = 0;

        foreach($this->lefts[$renglon->getLeft()] as $left){
            $pos++;
            if($renglon==$left) break;
        }

        return $this->getValue($renglon).' '.$this->getRightValue($this->lefts[$renglon->getLeft()][$pos]);

    }


    public function setConceptos(){
        $lefts = $this->lefts[$this->conceptos->getEncabezado()->getLeft()];
        $this->conceptos->dato=[];
        $arreglo_conceptos=[];
        
        for($i=1;$i<count($lefts);$i++)
            $arreglo_conceptos[]=$this->tops[$lefts[$i]->getTop()];

        foreach($arreglo_conceptos as $arreglo_concepto)
            $this->conceptos->dato[] = new \App\Helpers\Concepto($arreglo_concepto);
    }

    
}
