<?php

namespace App\Helpers;

class XMLString {
    protected $xml_string;

    /**
     * Recibe el xml en string
     * 
     * @param string $xml_string
     */
    function __construct($xml_string) {
        $this->xml_string = $xml_string;
    }

    /**
     * Devuelve el valor de la asignado a la key
     * 
     * @param string $key La key a buscar
     * @return bool|string Devuelve el valor de asignado a la key
     *                     Devuelve `false` en caso de no encontrar la key
     */
    public function getValue($key){
        return $this->getValueFromString($key,$this->xml_string);
    }

    private function getValueFromString($key,$xml_string){
        //Obtenemos la longitud de la $key
        $len_str = strlen($key);
        
        //Buscamos la posición de la $key en $xml_string
        $start = strpos($xml_string,$key);

        //Retornamos `false` en caso de que no exista
        if($start===false) return false;

        //A inicio le sumamos los carecteres `=` y `"`
        $start += $len_str+2;

        //Buscamos la posición donde termina el valor de la key. Iniciando después de la `"` de apertura.
        $fin = strpos($xml_string,'"',$start);

        //Devolvemos el valor de la key
        return substr($xml_string,$start,$fin-$start);
    }


    public function getValueFromTag($tag,$key){
        $tag_start = strpos($this->xml_string,'<cfdi:'.$tag);
        $tag_end = strpos($this->xml_string,'/>');
        $tag_string = substr($this->xml_string,$tag_start,$tag_end);

        return $this->getValueFromString($key,$tag_string);
    }
}
