<?php

namespace App\Helpers;

class Concepto {
    private $unidades;
    private $tipo_unidad;
    private $codigo;
    private $lote;
    private $fecha_caducidad;
    private $lote_caducidad;
    private $descripcion;
    private $precio_unitario;
    private $precio_total;
    private $is_servicio = false;

    /**
     * @param array $arreglo_concepto Contiene los datos de un concepto
     */
    function __construct($arreglo_concepto) {
        if(count($arreglo_concepto)==7){
            $this->unidades = $arreglo_concepto[0]->getValue();
            $this->tipo_unidad = $arreglo_concepto[1]->getValue();
            $this->codigo = $arreglo_concepto[2]->getValue();

            $this->lote_caducidad = $arreglo_concepto[3]->getValue();
            $this->lote = $this->getStringValue('Lote: ',' ',$arreglo_concepto[3]->getValue());
            $this->fecha_caducidad = $this->getStringValue('Fecha de caducidad: ',false,$arreglo_concepto[3]->getValue());

            $this->descripcion = $arreglo_concepto[4]->getValue();
            $this->precio_unitario = $arreglo_concepto[5]->getValue();
            $this->precio_total = $arreglo_concepto[6]->getValue();
        } else if(count($arreglo_concepto)==6){
            $this->is_servicio = true;
            $this->unidades = $arreglo_concepto[0]->getValue();
            $this->tipo_unidad = $arreglo_concepto[1]->getValue();
            $this->codigo = $arreglo_concepto[2]->getValue();
            $this->descripcion = $arreglo_concepto[3]->getValue();
            $this->precio_unitario = $arreglo_concepto[4]->getValue();
            $this->precio_total = $arreglo_concepto[5]->getValue();
        }
    }

    public function getTipoUnidad()
    {
        return $this->tipo_unidad;
    }


    public function getLoteCaducidad()
    {
        return $this->lote_caducidad;
    }

    public function getFechaCaducidad()
    {
        return $this->fecha_caducidad;
    }

    public function getLote()
    {
        return $this->lote;
    }

    public function getUnidades()
    {
        return $this->unidades;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    private function getStringValue($find_string,$delimiter,$div){
        $position_pos = strpos($div,$find_string);

        if($position_pos===false) return false;

        $position_start = $position_pos+strlen($find_string);

        if($delimiter!==false){
            $position_end = strpos($div,$delimiter,$position_start);
            $value = substr($div,$position_start,$position_end-$position_start);
        } else {
            $value = substr($div,$position_start);
        }

        return $value;
    }
}
