<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoEnvio extends Model
{
    use HasFactory;
    protected $table = 'c_tipo_envio';
    protected $fillable = [
        'nombre',
        'capturado_por',
        'actualizado_por',
    ];
}
