<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoCaja extends Model
{
    use HasFactory;
    protected $table = 'c_tipo_caja';
    protected $fillable = [
        'nombre',
        'capturado_por',
        'actualizado_por',
    ];
}
