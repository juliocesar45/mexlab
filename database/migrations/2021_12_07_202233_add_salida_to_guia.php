<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSalidaToGuia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guia', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger("salida_id")->unsigned()->nullable();
            $table->foreign("salida_id")->references("id")->on("salida")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guia', function (Blueprint $table) {
            $table->dropColumn(['salida_id']);
        });
    }
}
