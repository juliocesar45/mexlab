<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCamposRack extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('c_rack', function (Blueprint $table) {
            $table->string('fila1')->nullable();
            $table->string('alto_fila1')->nullable();
            $table->string('ancho_fila1')->nullable();
            $table->string('fila2')->nullable();
            $table->string('alto_fila2')->nullable();
            $table->string('ancho_fila2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('c_rack', function (Blueprint $table) {
            $table->dropColumn(['fila1','alto_fila1','ancho_fila1','fila2','alto_fila2','ancho_fila2']);
        });
    }
}
