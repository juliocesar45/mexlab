<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMensajeroIdToSalidaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salida', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger("mensajero_id")->unsigned()->nullable();
            $table->foreign("mensajero_id")->references("id")->on("c_mensajero")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salida', function (Blueprint $table) {
            $table->dropColumn('mensajero_id');
        });
    }
}
