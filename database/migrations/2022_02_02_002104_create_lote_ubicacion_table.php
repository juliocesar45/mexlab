<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoteUbicacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lote_ubicacion', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("c_ubicacione_id")->unsigned()->nullable();
            $table->foreign("c_ubicacione_id")->references("id")->on("c_ubicacione")->onDelete("cascade");
            $table->bigInteger("lote_producto_id")->unsigned()->nullable();
            $table->foreign("lote_producto_id")->references("id")->on("lote_producto")->onDelete("cascade");
            $table->unsignedInteger('cantidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lote_ubicacion');
    }
}
