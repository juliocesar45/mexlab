<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRutaEmbarquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ruta_embarque', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger("paqueteria_id")->unsigned()->nullable();
            $table->foreign("paqueteria_id")->references("id")->on("c_paqueteria")->onDelete("cascade");
            $table->string ('guia')->nullable();
            $table->string ('entregar_a')->nullable();
            $table->string ('direccion_factura')->nullable();
            $table->string ('direccion')->nullable();
            $table->bigInteger("conductor_id")->unsigned()->nullable();
            $table->foreign("conductor_id")->references("id")->on("c_conductor")->onDelete("cascade");
            $table->bigInteger("mensajero_id")->unsigned();
            $table->foreign("mensajero_id")->references("id")->on("c_mensajero")->onDelete("cascade");
            $table->bigInteger("tipo_envio_id")->unsigned()->nullable();
            $table->foreign("tipo_envio_id")->references("id")->on("c_tipo_envio")->onDelete("cascade");
            $table->string ('ruta')->nullable();
            $table->string ('tiempo_estimado')->nullable();
            $table->dateTime('fecha_entrega_estimada');
            $table->dateTime('fecha_entrega');
            $table->string ('estatus')->nullable();
            $table->string('capturado_por');
            $table->string('actualizado_por');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ruta_embarque');
    }
}