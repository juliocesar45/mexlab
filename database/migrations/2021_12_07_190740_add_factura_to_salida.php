<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFacturaToSalida extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salida', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger("factura_id")->unsigned();
            $table->foreign("factura_id")->references("id")->on("factura")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salida', function (Blueprint $table) {
            $table->dropColumn(['factura_id']);
        });
    }
}
