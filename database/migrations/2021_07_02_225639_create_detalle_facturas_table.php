<?php


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleFacturasTable extends Migration

{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('detalle_factura');

        Schema::create('detalle_factura', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger("factura_id")->unsigned()->nullable();
            $table->foreign("factura_id")->references("id")->on("factura")->onDelete("cascade");
            $table->bigInteger("lote_producto_id")->unsigned()->nullable();
            $table->foreign("lote_producto_id")->references("id")->on("lote_producto")->onDelete("cascade");
            $table->integer("cantidad");
            $table->string("notas")->nullable();
            $table->string("descripcion")->nullable();
            $table->string("lote_caducidad")->nullable();
            $table->string("unidad")->nullable();
            $table->string("codigo")->nullable();
            $table->string("codigo2")->nullable();
            $table->string("estatus");
            $table->string('capturado_por');
            $table->string('actualizado_por');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_factura');
    }
}
