<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_rack', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger("c_almacene_id")->unsigned();
            $table->string('pasillo');
            $table->integer('cantidad_slots');
            $table->float('largo');
            $table->float('ancho');
            $table->integer('cant_slot_alto');
            $table->integer('cant_slot_ancho');
            $table->foreign("c_almacene_id")->references("id")->on("c_almacenes");
            $table->string('capturado_por');
            $table->string('actualizado_por');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_rack');
    }
}
