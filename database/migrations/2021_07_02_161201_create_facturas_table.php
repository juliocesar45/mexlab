<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger("cliente_id")->unsigned();
            $table->foreign("cliente_id")->references("id")->on("c_cliente")->onDelete("cascade");
            $table->integer("cantidad");
            $table->bigInteger("estatus_factura_id")->unsigned();
            $table->foreign("estatus_factura_id")->references("id")->on("c_estatus_facturas")->onDelete("cascade");
            $table->string("identificador");
            $table->dateTime("fecha_embarque");
            $table->bigInteger("salida_id")->unsigned()->nullable();
            $table->foreign("salida_id")->references("id")->on("salida")->onDelete("cascade");
            $table->string('capturado_por');
            $table->string('actualizado_por');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura');
    }
}
