<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntradasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entrada', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->timestamps();
            $table->dateTime('fecha');
            $table->bigInteger("proveedor_id")->unsigned()->nullable();
            $table->foreign("proveedor_id")->references("id")->on("c_proveedor")->onDelete("cascade");
            $table->bigInteger("tipo_entrada_id")->unsigned()->nullable();
            $table->foreign("tipo_entrada_id")->references("id")->on("c_tipo_entrada")->onDelete("cascade");
            $table->string('estado');
            $table->string('folio_compra');
            $table->string('capturado_por');
            $table->string('actualizado_por');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entrada');
    }
}
