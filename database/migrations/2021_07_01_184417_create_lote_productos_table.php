<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoteProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('lote_producto');
        Schema::create('lote_producto', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('lote');
            $table->date('caducidad');
            $table->integer('cantidad');
            $table->string('estatus');
            $table->boolean('comodin');
            $table->integer('cantidad_actual');
            $table->string('capturado_por');
            $table->string('actualizado_por');
            $table->bigInteger("producto_id")->unsigned()->nullable();
            $table->foreign("producto_id")->references("id")->on("producto")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lote_producto');
    }
}
