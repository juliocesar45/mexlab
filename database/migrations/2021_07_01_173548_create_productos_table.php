<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('producto');

        Schema::create('producto', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->timestamps();
            $table->bigInteger("tipo_caja_id")->unsigned()->nullable();
            $table->foreign("tipo_caja_id")->references("id")->on("c_tipo_caja")->onDelete("cascade");
            $table->bigInteger("familia_producto_id")->unsigned()->nullable();
            $table->foreign("familia_producto_id")->references("id")->on("c_familia_producto")->onDelete("cascade");
            $table->float('alto');
            $table->float('ancho');
            $table->float('fondo');
            $table->float('volumen');
            $table->integer('cantidad');
            $table->string('codigo');
            $table->float('peso');
            $table->string('qr_data');
            $table->string('capturado_por');
            $table->string('actualizado_por');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto');
    }
}

