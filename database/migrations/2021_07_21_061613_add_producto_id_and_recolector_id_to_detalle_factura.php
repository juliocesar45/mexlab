<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProductoIdAndRecolectorIdToDetalleFactura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalle_factura', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger("producto_id")->unsigned();
            $table->foreign("producto_id")->references("id")->on("lote_producto");
            $table->bigInteger("recolector_id")->unsigned();
            $table->foreign("recolector_id")->references("id")->on("c_recolectore");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_factura', function (Blueprint $table) {
            $table->dropColumn(['producto_id', 'recolector_id']);
        });
    }
}
