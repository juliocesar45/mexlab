<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guia', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('guia');
            $table->bigInteger("c_paqueteria_id")->unsigned()->nullable();
            $table->foreign("c_paqueteria_id")->references("id")->on("c_paqueteria")->onDelete("cascade");
            $table->bigInteger("cliente_id")->unsigned()->nullable();
            $table->foreign("cliente_id")->references("id")->on("c_cliente")->onDelete("cascade");
            $table->string("direccion");
            $table->dateTime('fecha_recoleccion');
            $table->string('capturado_por');
            $table->string('actualizado_por');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guia');
    }
}
