<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUbicacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('c_ubicacione');
        
        Schema::create('c_ubicacione', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger("rack_id")->unsigned();
            $table->foreign("rack_id")->references("id")->on("c_rack")->onDelete("cascade");
            $table->bigInteger("lote_producto_id")->unsigned()->nullable();
            $table->foreign("lote_producto_id")->references("id")->on("lote_producto")->onDelete("cascade");
            $table->integer('slot');
            $table->integer('x');
            $table->integer('y');
            $table->boolean('seccion');
            $table->float('alto', 8, 2);
            $table->float('ancho', 8, 2);
            $table->string('identificador');
            $table->integer('cantidad');
            $table->string('qr_data');
            $table->string('capturado_por');
            $table->string('actualizado_por');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_ubicacione');
    }
}
