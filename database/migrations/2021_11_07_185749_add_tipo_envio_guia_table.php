<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTipoEnvioGuiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guia', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger("tipo_envio_id")->unsigned()->nullable();
            $table->foreign("tipo_envio_id")->references("id")->on("c_tipo_envio")->onDelete("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guia', function (Blueprint $table) {
            $table->dropColumn(['tipo_envio_id']);
        });
    }
}
