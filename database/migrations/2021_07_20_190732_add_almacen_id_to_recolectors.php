<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAlmacenIdToRecolectors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('c_recolectore', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger("almacen_id")->unsigned();
            $table->foreign("almacen_id")->references("id")->on("c_almacenes");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('c_recolectore', function (Blueprint $table) {
            //
        });
    }
}
