<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('salida');
        Schema::create('salida', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->dateTime('fecha');
            $table->bigInteger("tipo_salida_id")->unsigned()->nullable();
            $table->foreign("tipo_salida_id")->references("id")->on("c_tipo_salida")->onDelete("cascade");
            $table->bigInteger("cliente_id")->unsigned()->nullable();
            $table->foreign("cliente_id")->references("id")->on("c_cliente")->onDelete("cascade");
            $table->bigInteger("guia_id")->unsigned()->nullable();
            $table->foreign("guia_id")->references("id")->on("guia")->onDelete("cascade");
            $table->string ('resultado_envio')->nullable();
            $table->string ('notas')->nullable();            
            $table->bigInteger("ruta_embarque_id")->unsigned()->nullable();
            $table->foreign("ruta_embarque_id")->references("id")->on("ruta_embarque")->onDelete("cascade");
            $table->string('capturado_por');
            $table->string('actualizado_por');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salida');
    }
}
