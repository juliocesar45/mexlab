<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFacturaIdToGuiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guia', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('factura_id')->unsigned()->nullable();
            $table->foreign("factura_id")->references("id")->on("factura");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guia', function (Blueprint $table) {
            $table->dropColumn(['factura_id']);
        });
    }
}
